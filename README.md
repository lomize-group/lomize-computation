# lomize-computation
---
This repository contains code and instructions for the computation servers for [Lomize Group](https://pharmacy.umich.edu/lomize-group) at the University of Michigan. The application is written in [Java](https://www.java.com/) and uses the [Spring Boot](https://www.tutorialspoint.com/spring_boot/spring_boot_introduction.htm) framework.


## Getting Set Up
---
### Running with Docker
This project is not intended to be ran in a standalone manner. See instructions at [lomizegroup/opm](https://hub.docker.com/r/lomizegroup/opm) for running the computation service along with the frontend.

### Prerequisites
- [Java](https://www.java.com/) (version 8)
- [Gradle](https://gradle.org/) build tool for Java
    - Depending on your IDE, you may need to add plugins to Gradle to get full IDE support.
- Use Mac OS X or Linux environment

### Downloading the Code
```
git clone https://lomize-group@bitbucket.org/lomize-group/lomize-computation.git
cd lomize-computation
```


# Production Architecture (memprot.org)
## Environment
This application runs on an Ubuntu Linux VM on the [Amazon Web Services](https://aws.amazon.com/) cloud.

## Web Server Configuration
This application does not directly listen for requests. Instead, [NGINX](https://www.nginx.com/) is used as a [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy) to forward HTTPS requests to this web server, which runs locally on port 9000.

This application only listens for unencrypted HTTP requests. NGINX handles decryption of requests and encryption of responses.

### SSL Certificate
[Let's Encrypt](https://letsencrypt.org/) is used to acquire signed certificates. These are automatically renewed.

## System Process
This application runs as a `systemd` service in production. It does this in order to run in the background when nobody is logged into the VM. This service run as a user with low levels of permissions to avoid security vulnerabilities.


# Development
## Building the Application
The Gradle build system is used to build this application.
To build for development and rebuild when changes occur to source code files, run `./gradlew build -x test --continuous`

## Running the Web Server
To run the webserver with automatic reload, run `./gradlew bootRun`. You should be continuously building the project with `./gradlew build -x test --continuous` in order to get automatic reload to work

## Writing code
To make changes to source code, modify the files under `src/main/lomize/computation`.

Code should be written in a modular, testable style using object-oriented programming, including principles such as [dependency injection](https://en.wikipedia.org/wiki/Dependency_injection) to achieve [inversion of control](https://en.wikipedia.org/wiki/Inversion_of_control). This helps achieve separation of concerns, making code more testable, and allows for mock objects to be injected where needed in unit tests.

## Testing
To write unit tests, modify the files in `src/test/java/lomize/computation/`.

This project uses executables compiled in a Linux environment. You may find it difficult to test the entire system without the executables. You can make simple shell scripts that create files and produce output, and place them where you would expect to find the executables, in order to simulate their behavior.

# File Structure
## Source Code
All of the following are under `src/main/java/lomize/computation/`:

- `Application.java`
    - Contains the code to start the Spring Boot web application
- `common/`
    - Contains other code that can be used and shared throughout the application. If you write a new reusable class, put it in this folder.
    - Might contain interfaces, abstract classes, other classes
- `controllers/`
    - Contains API controllers for the web application. If you need to add more API endpoints, add controllers in this folder.
- `models/`
    - Contains data models.
    - `requests/`
        - Contains data models for expected HTTP request body. The Spring framework can de-serialize the JSON body of POST requests, or the query parameters of GET requests, into your request model.
    - `response/`
        - Contains the data models for HTTP response body. The Spring framework can serialize these classes into JSON when they are returned from an API endpoint.
- `services/`
    - Contains files and folders corresponding to different services used throughout the application.


## Test Code
The testing code should mirror the source code file structure, but lives under `src/test/java/lomize/computation/`.


## Resources
Resources needed by the web application, including data files, HTML templates, and compiled executables, are found under `/resources/`. This folder contains a few things:

- `resources/executables/`
    - Contains executables and supplemental files for each program.
- `processes/`
    - Contains execution folders. Executables and their supplemental files are copied into a folder in here. Input files are written and the executable is run, which may generate more files and write to an output file.
- `resources/templates/`
    - Contains Apache Freemarker templates. Freemarker is a templating engine used to dynamically render HTML. This is used to generate results pages based on computation results.

# Deploying
Assuming an identical production architecture to the one described in this document, simply run the start script with `./start.sh` from the root directory of this project. This builds the application and restarts the `systemd` process running this program.

The compiled java bytecode will be placed in a `.jar` file located in `/build/libs/`. Make sure this is never running as root user or a user with root privileges.

Make sure the program does not have root permissions. You should always set all permissions in this project as low as possible. It should be running as a user without root privileges.

The compiled `.jar` file should also be read-only to avoid other security vulnerabilities.
