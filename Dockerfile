FROM gradle:7.4-jdk17 AS buildvm
WORKDIR computation
RUN chown gradle:gradle .
CMD bash

FROM buildvm AS debug
EXPOSE 9000
RUN apt-get -y update \
    && apt-get -y install \
        awscli \
        htop \
        less \
        netcat \
        tmux \
        tree
USER gradle
COPY --chown=gradle htoprc /home/gradle/.config/htop/htoprc
CMD tmux

FROM buildvm AS build
USER gradle
COPY --chown=gradle . .
RUN gradle --no-daemon build -x test

# NOTE: on M1 host, build with multiple cpus (since built on host architecture)
# but run with one cpu (since x64 multicore guest is broken on M1 host)
FROM --platform=linux/amd64 eclipse-temurin:17-focal AS runvm
RUN apt-get -y update \
    && apt-get -y install \
        awscli \
        libgfortran5 \
    && useradd -m -s /bin/bash -U user
WORKDIR /home/user
EXPOSE 9000
CMD bash

FROM runvm AS run
USER user
WORKDIR computation
COPY --chown=user resources resources
# not sure if lombok.config is build time or run time dependency
COPY --chown=user lombok.config application.properties .
COPY --chown=user --from=build /home/gradle/computation/build build
RUN mkdir processes
