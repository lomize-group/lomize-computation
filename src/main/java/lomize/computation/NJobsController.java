package lomize.computation.controllers;

import java.util.concurrent.ThreadPoolExecutor;
import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.ApplicationContextUtils;
import lomize.computation.common.Util;
import lomize.computation.common.execution.Environment;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.ppm3.PPM3Params;
import lomize.computation.services.ppm3.PPM3Service;

@RestController
public class NJobsController {

    @CrossOrigin
    @GetMapping("/njobs")
    @Validated
    @ResponseBody
    public ResponseEntity<Response> get() {
        ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) ctx.getBean("threadPool");
        int nJobs = threadPool.getQueue().size();
        String message = String.format("njobs: %d", nJobs);
        return new SubmittedResponse(null, message).ok();
    }
}
