// https://stackoverflow.com/a/18486178
package lomize.computation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextUtils implements ApplicationContextAware {
    private static ApplicationContext ctx;

    @Override
    public void setApplicationContext(ApplicationContext ctxIn) {
        ctx = ctxIn;
    }

    public static ApplicationContext getApplicationContext() {
        return ctx;
    }
}
