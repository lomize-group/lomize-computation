package lomize.computation.controllers;

import static lomize.computation.services.tmmatch.TMMatchConfig.BUCKET_NAME;
import static lomize.computation.services.tmmatch.TMMatchConfig.BUCKET_REGION;
import static lomize.computation.services.tmmatch.TMMatchConfig.EMAIL_SUBJECT;
import static lomize.computation.services.tmmatch.TMMatchConfig.EXECUTABLE_NAME;
import static lomize.computation.services.tmmatch.TMMatchConfig.PROCESS_NAME;
import static lomize.computation.services.tmmatch.TMMatchConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.tmmatch.TMMatchConfig.TEMPLATE_FILENAME;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.TMMatchRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.tmmatch.TMMatchParams;
import lomize.computation.services.tmmatch.TMMatchService;

@RestController
public class TMMatchController {

	@CrossOrigin
	@PostMapping("/tmmatch")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid @RequestBody final TMMatchRequest req) {
		// create environment
		final Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create tmmatch service and run in background
		TMMatchService service = new TMMatchService(env, uploader, emailer, templateRenderer);
		service.setParams(new TMMatchParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);

		// return link to waiting page which will redirect to results when done
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
