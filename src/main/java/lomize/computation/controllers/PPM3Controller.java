package lomize.computation.controllers;

import static lomize.computation.services.ppm3.PPM3Config.BUCKET_NAME;
import static lomize.computation.services.ppm3.PPM3Config.BUCKET_REGION;
import static lomize.computation.services.ppm3.PPM3Config.EMAIL_SUBJECT;
import static lomize.computation.services.ppm3.PPM3Config.EXECUTABLE_NAME;
import static lomize.computation.services.ppm3.PPM3Config.PROCESS_NAME;
import static lomize.computation.services.ppm3.PPM3Config.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.ppm3.PPM3Config.TEMPLATE_FILENAME;

import java.io.IOException;
import java.io.FileWriter;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.PPM3Request;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.ppm3.PPM3Params;
import lomize.computation.services.ppm3.PPM3Service;

@RestController
public class PPM3Controller {

    @CrossOrigin
    @PostMapping("/ppm3")
    @Validated
    @ResponseBody
    public ResponseEntity<Response> post(@Valid @RequestBody final PPM3Request req) {
        // create environment
        Environment env = new Environment();
        env.createIn(Util.getProcessDirectory(PROCESS_NAME));
        env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
        env.setExecutableName(EXECUTABLE_NAME);

        // save input PDB file to environment
        final String inputPdbFilename = Util.sanitizeFilename(req.getPdbFilename());
        try (FileWriter writer = new FileWriter(env.getFile(inputPdbFilename))) {
            writer.write(req.getPdbFile());
        } catch (IOException e) {
            return new SubmittedResponse(null, "Could not save PDB file " + inputPdbFilename).badRequest();
        }

        // create template renderer
        TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

        // create file uploader
        AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
        uploader.setDestPath(env.getEnvName());

        // create emailer
        AmazonEmailClient emailer = null;
        if (req.getUserEmail() != null) {
            emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
        }

        // create ppm3 service and run in background
        PPM3Service service = new PPM3Service(env, uploader, emailer, templateRenderer);
        service.setParams(new PPM3Params(req));
        final int nJobs = service.runInNewThread();
        final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
        return new SubmittedResponse(resultsUrl, nJobs).ok();
    }
}
