package lomize.computation.controllers;

import static lomize.computation.services.tmpfold.TMPFoldConfig.BUCKET_NAME;
import static lomize.computation.services.tmpfold.TMPFoldConfig.BUCKET_REGION;
import static lomize.computation.services.tmpfold.TMPFoldConfig.EMAIL_SUBJECT;
import static lomize.computation.services.tmpfold.TMPFoldConfig.EXECUTABLE_NAME;
import static lomize.computation.services.tmpfold.TMPFoldConfig.PROCESS_NAME;
import static lomize.computation.services.tmpfold.TMPFoldConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.tmpfold.TMPFoldConfig.TEMPLATE_FILENAME;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.TMPFoldRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.tmpfold.TMPFoldParams;
import lomize.computation.services.tmpfold.TMPFoldService;

@RestController
public class TMPFoldController {

	@CrossOrigin
	@PostMapping("/tmpfold")
	@Validated
	@ResponseBody
	final ResponseEntity<Response> post(@Valid final TMPFoldRequest req) {
		final Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// save input pdb file if there is one
		final String pdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
		try {
			req.getPdbFile().transferTo(env.getFile(pdbFilename));
		} catch (IOException e) {
			return new SubmittedResponse(null, "Could not save PDB file " + pdbFilename).badRequest();
		}

		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create tmpfold service and run in background
		TMPFoldService service = new TMPFoldService(env, uploader, emailer, templateRenderer);
		service.setParams(new TMPFoldParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
