package lomize.computation.controllers;

import static lomize.computation.services.opm.update.OpmUpdateConfig.BUCKET_NAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.BUCKET_REGION;
import static lomize.computation.services.opm.update.OpmUpdateConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.UNFORMATTED_RESULTS_FILENAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.getResultFolderPath;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.controllers.request.OpmPdbRetrievalPrimaryStructureRequest;
import lomize.computation.controllers.request.OpmPdbRetrievalRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.opm.update.primary_structure.PrimaryStructureRetrievalService;
import lomize.computation.services.opm.update.unformatted.OpmPdbRetrievalService;

@RestController
public class OpmUpdateController {

	@CrossOrigin
	@PostMapping("/opm_pdb_retrieval")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid @RequestBody final OpmPdbRetrievalRequest req) {
		// create uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(getResultFolderPath(Util.getTimestampedRandomFolderName()));

		// create service, set parameters, and run service
		OpmPdbRetrievalService service = new OpmPdbRetrievalService(uploader);
		service.setPdbCodes(req.getPdbCodes());

		final ExecutorService threadPool = Executors.newFixedThreadPool(1);
		threadPool.execute(service);

		final String resultsUrl = uploader.getFileDestinationPath(UNFORMATTED_RESULTS_FILENAME);
		return new SubmittedResponse(resultsUrl).ok();
	}

	@CrossOrigin
	@PostMapping("/opm_pdb_retrieval/primary_structure")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> postPrimaryStructure(
			@Valid @RequestBody final OpmPdbRetrievalPrimaryStructureRequest req) {
		// create uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(getResultFolderPath(Util.getTimestampedRandomFolderName()));

		TemplateRenderer templateRenderer = new TemplateRenderer("link_list.ftl");

		// create service, set parameters, and run service
		PrimaryStructureRetrievalService service = new PrimaryStructureRetrievalService(templateRenderer, uploader);
		service.setInputData(req.getData());

		final ExecutorService threadPool = Executors.newFixedThreadPool(1);
		threadPool.execute(service);

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl).ok();
	}

}