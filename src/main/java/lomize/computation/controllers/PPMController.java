package lomize.computation.controllers;

import static lomize.computation.services.ppm.PPMConfig.BUCKET_NAME;
import static lomize.computation.services.ppm.PPMConfig.BUCKET_REGION;
import static lomize.computation.services.ppm.PPMConfig.EMAIL_SUBJECT;
import static lomize.computation.services.ppm.PPMConfig.EXECUTABLE_NAME;
import static lomize.computation.services.ppm.PPMConfig.PROCESS_NAME;
import static lomize.computation.services.ppm.PPMConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.ppm.PPMConfig.TEMPLATE_FILENAME;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.PPMRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.ppm.PPMParams;
import lomize.computation.services.ppm.PPMService;

@RestController
public class PPMController {

	@CrossOrigin
	@PostMapping("/ppm")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid final PPMRequest req) {
		// create environment
		Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// save input PDB file to environment
		final String inputPdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
		try {
			req.getPdbFile().transferTo(env.getFile(inputPdbFilename));
		} catch (IOException e) {
			return new SubmittedResponse(null, "Could not save PDB file " + inputPdbFilename).badRequest();
		}

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create ppm service and run in background
		PPMService service = new PPMService(env, uploader, emailer, templateRenderer);
		service.setParams(new PPMParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
