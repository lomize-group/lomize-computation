package lomize.computation.controllers;

import static lomize.computation.services.cellpm.CellPMConfig.BUCKET_NAME;
import static lomize.computation.services.cellpm.CellPMConfig.BUCKET_REGION;
import static lomize.computation.services.cellpm.CellPMConfig.EMAIL_SUBJECT;
import static lomize.computation.services.cellpm.CellPMConfig.EXECUTABLE_NAME;
import static lomize.computation.services.cellpm.CellPMConfig.PROCESS_NAME;
import static lomize.computation.services.cellpm.CellPMConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.cellpm.CellPMConfig.SUBDIRECTORY_NAME;
import static lomize.computation.services.cellpm.CellPMConfig.TEMPLATE_FILENAME;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.CellPMRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.cellpm.CellPMParams;
import lomize.computation.services.cellpm.CellPMService;

@RestController
public class CellPMController {

	@CrossOrigin
	@PostMapping("/cellpm")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid final CellPMRequest req) {
		if (req.getPdbFile() == null && (req.getOutputPdbFilename() == null || req.getSequence() == null)) {
			final String badReq = "Must specify either 'pdbFile', or both 'outputPdbFilename' and 'sequence' in request";
			return new SubmittedResponse(null, badReq).badRequest();
		}

		// create environment
		Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// create subdirectory for output files
		env.createDirectory(SUBDIRECTORY_NAME);

		// if using a file, save that file in environment
		if (req.getPdbFile() != null) {
			final String pdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
			try {
				req.getPdbFile().transferTo(env.getFile(SUBDIRECTORY_NAME, pdbFilename));
			} catch (IOException e) {
				return new SubmittedResponse(null, "Could not save PDB file " + pdbFilename).badRequest();
			}
		}

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create cellpm service and run in background
		CellPMService service = new CellPMService(env, uploader, emailer, templateRenderer);
		service.setParams(new CellPMParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
