package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lomize.computation.services.cellpm.MembraneType;

@Data
public class CellPMRequest {
	@NotNull(message = "Must include numeric 'temperature' in request")
	private Double temperature;

	@NotNull(message = "Must include numeric 'ph' in request")
	private Double ph;

	@NotNull(message = "Must include numeric 'membraneType' in request (possible values: 'DOPC')")
	private MembraneType membraneType;

	private MultipartFile pdbFile;
	
	private String sequence;
	private String outputPdbFilename;

	private String userEmail;
}