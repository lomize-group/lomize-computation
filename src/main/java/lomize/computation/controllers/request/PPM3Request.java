package lomize.computation.controllers.request;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;
import lombok.Data;

import lomize.computation.services.ppm3.MembraneType;
import lomize.computation.services.ppm3.Membrane;

@Data
public class PPM3Request {
    @NotNull(message = "Must specify 'membranes' in request")
    private ArrayList<Membrane> membranes;

    @NotNull(message = "Must specify 'heteroatoms' in request")
    private Boolean heteroatoms;

    @NotNull(message = "Must include a PDB file 'file' in request")
    private String pdbFilename;

    @NotNull(message = "Must include a PDB file 'file' in request")
    private String pdbFile;

    private String userEmail;
}
