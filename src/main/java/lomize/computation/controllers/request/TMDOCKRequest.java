package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class TMDOCKRequest {
	@NotNull(message = "Must send 'sequence' in request")
	private String sequence;

	@Pattern(regexp = "^[a-zA-Z0-9_]{1,30}$", message = "Must only use alphanumeric characters in 'filename'")
	private String filename = "result";

	private String userEmail;
}