package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class PPMRequest {
	@NotNull(message = "Must specify 'heteroatoms' in request")
	private Boolean heteroatoms;

	@NotNull(message = "Must specify 'topologyIn' in request")
	private Boolean topologyIn;

	@NotNull(message = "Must include a PDB file 'file' in request")
	private MultipartFile pdbFile;
	
	private String userEmail;
}