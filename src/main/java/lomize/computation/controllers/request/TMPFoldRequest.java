package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class TMPFoldRequest {
	@NotNull(message = "Must send 'sequence' in request")
	private String segments;

	@NotNull(message = "Must send file 'pdbFile' in request")
	private MultipartFile pdbFile;

	private String userEmail;
}