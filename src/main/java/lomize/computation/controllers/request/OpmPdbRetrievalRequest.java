package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class OpmPdbRetrievalRequest {
	@NotNull(message = "must send String[] 'pdbCodes' in request")
	private String[] pdbCodes;
}