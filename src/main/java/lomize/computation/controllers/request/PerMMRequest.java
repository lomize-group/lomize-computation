package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lomize.computation.services.permm.OptimizationMethod;

@Data
public class PerMMRequest {
	@NotNull(message = "Must specify numeric 'temperature' in request")
	private Double temperature;

	@NotNull(message = "Must specify numeric 'ph' in request")
	private Double ph;

	@NotNull(message = "Must specify 'optimizationMethod' in request (possible values: 'drag', 'global')")
	private OptimizationMethod optimizationMethod;

	@NotNull(message = "Must specify boolean 'estimatePermeabilityPo' in request")
	private Boolean estimatePermeabilityPo;

	@NotNull(message = "Must include a PDB file 'pdbFile' in request")
	private MultipartFile pdbFile;

	private String userEmail;
	
}