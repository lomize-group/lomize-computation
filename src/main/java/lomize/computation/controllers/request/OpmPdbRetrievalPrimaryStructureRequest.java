package lomize.computation.controllers.request;

import lombok.Data;
import lomize.computation.services.opm.update.primary_structure.PartialPrimaryStructure;

@Data
public class OpmPdbRetrievalPrimaryStructureRequest {
	private PartialPrimaryStructure[] data;
}