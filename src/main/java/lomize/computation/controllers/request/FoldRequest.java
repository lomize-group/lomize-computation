package lomize.computation.controllers.request;

import java.util.ArrayList;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FoldRequest {
    @NotNull(message = "Must specify 'sequences' in request")
    private ArrayList<String> sequences;
    @NotNull(message = "Must specify 'numRelax' in request")
    private Integer numRelax;
    @NotNull(message = "Must specify 'templateMode' in request")
    private TemplateMode templateMode;
    @NotNull(message = "Must specify 'templateFiles' in request")
    private ArrayList<File> templateFiles;
    private MsaMode msaMode = MsaMode.MMSEQS2_UNIREF_ENV;
    private String msaFile; 
    @NotNull(message = "Must specify 'pairMode' in request")
    private PairMode pairMode;
    @NotNull(message = "Must specify 'numRecycles' in request")
    private NumRecycle numRecycles;
    @NotNull(message = "Must specify 'recycleEarlyStopTolerance' in request")
    private RecycleEarlyStopTolerance recycleEarlyStopTolerance;
    private MaxMsa maxMsa = MaxMsa.AUTO;
    private Integer randomSeed = 0;
    private Integer numSeeds = 1;
    private Boolean useDropout = false;
    private Boolean disableClusterProfile = false;
    private Boolean saveRecycles = false;
    private ModelType modelType = ModelType.AUTO;

    private String userEmail;

    public static enum TemplateMode {
        NONE,
        PDB70,
        CUSTOM;
    }

    public static enum MsaMode {
        MMSEQS2_UNIREF_ENV,
        MMSEQS2_UNIREF,
        SINGLE_SEQUENCE,
        CUSTOM;
    }

    public static enum PairMode {
        UNPAIRED_PAIRED,
        PAIRED,
        UNPAIRED;
    }

    public static enum NumRecycle {
        _1,
        _3,
        _6,
        _12,
        _24,
        _48;
    }

    public static enum RecycleEarlyStopTolerance {
        AUTO,
        _0_0,
        _0_2,
        _0_5,
        _0_7,
        _1_0;
    }

    public static enum MaxMsa {
        AUTO,
        _512_1024,
        _256_512,
        _64_128,
        _32_64,
        _16_32;
    }

    public static enum ModelType {
        AUTO,
        ALPHAFOLD2_PTM,
        ALPHAFOLD2_MULTIMER_V1,
        ALPHAFOLD2_MULTIMER_V2,
        ALPHAFOLD2_MULTIMER_V3;
    }

    @Data
    public static class File {
        private String name;
        private String contents;
    }
}
