package lomize.computation.controllers.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lomize.computation.services.fmap.MembraneType;
import lomize.computation.services.fmap.MicelleType;
import lomize.computation.services.fmap.ModelType;

@Data
public class FMAPRequest {
	@NotNull(message = "Must send 'model' in request")
	private ModelType model;

	@Pattern(regexp = "^[a-zA-Z0-9_]{1,30}$", message = "Must only use alphanumeric characters in 'filename'")
	private String outputFilename = "result";

	@NotNull(message = "Must specify 'gen3dModels' in request")
	private Boolean gen3dModels;

	@NotNull(message = "Must send 'temperature' in request")
	private Double temperature;

	@NotNull(message = "Must send 'ph' in request")
	private Double ph;

	@NotNull(message = "Must send 'sequence' in request")
	private String sequence;

	private MicelleType micelleType;
    private Integer micelleDiameter;
	private MembraneType membraneType;

	private String predefinedSegments;

	private String userEmail;

}
