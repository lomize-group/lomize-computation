package lomize.computation.controllers;

import static lomize.computation.services.tmdock.TMDOCKConfig.BUCKET_NAME;
import static lomize.computation.services.tmdock.TMDOCKConfig.BUCKET_REGION;
import static lomize.computation.services.tmdock.TMDOCKConfig.EMAIL_SUBJECT;
import static lomize.computation.services.tmdock.TMDOCKConfig.EXECUTABLE_NAME;
import static lomize.computation.services.tmdock.TMDOCKConfig.PROCESS_NAME;
import static lomize.computation.services.tmdock.TMDOCKConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.tmdock.TMDOCKConfig.TEMPLATE_FILENAME;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.TMDOCKRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.tmdock.TMDOCKParams;
import lomize.computation.services.tmdock.TMDOCKService;

@RestController
public class TMDOCKController {

	@CrossOrigin
	@PostMapping("/tmdock")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid @RequestBody final TMDOCKRequest req) {
		// create environment
		final Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create tmdock service and run in background
		TMDOCKService service = new TMDOCKService(env, uploader, emailer, templateRenderer);
		service.setParams(new TMDOCKParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);

		// return link to waiting page which will redirect to results when done
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
