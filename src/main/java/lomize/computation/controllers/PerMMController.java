package lomize.computation.controllers;

import static lomize.computation.services.permm.PerMMConfig.BUCKET_NAME;
import static lomize.computation.services.permm.PerMMConfig.BUCKET_REGION;
import static lomize.computation.services.permm.PerMMConfig.EMAIL_SUBJECT;
import static lomize.computation.services.permm.PerMMConfig.EXECUTABLE_NAME;
import static lomize.computation.services.permm.PerMMConfig.PROCESS_NAME;
import static lomize.computation.services.permm.PerMMConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.permm.PerMMConfig.TEMPLATE_FILENAME;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.PerMMRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.permm.PerMMParams;
import lomize.computation.services.permm.PerMMService;

@RestController
public class PerMMController {

	@CrossOrigin
	@PostMapping("/permm")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid final PerMMRequest req) {
		// create environment
		Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		final String pdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
		try {
			req.getPdbFile().transferTo(env.getFile(pdbFilename));
		} catch (IOException e) {
			e.printStackTrace();
			return new SubmittedResponse(null, "Could not save PDB file " + pdbFilename).badRequest();
		}

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create permm service and run in background
		PerMMService service = new PerMMService(env, uploader, emailer, templateRenderer);
		service.setParams(new PerMMParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
