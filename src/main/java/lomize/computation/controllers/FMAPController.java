package lomize.computation.controllers;

import static lomize.computation.services.fmap.FMAPConfig.BUCKET_NAME;
import static lomize.computation.services.fmap.FMAPConfig.BUCKET_REGION;
import static lomize.computation.services.fmap.FMAPConfig.EMAIL_SUBJECT;
import static lomize.computation.services.fmap.FMAPConfig.EXECUTABLE_NAME;
import static lomize.computation.services.fmap.FMAPConfig.PROCESS_NAME;
import static lomize.computation.services.fmap.FMAPConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.fmap.FMAPConfig.TEMPLATE_FILENAME;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.AmazonEmailClient;
import lomize.computation.common.cloud.AmazonS3StorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.controllers.request.FMAPRequest;
import lomize.computation.response.Response;
import lomize.computation.response.SubmittedResponse;
import lomize.computation.services.fmap.FMAPParams;
import lomize.computation.services.fmap.FMAPService;
import lomize.computation.services.fmap.ModelType;

@RestController
public class FMAPController {

	@CrossOrigin
	@PostMapping("/fmap")
	@Validated
	@ResponseBody
	public ResponseEntity<Response> post(@Valid @RequestBody final FMAPRequest req) {
		// ensure the micelle/membrane type corresponding to selected model is sent with
		// request
		if (req.getModel() == ModelType.PEPTIDE_IN_MICELLES && req.getMicelleType() == null) {
			return new SubmittedResponse(null, "Must send micelle type when selecting micelle model").badRequest();
		}
		if (req.getModel() == ModelType.PEPTIDE_IN_MEMBRANES && req.getMembraneType() == null) {
			return new SubmittedResponse(null, "Must send membrane type when selecting membrane model").badRequest();
		}

		// create environment
		final Environment env = new Environment();
		env.createIn(Util.getProcessDirectory(PROCESS_NAME));
		env.copyChildren(Util.getExecutableDirectory(PROCESS_NAME));
		env.setExecutableName(EXECUTABLE_NAME);

		// create template renderer
		TemplateRenderer templateRenderer = new TemplateRenderer(TEMPLATE_FILENAME);

		// create file uploader
		AmazonS3StorageClient uploader = new AmazonS3StorageClient(BUCKET_NAME, BUCKET_REGION);
		uploader.setDestPath(env.getEnvName());

		// create emailer
		AmazonEmailClient emailer = null;
		if (req.getUserEmail() != null) {
			emailer = new AmazonEmailClient(req.getUserEmail(), EMAIL_SUBJECT);
		}

		// create FMAP service and run in background
		FMAPService service = new FMAPService(env, uploader, emailer, templateRenderer);
		service.setParams(new FMAPParams(req));
		final int nJobs = service.runInNewThread();

		final String resultsUrl = uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
		return new SubmittedResponse(resultsUrl, nJobs).ok();
	}
}
