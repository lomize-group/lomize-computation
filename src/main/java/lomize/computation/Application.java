package lomize.computation;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Value;

@SpringBootApplication
@RestController
public class Application {

    @Value("${lomize.threadPoolSize}")
    private Integer threadPoolSize;

    @Value("${lomize.timeLimit}")
    private Integer timeLimit;

    @RequestMapping("/")
    public String home() {
        return "The server is running. Try sending a request to one of the endpoints.";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean("threadPool")
    public ThreadPoolExecutor getThreadPool() {
        // the upcast from ExecutorService is valid because we know that the library implementation
        // returns a ThreadPoolExecutor:
        // https://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/concurrent/Executors.java#l73
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(threadPoolSize);
    }

    @Bean("timeLimit")
    public Integer getTimeLimit() {
        return timeLimit;
    }
}
