package lomize.computation.response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SubmittedResponse extends Response {

	private static final String DEFAULT_MESSAGE = "The job has been submitted.";
	public static final String WAITING_URL_BASE = "https://lomize-common.s3.us-east-2.amazonaws.com/waiting.html";

	protected String resultsUrl;
	protected String waitingUrl;
	protected String message;

	public SubmittedResponse() {
		this.resultsUrl = null;
		this.waitingUrl = null;
		this.message = DEFAULT_MESSAGE;
	}

	public SubmittedResponse(final String resultsUrl) {
        this(resultsUrl, null);
	}

    public SubmittedResponse(final String resultsUrl, final int nJobs) {
        this(
            resultsUrl,
            nJobs == 0 ? null : String.format(
                "At the time of submission, there were %d jobs waiting to run, so you may need to "
                + "wait extra time for your results.", nJobs
            )
        );
    }

	public SubmittedResponse(final String resultsUrl, final String message) {
		this.resultsUrl = resultsUrl;
		this.waitingUrl = calcWaitingUrl(resultsUrl);
        this.message = message != null ? message : DEFAULT_MESSAGE;
	}

	private static String calcWaitingUrl(final String resultsUrl) {
		if (resultsUrl == null) {
			return null;
		}
		try {
			return WAITING_URL_BASE + "?results=" + URLEncoder.encode(resultsUrl, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e.getCause());
		}
	}

}
