package lomize.computation.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class Response {

	public ResponseEntity<Response> ok() {
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<Response> badRequest() {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(this);
	}

}