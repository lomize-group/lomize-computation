package lomize.computation.services.ppm;

import static lomize.computation.services.ppm.PPMConfig.RESULTS_PAGE_FILENAME;

import java.io.File;
import java.io.IOException;

import org.apache.http.entity.ContentType;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

public class PPMService extends AbstractExecutionService<PPMParams> {

	public PPMService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	private static String getOutputFilename(final String inputFilename) {
		String[] splitFilename = inputFilename.split("\\.");
		splitFilename[0] += "out";
		return String.join(".", splitFilename);
	}

	public Void call() {
		// constant as transmembrane for now
		final String proteinType = "transmembrane";

		// set input for executable
		this.env.setExecutableInput(proteinType, this.params.getTopologyInOut().toString(),
				this.params.getHeteroatoms().toString(), "./" + this.params.getInputPdbFilename());

		// run executable
		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing ppm: " + e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException("Error executing ppm: " + e.getMessage());
		}

		final String outputPdbFilename = getOutputFilename(this.params.getInputPdbFilename());
		final File outputPdbFile = this.env.getFile(outputPdbFilename);
		final boolean outputPdbFileExists = outputPdbFile.exists();

		// upload output pdb file
		if (outputPdbFileExists) {
			this.uploader.uploadFile(outputPdbFile, outputPdbFilename, null);
		}

		PPMTemplateModel templateModel = PPMTemplateModel.fromOutput(this.env.readOutputLines());

		final String pdbFileUrl = this.uploader.getFileDestinationPath(outputPdbFilename);
		if (outputPdbFileExists) {
			templateModel.setPdbFileUrl(pdbFileUrl);
		}

		// render and upload HTMl template
		final String resultsPageHtml = this.templateRenderer.render(templateModel);
		this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

		// send email
		if (this.emailer != null) {
			String message = "Your PPM computation has finished.\n";

			if (outputPdbFileExists) {
				message += "The generated PDB is accessible at: " + pdbFileUrl + "\n";
			}
			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			message += "The generated statistics are accessible at: " + resultsUrl + "\n";

			this.emailer.send(message);
		}

		return null;
	}
}
