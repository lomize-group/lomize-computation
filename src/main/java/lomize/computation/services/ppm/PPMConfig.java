package lomize.computation.services.ppm;

import com.amazonaws.regions.Regions;

public class PPMConfig {
	public static final String RESULTS_PAGE_FILENAME = "ppm_results.html";
	public static final String TEMPLATE_FILENAME = "ppm_results.ftl";

	public static final String BUCKET_NAME = "ppm-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "PPM Computation Results";

	public static final String EXECUTABLE_NAME = "transversion";
	public static final String PROCESS_NAME = "ppm";

	public static final String INPUT_FILENAME = "input.in";
    public static final String OUTPUT_FILENAME = "output.out";
    
    public static final String getIcn3dUrl(final String pdbFileUrl) {
        return ICN3D_BASE_URL + pdbFileUrl;
    }

	public static final String getJmolUrl(final String pdbFileUrl) {
		return JMOL_BASE_URL + pdbFileUrl;
	}

    private static final String ICN3D_BASE_URL = "https://www.ncbi.nlm.nih.gov/Structure/icn3d/full.html?type=pdb&url=";
    private static final String JMOL_BASE_URL = "https://bioinformatics.org/firstglance/fgij/fg.htm?mol=";
}
