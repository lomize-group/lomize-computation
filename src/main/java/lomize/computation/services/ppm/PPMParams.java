package lomize.computation.services.ppm;

import lombok.Data;
import lomize.computation.common.TopologyInOut;
import lomize.computation.common.Util;
import lomize.computation.common.YesNo;
import lomize.computation.controllers.request.PPMRequest;

@Data
public class PPMParams {
	private TopologyInOut topologyInOut;
	private YesNo heteroatoms;
	private String inputPdbFilename;

	public PPMParams(final PPMRequest req) {
		this.topologyInOut = req.getTopologyIn() ? TopologyInOut.IN : TopologyInOut.OUT;
		this.heteroatoms = YesNo.fromBoolean(req.getHeteroatoms());
		this.inputPdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
	}
}