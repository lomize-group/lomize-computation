package lomize.computation.services.ppm;

import java.util.ArrayList;

import lombok.Data;
import lombok.AccessLevel;
import lombok.Setter;
import lomize.computation.common.Measurement;

@Data
public class PPMTemplateModel {

	@Data
	public static class OutputData {
		private String subunits;
		private String tilt;
		private String segments;
	}

	@Setter(AccessLevel.NONE)
	private ArrayList<String> messageList = new ArrayList<>();

    @Data
    public static class Section {
        private String name;
        private ArrayList<OutputData> data = new ArrayList<>();
    }

    @Setter(AccessLevel.NONE)
    private ArrayList<Section> sections = new ArrayList<>();

    public void addSection(final Section section) {
        sections.add(section);
    }

    public void addData(final OutputData data) {
        sections.get(sections.size() - 1).data.add(data);
    }

	private Measurement tilt = null;
	private Measurement thickness = null;
	private String gibbs = null;

	@Setter(AccessLevel.NONE)
    private String pdbFileUrl = null;
    @Setter(AccessLevel.NONE)
    private String icn3dUrl = null;
	@Setter(AccessLevel.NONE)
	private String jmolUrl = null;

	public void setPdbFileUrl(final String pdbFileUrl) {
		this.pdbFileUrl = pdbFileUrl;
        this.icn3dUrl = PPMConfig.getIcn3dUrl(pdbFileUrl);
        this.jmolUrl = PPMConfig.getJmolUrl(pdbFileUrl);
	}

	public void addMessage(final String message) {
		messageList.add(message);
	}

	public static PPMTemplateModel fromOutput(final ArrayList<String> ppmOutput) {
		PPMTemplateModel templateModel = new PPMTemplateModel();
		boolean readingMessages = true;
		boolean readingMeasurements = false;
        final String residuesSegmentsName = "Embedded_residues:";
        final String transmembraneSegmentsName = "Transmembrane_secondary_structure_segments:";
		for (final String line : ppmOutput) {
			// if messages are currently being read and we encounter a "#" in
			// the output, we have gotten to the data section of the PPM output.
			if (readingMessages && line.startsWith("#")) {
				readingMeasurements = true;
				readingMessages = false;
			}

			if (readingMessages) {
				templateModel.addMessage(line);
			} else if (readingMeasurements) {
                // table at the top with thickness, Gibbs, and Tilt Angle
				String[] measurements = line.split(";");
                String nextName = measurements[measurements.length - 1].trim();

                if (nextName.equals(residuesSegmentsName) || nextName.equals(transmembraneSegmentsName)) {
                    readingMeasurements = false;
                    Section section = new PPMTemplateModel.Section();
                    section.setName(nextName);
                    templateModel.addSection(section);
                    continue;
                }

				if (measurements.length > 2) {
					Measurement thickness = new Measurement();
					thickness.setValue(measurements[1].trim());
					thickness.setError(measurements[2].trim());
					templateModel.setThickness(thickness);
				}
				if (measurements.length > 4) {
					Measurement tilt = new Measurement();
					tilt.setValue(measurements[3].trim());
					tilt.setError(measurements[4].trim());
					templateModel.setTilt(tilt);
				}
				if (measurements.length > 5) {
					templateModel.setGibbs(measurements[5].trim());
				}
            // Residues table
			} else {
                String[] splitLine = line.split(";");
                String nextName = splitLine[splitLine.length - 1].trim();

                if (nextName.equals(residuesSegmentsName) || nextName.equals(transmembraneSegmentsName)) {
                    Section section = new PPMTemplateModel.Section();
                    section.setName(nextName);
                    templateModel.addSection(section);
                    continue;
                }

                OutputData data = new PPMTemplateModel.OutputData();

                if (splitLine.length > 1) {
                    data.setSubunits(splitLine[1].trim());
                }
                if (splitLine.length > 2) {
                    data.setTilt(splitLine[2].trim());
                }
                if (splitLine.length > 3) {
                    data.setSegments(splitLine[3].trim());
                }

                templateModel.addData(data);
			}
		}

		return templateModel;
	}
}
