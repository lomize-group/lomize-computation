package lomize.computation.services;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Future;

import lombok.Getter;
import org.springframework.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.ApplicationContextUtils;

// we rely on there being two similar classes that do two similar things so that we can
// define our own abstraction layer.
public abstract class AbstractExecutionService<ParamType> extends Thread implements Callable<Void> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractExecutionService.class);

	protected final Environment env;
	protected final IFileStorageClient uploader;
	protected final IEmailClient emailer;
	protected final TemplateRenderer templateRenderer;

	@Getter
	protected ParamType params = null;

    protected Process process = null;

    public Integer getTimeLimit() {
        ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
        return (Integer) ctx.getBean("timeLimit");
    }

	public AbstractExecutionService(final Environment env, final IFileStorageClient uploader,
			final IEmailClient emailer, final TemplateRenderer templateRenderer) {
		this.env = env;
		this.uploader = uploader;
		this.emailer = emailer;
		this.templateRenderer = templateRenderer;
	}

	public void setParams(ParamType params) {
		this.params = params;
	}

    // returns the number of waiting threads before the new job starts
	public int runInNewThread() {
        ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) ctx.getBean("threadPool");
        int nJobs = threadPool.getQueue().size();
        // calls run method defined here.
        // we can call this thread outside the thread pool since it doesn't do much. it simply invokes (blocking) the
        // thread pool which has a limited number of workers.
        // in fact, we should not invoke the thread pool from the thread pool or else that is a deadlock.
        this.start();
        return nJobs;
	}

    // must be public to correctly implement interface, but really it needs to be private
    public void run() {
        // this method is needed since invokeAll blocks until the thread is complete
        Future<Void> future;
        ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) ctx.getBean("threadPool");
        Integer timeLimit = this.getTimeLimit();

        try {
            // calls the abstract call method
            future = threadPool.invokeAll(List.of(this), timeLimit, TimeUnit.DAYS).get(0);
        } catch (InterruptedException exc) {
            logger.debug("Executable interrupted");
            return;
        }

        if (future.isCancelled()) {
            logger.debug("Executable exceeded time limit");
            if (this.process != null) {
                logger.debug("killing process " + this.process.pid());
                this.process.destroy();
            }

            // send failure email
            if (this.emailer != null) {
                String message = "Your process exceeded the time limit: ";
                message += this.env.getEnvDirectory();
                this.emailer.send(message);
            }
        } else {
            logger.debug("Executable finished within time limit");
        }
    }
}
