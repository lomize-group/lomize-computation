package lomize.computation.services.opm.update.unformatted;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lomize.computation.services.opm.update.PdbCodeSource;

@Getter
@Setter
public class UnformattedPdbData {
	private static final String SEPARATOR = StringUtils.repeat("-", 30);

	private static final String[] FILE_KEY = { "input pdb code", "pdb codes of OPM entries with the same uniprot codes",
			"pdb codes of OPM entries with the same pfam codes (left blank if any entries are listed on the previous line)",
			"uniprot codes taken from protein data bank", "protein name taken from uniprot",
			"resolution taken from protein data bank", "subunit letter taken from protein data bank",
			"topology (in or out) taken from OPM",
			"membrane taken from OPM (taken from uniprot if no matches are found in OPM)", "species taken from uniprot",
			"family taken from OPM (taken from uniprot if no matches are found in OPM)", SEPARATOR };

	public static String getHeader() {
		return String.join("\n", FILE_KEY);
	}

	protected String inputPdbCode;
	protected String proteinName;
	protected String resolution;
	protected String subunit;
	protected Boolean topology;
	protected String membrane;
	protected String species;
	protected String family;
	protected String[] uniprotCodes;
	protected String[] pfamCodes;

	protected PdbCodeSource resultPdbCodeSource;
	protected String[] resultPdbCodes;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.inputPdbCode).append("\n");

		if (this.resultPdbCodeSource == PdbCodeSource.Uniprot) {
			sb.append(String.join(" ", this.resultPdbCodes));
		} else if (this.resultPdbCodeSource == PdbCodeSource.Pfam) {
			sb.append("\n").append(String.join(" ", this.resultPdbCodes));
		}
		sb.append("\n");

		sb.append(String.join(" ", this.uniprotCodes)).append("\n");
		sb.append(this.proteinName).append("\n");
		sb.append(this.resolution).append("\n");
		sb.append(this.subunit).append("\n");
		sb.append(this.topology).append("\n");
		sb.append(this.membrane).append("\n");
		sb.append(this.species).append("\n");
		sb.append(this.family).append("\n");
		sb.append(SEPARATOR);

		return sb.toString();
	}
}