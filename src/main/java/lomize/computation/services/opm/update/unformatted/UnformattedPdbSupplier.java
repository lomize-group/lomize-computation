package lomize.computation.services.opm.update.unformatted;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Supplier;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.Data;
import lomize.computation.common.client.OpmClient;
import lomize.computation.common.client.RcsbClient;
import lomize.computation.common.client.RcsbClient.RcsbMolResponse;
import lomize.computation.common.client.RcsbClient.RcsbPdbResponse;
import lomize.computation.common.client.UniprotClient;
import lomize.computation.common.client.UniprotClient.UniprotResponse;
import lomize.computation.services.opm.update.PdbCodeSource;

public class UnformattedPdbSupplier implements Supplier<UnformattedPdbData> {
	private final String pdbCode;

	private final OpmClient opmClient;
	private final RcsbClient rcsbClient;
	private final UniprotClient uniprotClient;

	public UnformattedPdbSupplier(final String pdbCode) {
		HttpClient client = HttpClientBuilder.create().build();
		this.pdbCode = pdbCode;
		this.opmClient = new OpmClient(client);
		this.rcsbClient = new RcsbClient(client);
		this.uniprotClient = new UniprotClient(client);
	}

	@Data
	public static class OpmEntry {
		private Boolean topology;
		private String membrane;
		private String family;
		private String[] pdbCodes;
	}

	private static OpmEntry convertToOpmEntry(final JSONArray matches) throws JSONException {
		OpmEntry result = new OpmEntry();

		JSONObject first = matches.getJSONObject(0);

		// get topology of first one (should all be the same)
		result.setTopology(first.getBoolean("topology_show_in"));

		// get membrane of first one (should all be the same)
		Integer memId = first.getInt("membrane_id");
		String memName = first.getString("membrane_name_cache");
		result.setMembrane(memName + " (OPM ID = " + memId + ")");

		// get family of first one (should all be the same)
		Integer famId = first.getInt("family_id");
		String famName = first.getString("family_name_cache");
		result.setFamily(famName + " (OPM ID = " + famId + ")");

		// get all pdb codes and sort them
		Set<String> pdbCodeSet = new HashSet<>();
		for (int i = 0; i < matches.length(); ++i) {
			pdbCodeSet.add(matches.getJSONObject(i).getString("pdbid"));
		}
		String[] pdbCodes = pdbCodeSet.toArray(new String[pdbCodeSet.size()]);
		Arrays.sort(pdbCodes);
		result.setPdbCodes(pdbCodes);

		return result;
	}

	public UnformattedPdbData get() {
		UnformattedPdbData result = new UnformattedPdbData();

		result.setInputPdbCode(this.pdbCode);

		try {
			RcsbMolResponse rcsbMol = this.rcsbClient.fetchMol(this.pdbCode);
			result.setUniprotCodes(rcsbMol.getUniprotCodes());
			result.setSubunit(rcsbMol.getSubunit());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		try {
			RcsbPdbResponse rcsbPdb = this.rcsbClient.fetchPdb(this.pdbCode);
			result.setResolution(rcsbPdb.getResolution());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		try {
			UniprotResponse uniprot = null;
			for (String uniprotCode : result.getUniprotCodes()) {
				try {
					uniprot = this.uniprotClient.getEntry(uniprotCode);
					break;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (uniprot == null) {
				throw new Exception("no data found from uniprot");
			}

			result.setSpecies(uniprot.getShortSpeciesName());
			result.setProteinName(uniprot.getProteinName());
			result.setPfamCodes(uniprot.getPfamCodes());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		// try match by uniprot first
		Set<String> resultPdbCodes = new HashSet<>();
		for (final String uniprotCode : result.getUniprotCodes()) {
			try {
				OpmEntry opm = convertToOpmEntry(this.opmClient.getPrimaryStructuresByUniprot(uniprotCode));
				if (resultPdbCodes.isEmpty()) {
					result.setFamily(opm.getFamily());
					result.setMembrane(opm.getMembrane());
					result.setTopology(opm.getTopology());
				}
				for (final String pdbCode : opm.getPdbCodes()) {
					resultPdbCodes.add(pdbCode);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			result.setResultPdbCodeSource(PdbCodeSource.Uniprot);
		}

		if (resultPdbCodes.isEmpty()) {
			// try match by pfam if match by uniprot failed
			for (final String pfamCode : result.getPfamCodes()) {
				try {
					OpmEntry opm = convertToOpmEntry(this.opmClient.getPrimaryStructuresByPfam(pfamCode));
					if (resultPdbCodes.isEmpty()) {
						result.setFamily(opm.getFamily());
						result.setMembrane(opm.getMembrane());
						result.setTopology(opm.getTopology());
					}
					for (final String pdbCode : opm.getPdbCodes()) {
						resultPdbCodes.add(pdbCode);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			result.setResultPdbCodeSource(PdbCodeSource.Pfam);
		}
		result.setResultPdbCodes(resultPdbCodes.toArray(new String[resultPdbCodes.size()]));

		return result;
	}
}