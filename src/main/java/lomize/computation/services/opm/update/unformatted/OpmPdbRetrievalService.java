package lomize.computation.services.opm.update.unformatted;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import lombok.Getter;
import lombok.Setter;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.services.opm.update.OpmUpdateConfig;

public class OpmPdbRetrievalService implements Runnable {

	private final IFileStorageClient uploader;
	private final Executor requestPool = Executors.newFixedThreadPool(3);
	private final ArrayList<UnformattedPdbData> results = new ArrayList<>();
	private @Getter CompletableFuture<Void> runningFuture;
	private @Setter String[] pdbCodes;

	public OpmPdbRetrievalService(final IFileStorageClient uploader) {
		this.uploader = uploader;
	}

	private void addResultIfNotNull(final UnformattedPdbData result) {
		if (result != null) {
			this.results.add(result);
		}
	}

	private void afterRequestsComplete() {
		// sort results alphabetically by pdb code
		this.results.sort((a, b) -> a.getInputPdbCode().compareToIgnoreCase(b.getInputPdbCode()));

		StringBuilder sb = new StringBuilder();

		// append the header
		sb.append(UnformattedPdbData.getHeader()).append("\n");

		// append each row/section
		for (final UnformattedPdbData r : this.results) {
			sb.append(r.toString()).append("\n");
		}

		// upload file
		this.uploader.uploadFile(sb.toString(), OpmUpdateConfig.UNFORMATTED_RESULTS_FILENAME, null);
	}

	public void run() {
		ArrayList<CompletableFuture<Void>> cfs = new ArrayList<>();

		// add futures to list
		// add each result to result list when futures are finished
		for (final String pdbCode : this.pdbCodes) {
			UnformattedPdbSupplier supplier = new UnformattedPdbSupplier(pdbCode);
			CompletableFuture<Void> cf = CompletableFuture.supplyAsync(supplier, this.requestPool)
					.thenAccept(this::addResultIfNotNull);
			cfs.add(cf);
		}

		// when all futures finished, write data to file
		this.runningFuture = CompletableFuture.allOf(cfs.toArray(new CompletableFuture[cfs.size()]))
				.thenAccept(_void -> this.afterRequestsComplete());
	}

}
