package lomize.computation.services.opm.update.primary_structure;

import lombok.Data;

@Data
public class PrimaryStructure {

	private String pdbCode;
	private String name;
	private String topologySubunit;
	private Boolean topologyShowIn;
	private Double thickness;
	private Double thicknessError;
	private Integer tilt;
	private Integer tiltError;
	private Double gibbs;

	private String resolution = "";
	private String[] uniprotCodes = {};

	private Integer speciesId = null;
	private Integer membraneId = null;
	private Integer familyId = null;
	private Integer superfamilyId = null;
	private Integer classId = null;
	private Integer typeId = null;
}