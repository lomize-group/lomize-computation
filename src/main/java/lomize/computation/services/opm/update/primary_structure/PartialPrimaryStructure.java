package lomize.computation.services.opm.update.primary_structure;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lomize.computation.common.TopologyInOut;

@Data
public class PartialPrimaryStructure {
	@NotNull(message = "must include String 'pdbCode'")
	private String pdbCode;

	private String refPdb;

	@NotNull(message = "must include String 'name'")
	private String name;

	@NotNull(message = "must include String 'subunit'")
	private String subunit;

	@NotNull(message = "must include String 'topology'")
	private TopologyInOut topology;

	@NotNull(message = "must include Double 'thickness'")
	private Double thickness;

	@NotNull(message = "must include Double 'thicknessError'")
	private Double thicknessError;

	@NotNull(message = "must include Integer 'tilt'")
	private Integer tilt;

	@NotNull(message = "must include Integer 'tiltError'")
	private Integer tiltError;

	@NotNull(message = "must include Double 'gibbs'")
	private Double gibbs;
}