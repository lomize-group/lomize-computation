package lomize.computation.services.opm.update.primary_structure;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Objects;

import com.opencsv.CSVWriter;

import org.json.JSONException;
import org.json.JSONObject;

import lomize.computation.common.Util;
import lomize.computation.common.client.OpmClient.OpmNames;

public class PrimaryStructureSerializer {

	public static final String[] CSV_HEADER = new String[] { OpmNames.NAME, OpmNames.PDBCODE, OpmNames.RESOLUTION,
			OpmNames.TOPOLOGY_SUBUNIT, OpmNames.TOPOLOGY_SHOW_IN, OpmNames.THICKNESS, OpmNames.THICKNESSERROR,
			OpmNames.TILT, OpmNames.TILTERROR, OpmNames.GIBBS, OpmNames.MEMBRANE_ID, OpmNames.SPECIES_ID,
			OpmNames.FAMILY_ID, OpmNames.SUPERFAMILY_ID, OpmNames.CLASSTYPE_ID, OpmNames.TYPE_ID,
			OpmNames.UNIPROT_CODE };

	public static PrimaryStructure deserialize(final JSONObject json) throws JSONException {
		PrimaryStructure result = new PrimaryStructure();
		result.setTopologyShowIn(json.getBoolean(OpmNames.TOPOLOGY_SHOW_IN));
		result.setTypeId(json.getInt(OpmNames.TYPE_ID));
		result.setClassId(json.getInt(OpmNames.CLASSTYPE_ID));
		result.setSuperfamilyId(json.getInt(OpmNames.SUPERFAMILY_ID));
		result.setFamilyId(json.getInt(OpmNames.FAMILY_ID));
		result.setMembraneId(json.getInt(OpmNames.MEMBRANE_ID));
		result.setSpeciesId(json.getInt(OpmNames.SPECIES_ID));
		result.setGibbs(json.getDouble(OpmNames.GIBBS));
		result.setThickness(json.getDouble(OpmNames.THICKNESS));
		result.setThicknessError(json.getDouble(OpmNames.THICKNESSERROR));
		result.setTilt(json.getInt(OpmNames.TILT));
		result.setTiltError(json.getInt(OpmNames.TILTERROR));
		result.setName(json.getString(OpmNames.NAME));
		result.setResolution(json.getString(OpmNames.RESOLUTION));
		result.setPdbCode(json.getString(OpmNames.PDBCODE));
		result.setTopologySubunit(json.getString(OpmNames.TOPOLOGY_SUBUNIT));
		result.setUniprotCodes(Util.toStringArray(json.getJSONArray(OpmNames.UNIPROT_CODES)));
		return result;
	}

	private static String MANUAL_CODE = "m";

	public static String serializeCsv(final PrimaryStructure p) {
		final StringWriter s = new StringWriter();
		try (final CSVWriter writer = new CSVWriter(s)) {

			// write in same order as headers
			writer.writeNext(new String[] { Objects.toString(p.getName(), ""), Objects.toString(p.getPdbCode(), ""),
					Objects.toString(p.getResolution(), ""), Objects.toString(p.getTopologySubunit(), ""),
					Objects.toString(p.getTopologyShowIn(), ""), Objects.toString(p.getThickness(), ""),
					Objects.toString(p.getThicknessError(), ""), Objects.toString(p.getTilt(), ""),
					Objects.toString(p.getTiltError(), ""), Objects.toString(p.getGibbs(), ""),
					Objects.toString(p.getMembraneId(), MANUAL_CODE), Objects.toString(p.getSpeciesId(), MANUAL_CODE),
					Objects.toString(p.getFamilyId(), MANUAL_CODE), Objects.toString(p.getSuperfamilyId(), ""),
					Objects.toString(p.getClassId(), ""), Objects.toString(p.getTypeId(), ""),
					String.join(" ", p.getUniprotCodes()) });
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s.toString();
	}

	public static String getCsvHeader() {
		return String.join(",", CSV_HEADER);
	}

	public static String serializeCsv(final Collection<PrimaryStructure> primaryStructures) {
		StringBuilder sb = new StringBuilder();
		sb.append(getCsvHeader()).append("\n");
		for (final PrimaryStructure p : primaryStructures) {
			sb.append(serializeCsv(p));
		}
		return sb.toString();
	}
}