package lomize.computation.services.opm.update;

public enum PdbCodeSource {
	Uniprot, Pfam
};