package lomize.computation.services.opm.update.primary_structure;

import static lomize.computation.services.opm.update.OpmUpdateConfig.CSV_RESULTS_FILENAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.NEW_FAMILIES_FILENAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.NEW_SPECIES_FILENAME;
import static lomize.computation.services.opm.update.OpmUpdateConfig.RESULTS_PAGE_FILENAME;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.http.entity.ContentType;

import lombok.Getter;
import lombok.Setter;
import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.UrlList;
import lomize.computation.common.cloud.IFileStorageClient;

public class PrimaryStructureRetrievalService implements Runnable {

	public static final int CONCURRENCY = 3;
	private final IFileStorageClient uploader;
	private final TemplateRenderer templateRenderer;
	private final Executor requestPool = Executors.newFixedThreadPool(CONCURRENCY);
	private @Getter CompletableFuture<Void> runningFuture;
	private @Setter PartialPrimaryStructure[] inputData;
	private final ArrayList<NewFamily> newFamilies;
	private final Set<String> newSpecies;

	private final ArrayList<PrimaryStructure> results = new ArrayList<>();

	public PrimaryStructureRetrievalService(final TemplateRenderer templateRenderer,
			final IFileStorageClient uploader) {
		this.templateRenderer = templateRenderer;
		this.uploader = uploader;
		this.newFamilies = new ArrayList<>();
		this.newSpecies = new HashSet<>();
	}

	protected void addResultIfNotNull(final PrimaryStructureResult result) {
		if (result != null) {
			this.results.add(result.getPrimaryStructure());
			this.newFamilies.add(result.getNewFamilies());
			this.newSpecies.addAll(result.getNewSpecies());
		}
	}

	protected void afterRequestsComplete() {
		// sort results alphabetically by pdb code
		this.results.sort((a, b) -> a.getPdbCode().compareToIgnoreCase(b.getPdbCode()));

		// create CSV
		final String csvFileString = PrimaryStructureSerializer.serializeCsv(this.results);
		UrlList urlList = new UrlList();

		// upload data files
		if (!this.newFamilies.isEmpty()) {
			Collections.sort(this.newFamilies, (a, b) -> a.getPdbCode().compareToIgnoreCase(b.getPdbCode()));
			StringJoiner lineJoiner = new StringJoiner("\n");
			for (final NewFamily fam : this.newFamilies) {
				if (!fam.getPfamCodes().isEmpty()) {
					String[] pfams = fam.getPfamCodes().toArray(new String[fam.getPfamCodes().size()]);
					Arrays.sort(pfams);
					StringJoiner spaceJoiner = new StringJoiner(" ");
					spaceJoiner.add(fam.getPdbCode()).add(String.join(" ", pfams));
					lineJoiner.add(spaceJoiner.toString());
				}
			}

			this.uploader.uploadFile(lineJoiner.toString(), NEW_FAMILIES_FILENAME, null);
			urlList.add(NEW_FAMILIES_FILENAME, this.uploader.getFileDestinationPath(NEW_FAMILIES_FILENAME));
		}
		if (!this.newSpecies.isEmpty()) {
			final String[] speciesNames = this.newSpecies.toArray(new String[this.newSpecies.size()]);
			Arrays.sort(speciesNames);
			this.uploader.uploadFile(String.join("\n", speciesNames), NEW_SPECIES_FILENAME, null);
			urlList.add(NEW_SPECIES_FILENAME, this.uploader.getFileDestinationPath(NEW_SPECIES_FILENAME));
		}

		this.uploader.uploadFile(csvFileString, CSV_RESULTS_FILENAME, null);
		urlList.add(CSV_RESULTS_FILENAME, this.uploader.getFileDestinationPath(CSV_RESULTS_FILENAME));

		// upload HTML page
		final String templateString = this.templateRenderer.render(urlList);
		this.uploader.uploadFile(templateString, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);
	}

	public void run() {
		ArrayList<CompletableFuture<Void>> cfs = new ArrayList<>();

		// add futures to list
		// add each result to result list when futures are finished
		for (final PartialPrimaryStructure pstruct : this.inputData) {
			PrimaryStructureSupplier supplier = new PrimaryStructureSupplier(pstruct);
			CompletableFuture<Void> cf = CompletableFuture.supplyAsync(supplier, this.requestPool)
					.thenAccept(this::addResultIfNotNull);
			cfs.add(cf);
		}

		// when all futures finished, write data to file
		this.runningFuture = CompletableFuture.allOf(cfs.toArray(new CompletableFuture[cfs.size()]))
				.thenAccept(v -> this.afterRequestsComplete());
	}
}