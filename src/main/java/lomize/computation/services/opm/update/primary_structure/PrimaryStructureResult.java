package lomize.computation.services.opm.update.primary_structure;

import java.util.ArrayList;

import lombok.Data;

@Data
public class PrimaryStructureResult {
	private PrimaryStructure primaryStructure;
	private NewFamily newFamilies;
	private ArrayList<String> newSpecies;
}