package lomize.computation.services.opm.update;

import java.nio.file.Paths;

import com.amazonaws.regions.Regions;

public class OpmUpdateConfig {
	public static final String PROCESS_NAME = "opm";

	public static final String BUCKET_NAME = "lomize-common";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String UNFORMATTED_RESULTS_FILENAME = "pdb_info.out";
	public static final String CSV_RESULTS_FILENAME = "pdb_info.csv";
	public static final String RESULTS_PAGE_FILENAME = "pdb_info.html";
	public static final String NEW_FAMILIES_FILENAME = "new_families.txt";
	public static final String NEW_SPECIES_FILENAME = "new_species.txt";
	public static final String RESULTS_FOLDER_NAME = "fetch_pdb_info_results";

	public static final String getResultFolderPath(final String subFolderName) {
		return Paths.get(RESULTS_FOLDER_NAME, subFolderName).toString();
	}
}