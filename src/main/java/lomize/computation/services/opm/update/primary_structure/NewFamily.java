package lomize.computation.services.opm.update.primary_structure;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;

public class NewFamily {
	@Getter
	private final String pdbCode;

	@Getter
	private final Set<String> pfamCodes = new HashSet<>();

	public NewFamily(final String pdbCode) {
		this.pdbCode = pdbCode;
	}

	public void addPfamCode(final String pfamCode) {
		this.pfamCodes.add(pfamCode);
	}
}