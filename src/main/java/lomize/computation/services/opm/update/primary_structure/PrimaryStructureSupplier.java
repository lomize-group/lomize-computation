package lomize.computation.services.opm.update.primary_structure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import javax.xml.ws.http.HTTPException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import lomize.computation.common.TopologyInOut;
import lomize.computation.common.Util;
import lomize.computation.common.client.OpmClient;
import lomize.computation.common.client.RcsbClient;
import lomize.computation.common.client.RcsbClient.RcsbMolResponse;
import lomize.computation.common.client.RcsbClient.RcsbPdbResponse;
import lomize.computation.common.client.UniprotClient;
import lomize.computation.common.client.UniprotClient.UniprotResponse;

public class PrimaryStructureSupplier implements Supplier<PrimaryStructureResult> {
	private PartialPrimaryStructure input;
	private final OpmClient opmClient;
	private final RcsbClient rcsbClient;
	private final UniprotClient uniprotClient;
	private final NewFamily newFamilies;
	private final ArrayList<String> newSpecies;

	public PrimaryStructureSupplier(final PartialPrimaryStructure input) {
		HttpClient client = HttpClientBuilder.create().build();
		this.input = input;
		this.opmClient = new OpmClient(client);
		this.rcsbClient = new RcsbClient(client);
		this.uniprotClient = new UniprotClient(client);
		this.newFamilies = new NewFamily(input.getPdbCode());
		this.newSpecies = new ArrayList<>();
	}

	private PrimaryStructure getWithRefPdb() throws IOException, HTTPException, ClientProtocolException {
		// get all info from OPM
		JSONObject json = this.opmClient.getPrimaryStructure(this.input.getRefPdb());
		PrimaryStructure primaryStructure = PrimaryStructureSerializer.deserialize(json);

		// // get uniprot codes from the PDB
		try {
			RcsbMolResponse rcsbMol = this.rcsbClient.fetchMol(this.input.getPdbCode());
			String uniprotIds[] = this.uniprotClient.getUniprotCodesFromIds(rcsbMol.getUniprotCodes());
			primaryStructure.setUniprotCodes(uniprotIds);

		} catch (Exception e) {
			primaryStructure.setUniprotCodes(new String[0]);
		}
		return primaryStructure;
	}

	private PrimaryStructure getWithoutRefPdb() {
		PrimaryStructure primaryStructure = new PrimaryStructure();

		// get uniprot codes and species from RCSB
		String[] uniprotCodes;
		String[] speciesNames;
		try {
			RcsbMolResponse rcsbMol = this.rcsbClient.fetchMol(this.input.getPdbCode());

			// record the uniprot codes
			uniprotCodes = rcsbMol.getUniprotCodes();
			speciesNames = rcsbMol.getSpeciesNames();
		} catch (Exception e) {
			e.printStackTrace();
			uniprotCodes = new String[0];
			speciesNames = new String[0];
		}

		// automatically define species name if only one is available
		// otherwise leave null so we know to define it manually
		if (speciesNames.length == 1) {
			final String shortName = String.join(" ", Util.firstWords(speciesNames[0], 2));
			try {
				JSONObject opmSpecies = this.opmClient.getSpeciesByName(shortName);
				primaryStructure.setSpeciesId(opmSpecies.getInt("id"));
			} catch (Exception e) {
				e.printStackTrace();

				// species not found in OPM, add it to new species list
				this.newSpecies.add(shortName);
			}
		} else {
			// get all species not found in OPM
			for (final String speciesName : speciesNames) {
				final String shortName = String.join(" ", Util.firstWords(speciesName, 2));
				try {
					this.opmClient.getSpeciesByName(shortName);
				} catch (Exception e) {
					this.newSpecies.add(shortName);
				}
			}
		}

		// get all uniprot ids and pfam codes from uniprot
		Set<String> uniprotIds = new HashSet<>();
		Set<String> uniprotPfams = new HashSet<>();
		for (String uniprotCode : uniprotCodes) {
			try {
				// get uniprot entry and add all pfam codes
				UniprotResponse uniprot = this.uniprotClient.getEntry(uniprotCode);
				uniprotIds.add(uniprot.getId());
				for (final String pfam : uniprot.getPfamCodes()) {
					uniprotPfams.add(pfam.toUpperCase());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// set sorted uniprot IDs
		String[] uniprotIdArray = uniprotIds.toArray(new String[uniprotIds.size()]);
		Arrays.sort(uniprotIdArray);
		primaryStructure.setUniprotCodes(uniprotIdArray);

		// get family ids from OPM for this pfam code
		Set<Integer> familyIds = new HashSet<>();
		for (final String pfamCode : uniprotPfams) {
			try {
				// get families with this pfam code from OPM API
				JSONArray opmFamilies = this.opmClient.getFamiliesByPfam(pfamCode);

				// if no families in OPM have this pfam code, record it
				if (opmFamilies.length() == 0) {
					this.newFamilies.addPfamCode(pfamCode);
				} else {
					// record all family IDs in OPM for this pfam code
					for (int i = 0; i < opmFamilies.length(); ++i) {
						familyIds.add(opmFamilies.getJSONObject(i).getInt("id"));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// auto define family ID if there is only one.
		// otherwise leave null to be manually defined.
		if (familyIds.size() == 1) {
			primaryStructure.setFamilyId(familyIds.iterator().next());
		}

		return primaryStructure;
	}

	public PrimaryStructureResult get() {
		PrimaryStructure primaryStructure;

		// get classification info from either reference PDB or from uniprot/rcsb
		if (this.input.getRefPdb() == null) {
			primaryStructure = this.getWithoutRefPdb();
		} else {
			// if an error with refpdb comes up, fetch data from uniprot/rcsb (i.e. without
			// refpdb)
			try {
				primaryStructure = this.getWithRefPdb();
			} catch (Exception e) {
				primaryStructure = this.getWithoutRefPdb();
			}
		}

		// set parameters from provided input
		primaryStructure.setPdbCode(this.input.getPdbCode());
		primaryStructure.setTopologySubunit(this.input.getSubunit());
		primaryStructure.setTopologyShowIn(this.input.getTopology() == TopologyInOut.IN);
		primaryStructure.setName(this.input.getName());
		primaryStructure.setThickness(this.input.getThickness());
		primaryStructure.setThicknessError(this.input.getThicknessError());
		primaryStructure.setTilt(this.input.getTilt());
		primaryStructure.setTiltError(this.input.getTiltError());
		primaryStructure.setGibbs(this.input.getGibbs());

		// update resolution
		try {
			RcsbPdbResponse rcsbPdb = this.rcsbClient.fetchPdb(this.input.getPdbCode());
			primaryStructure.setResolution(rcsbPdb.getResolution());
		} catch (Exception e) {
			e.printStackTrace();
			primaryStructure.setResolution("");
		}

		// get full result with new families and species lists
		PrimaryStructureResult result = new PrimaryStructureResult();
		result.setPrimaryStructure(primaryStructure);
		result.setNewFamilies(this.newFamilies);
		result.setNewSpecies(this.newSpecies);

		return result;
	}
}