package lomize.computation.services.fmap;

import com.amazonaws.regions.Regions;

public class FMAPConfig {
	public static final String RESULTS_PAGE_FILENAME = "fmap_results.html";
	public static final String TEMPLATE_FILENAME = "fmap_results.ftl";

	public static final String BUCKET_NAME = "fmap-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "FMAP Computation Results";

	public static final String EXECUTABLE_NAME = "fmap";
	public static final String PROCESS_NAME = "fmap";

	public static final String getGlmolUrl(final String pdbMultipleModelsUrl) {
		return GLMOL_BASE_URL + pdbMultipleModelsUrl;
	}

    public static final String getIcn3dUrl(final String pdbModelUrl) {
        return ICN3D_BASE_URL + pdbModelUrl;
    }

	private static final String GLMOL_BASE_URL = "https://membranome.org/protein_viewer?url=";
    private static final String ICN3D_BASE_URL = "https://www.ncbi.nlm.nih.gov/Structure/icn3d/full.html?type=pdb&url=";
}
