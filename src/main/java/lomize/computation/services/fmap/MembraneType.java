package lomize.computation.services.fmap;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MembraneType {
    @JsonProperty(Constants.DOPC)
	DOPC {
		@Override
		public String toString() {
			return Constants.DOPC;
		}
	},

    @JsonProperty(Constants.DMPC)
    DMPC {
        @Override
        public String toString() {
            return Constants.DMPC;
        }
    },

	@JsonProperty(Constants.DLPC)
    DLPC {
        @Override
        public String toString() {
            return Constants.DLPC;
        }
    },

    @JsonProperty(Constants.DEPC)
    DEPC {
        @Override
        public String toString() {
            return Constants.DEPC;
        }
    },

    @JsonProperty(Constants.memb)
    memb {
        @Override
        public String toString() {
            return Constants.memb;
        }
    },

    @JsonProperty(Constants.bact)
    bact {
        @Override
        public String toString() {
            return Constants.bact;
        }
    },

    @JsonProperty(Constants.plas)
    plas {
        @Override
        public String toString() {
            return Constants.plas;
        }
    },

    @JsonProperty(Constants.reti)
    reti {
        @Override
        public String toString() {
            return Constants.reti;
        }
    },

    @JsonProperty(Constants.mito)
    mito {
        @Override
        public String toString() {
            return Constants.mito;
        }
    },

    @JsonProperty(Constants.thyl)
    thyl {
        @Override
        public String toString() {
            return Constants.thyl;
        }
    },

    @JsonProperty(Constants.arch)
    arch {
        @Override
        public String toString() {
            return Constants.arch;
        }
    },

    @JsonProperty(Constants.gpos)
    gpos {
        @Override
        public String toString() {
            return Constants.gpos;
        }
    };

	private static class Constants {
        public static final String DOPC = "DOPC";
        public static final String DMPC = "DMPC";
        public static final String DLPC = "DLPC";
        public static final String DEPC = "DEPC";
        public static final String memb = "memb";
        public static final String bact = "bact";
        public static final String plas = "plas";
        public static final String reti = "reti";
        public static final String mito = "mito";
        public static final String thyl = "thyl";
        public static final String arch = "arch";
        public static final String gpos = "gpos";
	}
}
