package lomize.computation.services.fmap;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ModelType {
	@JsonProperty(Constants.PEPTIDE_IN_WATER)
	PEPTIDE_IN_WATER {
		@Override
		public String toString() {
			return Constants.PEPTIDE_IN_WATER;
		}
	},

	@JsonProperty(Constants.PEPTIDE_IN_MICELLES)
	PEPTIDE_IN_MICELLES {
		@Override
		public String toString() {
			return Constants.PEPTIDE_IN_MICELLES;
		}
	},

	@JsonProperty(Constants.PEPTIDE_IN_MEMBRANE)
	PEPTIDE_IN_MEMBRANES {
		@Override
		public String toString() {
			return Constants.PEPTIDE_IN_MEMBRANE;
		}
	},

	@JsonProperty(Constants.TRANSMEMBRANE_PROTEIN)
	TRANSMEMBRANE_PROTEIN {
		@Override
		public String toString() {
			return Constants.TRANSMEMBRANE_PROTEIN;
		}
	},

	@JsonProperty(Constants.WATER_SOLUBLE_PROTEIN)
	WATER_SOLUBLE_PROTEIN {
		@Override
		public String toString() {
			return Constants.WATER_SOLUBLE_PROTEIN;
		}
	};

	private static class Constants {
		public static final String PEPTIDE_IN_WATER = "Peptide in water";
		public static final String PEPTIDE_IN_MICELLES = "pep_micelles";
		public static final String PEPTIDE_IN_MEMBRANE = "pep_membrane";
		public static final String TRANSMEMBRANE_PROTEIN = "tran_protein";
		public static final String WATER_SOLUBLE_PROTEIN = "water_protein";
	}
}