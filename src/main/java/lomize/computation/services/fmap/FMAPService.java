package lomize.computation.services.fmap;

import static lomize.computation.services.fmap.FMAPConfig.RESULTS_PAGE_FILENAME;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

import lomize.computation.services.fmap.FMAPTemplateModel;
import lomize.computation.services.fmap.FMAPTemplateModel.PdbModel;

public class FMAPService extends AbstractExecutionService<FMAPParams> {

	private static final Logger logger = LoggerFactory.getLogger(FMAPService.class);

	public FMAPService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	public Void call() {
		// set input file for executable
		this.env.setExecutableInput(this.params.getModel().toString(), this.params.getGen3dModels().toString(),
				this.params.getTemperature().toString(), this.params.getPH().toString(),
				this.params.getPdbMultipleModelsFilename(), this.params.getSequence(), this.params.getMicellOrMembraneType(),
				this.params.getPredefinedSegments());

        logger.info("running executable");

		// run executable
		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing fmap: " + e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException("Error executing fmap: " + e.getMessage());
		}

        logger.info("ran executable");

		final File pdbOutFile = this.env.getFile(this.params.getPdbMultipleModelsFilename());
		final boolean pdbOutFileExists = pdbOutFile.exists();

		FMAPTemplateModel templateModel = handlePdbOutFiles();
		templateModel.setMessageList(this.env.readOutputLines());

		// render HTML results page and upload
		String resultsPageHtml = this.templateRenderer.render(templateModel);
        logger.debug("made template");
        String fname = this.env.getFile(RESULTS_PAGE_FILENAME).getAbsolutePath();
        logger.debug("results fname = " + fname);

        try (FileWriter writer = new FileWriter(fname)) {
            writer.write(resultsPageHtml);
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.debug("wrote template");

		this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

		// send email
		if (this.emailer != null) {
			logger.debug("Sending email");

			String message = "Your FMAP computation has finished.\n";
			if (this.uploader != null && pdbOutFileExists) {
                final String pdbFileUrl = this.uploader.getFileDestinationPath(this.params.getPdbMultipleModelsFilename());
				message += "The generated PDB is accessible at: " + pdbFileUrl + "\n";
			}
			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			message += "The generated statistics are accessible at: " + resultsUrl + "\n";

			this.emailer.send(message);

			logger.debug("Email sent.");
		}

        return null;
	}

    FMAPTemplateModel handlePdbOutFiles() {
		FMAPTemplateModel templateModel = new FMAPTemplateModel();

        // first, handle multiple models (main) pdb out file
        final String pdbOutFname = this.params.getPdbMultipleModelsFilename();
		final File pdbOutFile = this.env.getFile(pdbOutFname);
        if (!pdbOutFile.exists())
            return templateModel;

		// upload pdb out file
		final String pdbFileUrl = this.uploader.getFileDestinationPath(this.params.getPdbMultipleModelsFilename());
        uploader.uploadFile(pdbOutFile, pdbOutFname, null);
        templateModel.setPdbMultipleModelsUrl(pdbFileUrl);

        // then, split each model into its own file
        logger.debug("making model files");
        // MODEL followed by some whitespace followed by a model number
        final Pattern modelPattern = Pattern.compile("^MODEL\\s+(\\d+)$");

        try (BufferedReader file = new BufferedReader(new FileReader(pdbOutFile))) {
            String line;
            String modelFname = null;
            File modelFile = null;
            BufferedWriter modelWriter = null;
            PdbModel model = null;

            while ((line = file.readLine()) != null) {
                Matcher modelMatch = modelPattern.matcher(line);

                if (modelMatch.matches()) {
                    if (modelFname != null || modelFile != null || modelWriter != null || model != null) {
                        logger.error("model began before other model ended");
                        return templateModel;
                    }

                    Integer modelNum = Integer.parseInt(modelMatch.group(1));
                    modelFname = this.params.getPdbModelFilename(modelNum);
                    modelFile = this.env.getFile(modelFname);
                    modelWriter = new BufferedWriter(new FileWriter(this.env.getFile(modelFname)));
                    model = new FMAPTemplateModel.PdbModel();
                    model.setNum(modelNum);
                    String modelUrl = this.uploader.getFileDestinationPath(modelFname);
                    model.setUrl(modelUrl);
                    model.setIcn3dUrl(FMAPConfig.getIcn3dUrl(modelUrl));
                    continue;
                }

                if (line.matches("^ENDMDL$")) {
                    if (modelFname == null || modelFile == null || modelWriter == null || model == null) {
                        logger.error("model ended without model beginning");
                        return templateModel;
                    }

                    modelWriter.close();
                    modelWriter = null;
                    uploader.uploadFile(modelFile, modelFname, null);
                    modelFile = null;
                    modelFname = null;
                    templateModel.addPdbModel(model);
                    model = null;
                    continue;
                }

                if (modelFname == null || modelFile == null || modelWriter == null || model == null) {
                    logger.warn("data line without model");
                    continue;
                }

                modelWriter.write(line);
                modelWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.debug("made model files");
        return templateModel;
    }
}
