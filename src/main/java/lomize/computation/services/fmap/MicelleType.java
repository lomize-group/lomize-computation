package lomize.computation.services.fmap;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MicelleType {
	@JsonProperty(Constants.SDS)
	SDS {
		@Override
		public String toString() {
			return Constants.SDS_VAL;
		}
	},

	@JsonProperty(Constants.DPC)
	DPC {
		@Override
		public String toString() {
			return Constants.DPC_VAL;
		}
	},

	@JsonProperty(Constants.LPS)
	LPS {
		@Override
		public String toString() {
			return Constants.LPS_VAL;
		}
    },
    
    @JsonProperty(Constants.DHPC)
	DHPC {
		@Override
		public String toString() {
			return Constants.DHPC_VAL;
		}
	},

    @JsonProperty(Constants.DIAMETER)
	DIAMETER {
		@Override
		public String toString() {
			return Constants.DIAMETER_VAL;
		}
	};

	public static class Constants {
		public static final String SDS = "sds";
		public static final String DPC = "dpc";
		public static final String LPS = "lps";
		public static final String DHPC = "dhpc";
        public static final String DIAMETER = "diameter";
		public static final String SDS_VAL = "1";
		public static final String DPC_VAL = "2";
		public static final String LPS_VAL = "3";
		public static final String DHPC_VAL = "4";
        public static final String DIAMETER_VAL = "5";
	};
}
