package lomize.computation.services.fmap;

import java.util.ArrayList;
import java.util.StringJoiner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class FMAPTemplateModel {
	private static final Logger logger = LoggerFactory.getLogger(FMAPTemplateModel.class);

    @Data
    public static class Environment {
        private String model;
        private String temperature;
        private String ph;
        private String environment;
    };

    private Environment environment = new Environment();

    @Data
    public static class Peptide {
        private String energy;
        private String aminoAcidSequence;
    };

    private Peptide peptide = new Peptide();

    @Data
    public static class AlphaHelix {
        private String segment;
        private String stability1, stability2;
        private String gibbs;
        private String tiltAngle;
        private String depthThickness;
        private String sequence;
    };

    private ArrayList<AlphaHelix> alphaHelices = new ArrayList<>();

    @Setter(AccessLevel.NONE)
	private ArrayList<String> messageList = new ArrayList<>();

    public void setMessageList(final ArrayList<String> messageList) {
        this.messageList = messageList;

        final Pattern modelPattern = Pattern.compile("^Model: (.*)$"),
            tempPattern = Pattern.compile("^Temperature: (\\d+)\\. K$"),
            phPattern = Pattern.compile("^pH: (\\d+\\.\\d+)$"),
            envPattern = Pattern.compile("^Environment: (.*)$"),
            energyPattern = Pattern.compile("^Membrane or micelle binding energy of peptide: *(-?\\d+\\.\\d+) kcal/mol$"),
            alphaHelixPattern1 = Pattern.compile("^Helix (\\d+): segment +(.+), Stability relative to coil in water +(-?\\d+\\.\\d+) kcal/mol$"),
            alphaHelixPattern2 = Pattern.compile("^Helix +(\\d+): segment +(.+), Stability relative to coil in water and bound coil: *(-?\\d+\\.\\d+) and +(-?\\d+\\.\\d+) kcal/mol$"),
            transferEnergyPattern = Pattern.compile("^Transfer energy of alpha-helix +(\\d+) to membrane: +(-?\\d+\\.\\d+) kcal/mol, tilt angle: +(\\d+)\\., depth/thickness: +(\\d+\\.\\d+) A$"),
            sequencePattern = Pattern.compile("^Sequence of helix +(\\d+): +(.+)$");

        for (int i = 0; i < messageList.size(); ++i) {
            String line = messageList.get(i);
            logger.debug(line);
            final Matcher modelMatch = modelPattern.matcher(line),
                tempMatch = tempPattern.matcher(line),
                phMatch = phPattern.matcher(line),
                envMatch = envPattern.matcher(line),
                energyMatch = energyPattern.matcher(line),
                alphaHelixMatch1 = alphaHelixPattern1.matcher(line),
                alphaHelixMatch2 = alphaHelixPattern2.matcher(line),
                transferEnergyMatch = transferEnergyPattern.matcher(line),
                sequenceMatch = sequencePattern.matcher(line);

            if (modelMatch.matches()) {
                this.environment.model = modelMatch.group(1);
            } else if (tempMatch.matches()) {
                this.environment.temperature = tempMatch.group(1);
            } else if (phMatch.matches()) {
                this.environment.ph = phMatch.group(1);
            } else if (envMatch.matches()) {
                this.environment.environment = envMatch.group(1);
            } else if (energyMatch.matches()) {
                this.peptide.energy = energyMatch.group(1);
            } else if (line.equals("Amino acid sequence:")) {
                StringJoiner joiner = new StringJoiner("\n", "", "");

                while (++i < messageList.size()) {
                    line = messageList.get(i);
                    logger.debug(line);

                    // is blank line? (contains only whitespace)
                    if (line.matches("^\\s*$"))
                        break;

                    joiner.add(line);
                }

                this.peptide.aminoAcidSequence = joiner.toString();
            } else if (alphaHelixMatch1.matches()) {
                Integer num = Integer.parseInt(alphaHelixMatch1.group(1));

                if (num - 1 != this.alphaHelices.size()) {
                    logger.warn("received alpha helix data out of order: " + line);
                    continue;
                }

                AlphaHelix alphaHelix = new AlphaHelix();
                alphaHelix.segment = alphaHelixMatch1.group(2);
                alphaHelix.stability1 = alphaHelixMatch1.group(3);
                this.alphaHelices.add(alphaHelix);
            } else if (alphaHelixMatch2.matches()) {
                Integer num = Integer.parseInt(alphaHelixMatch2.group(1));

                if (num - 1 != this.alphaHelices.size()) {
                    logger.warn("received alpha helix data out of order: " + line);
                    continue;
                }

                AlphaHelix alphaHelix = new AlphaHelix();
                alphaHelix.segment = alphaHelixMatch2.group(2);
                alphaHelix.stability1 = alphaHelixMatch2.group(3);
                alphaHelix.stability2 = alphaHelixMatch2.group(4);
                this.alphaHelices.add(alphaHelix);
            } else if (transferEnergyMatch.matches()) {
                Integer num = Integer.parseInt(transferEnergyMatch.group(1));

                if (num - 1 > this.alphaHelices.size() - 1) {
                    logger.warn("recevied alpha hexlix sequence for unknown alpha helix: " + line);
                    logger.warn((num - 1) + " " + this.alphaHelices.size());
                    continue;
                }

                AlphaHelix alphaHelix = this.alphaHelices.get(num - 1);
                alphaHelix.gibbs = transferEnergyMatch.group(2);
                alphaHelix.tiltAngle = transferEnergyMatch.group(3);
                alphaHelix.depthThickness = transferEnergyMatch.group(4);
            } else if (sequenceMatch.matches()) {
                Integer num = Integer.parseInt(sequenceMatch.group(1));

                if (num - 1 > this.alphaHelices.size() - 1) {
                    logger.warn("recevied alpha hexlix sequence for unknown alpha helix: " + line);
                    logger.warn((num - 1) + " " + this.alphaHelices.size());
                    continue;
                }

                AlphaHelix alphaHelix = this.alphaHelices.get(num - 1);
                alphaHelix.sequence = sequenceMatch.group(2);
            }
        }

        logger.debug("model = " + this.environment.model);
        logger.debug("temperature = " + this.environment.temperature);
        logger.debug("ph = " + this.environment.ph);
        logger.debug("environment = " + this.environment.environment);
        logger.debug("energy = " + this.peptide.energy);
        logger.debug("aminoAcidSequence = " + this.peptide.aminoAcidSequence);
        logger.debug(this.alphaHelices.size() + " alpha helices found");

        for (int i = 0; i < this.alphaHelices.size(); ++i) {
            AlphaHelix alphaHelix = this.alphaHelices.get(i);
            logger.debug("alpha helix " + (i + 1) + " = ");
            logger.debug("  segment = " + alphaHelix.segment);
            logger.debug("  staility1 = " + alphaHelix.stability1);
            logger.debug("  staility2 = " + alphaHelix.stability2);
            logger.debug("  gibbs = " + alphaHelix.gibbs);
            logger.debug("  tiltAngle = " + alphaHelix.tiltAngle);
            logger.debug("  depthThickness = " + alphaHelix.depthThickness);
            logger.debug("  sequence = " + alphaHelix.sequence);
        }
    }

	private String pdbMultipleModelsUrl = null;
	@Setter(AccessLevel.NONE)
	private String glmolUrl = null;

    @Data
    public static class PdbModel {
        private Integer num;
        private String url;
        private String icn3dUrl;
    }

    @Setter(AccessLevel.NONE)
    private ArrayList<PdbModel> pdbModels = new ArrayList<>();

	public void setPdbMultipleModelsUrl(final String pdbMultipleModelsUrl) {
		this.pdbMultipleModelsUrl = pdbMultipleModelsUrl;
		this.glmolUrl = FMAPConfig.getGlmolUrl(pdbMultipleModelsUrl);
	}

    public void addPdbModel(final PdbModel pdbModel) {
        this.pdbModels.add(pdbModel);
    }

	private String submitAgainUrl = "https://membranome.org/fmap";
}
