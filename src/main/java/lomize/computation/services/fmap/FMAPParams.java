package lomize.computation.services.fmap;

import lombok.Data;
import lomize.computation.common.YesNo;
import lomize.computation.controllers.request.FMAPRequest;
import lomize.computation.services.fmap.ModelType;

@Data
public class FMAPParams {
	private ModelType model;
	private String sequence;
	private YesNo gen3dModels;
	private Double temperature;
	private Double pH;
    private String pdbMultipleModelsFilename;
    private String pdbModelFilenameBase;
	private String micellOrMembraneType;
	private String predefinedSegments;

	public FMAPParams(FMAPRequest req) {
		this.model = req.getModel();
		this.sequence = req.getSequence();
		this.gen3dModels = req.getGen3dModels() ? YesNo.YES : YesNo.NO;
		this.temperature = req.getTemperature();
		this.pH = req.getPh();
        this.pdbMultipleModelsFilename = req.getOutputFilename() + ".out.pdb";
        this.pdbModelFilenameBase = req.getOutputFilename();

		if (this.model == ModelType.PEPTIDE_IN_MICELLES) {
            MicelleType micelleType = req.getMicelleType();

            if (micelleType.equals(MicelleType.DIAMETER)) {
                Integer diameter = req.getMicelleDiameter();
                if (diameter == null)
                    diameter = 1;
                this.micellOrMembraneType = String.format("%s%10d", micelleType, diameter);
            } else {
                this.micellOrMembraneType = micelleType.toString();
            }
		} else if (this.model == ModelType.PEPTIDE_IN_MEMBRANES) {
			this.micellOrMembraneType = req.getMembraneType().toString();
		} else {
			this.micellOrMembraneType = "";
		}

		this.predefinedSegments = req.getPredefinedSegments();
		if (this.predefinedSegments == null) {
			this.predefinedSegments = "";
		}
	}

    // e.g. output pdb file = result && modelNum == 2 -> result.2.out.pdb
    public String getPdbModelFilename(final Integer modelNum) {
        return this.pdbModelFilenameBase + "." + modelNum + ".out.pdb";
    }
}
