package lomize.computation.services.tmdock;

import static lomize.computation.services.tmdock.TMDOCKConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.tmdock.TMDOCKConfig.SUBDIRECTORY_NAME;

import java.io.File;
import java.io.IOException;

import org.apache.http.entity.ContentType;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

public class TMDOCKService extends AbstractExecutionService<TMDOCKParams> {

	public TMDOCKService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	public Void call() {
		// write input file
		this.env.setExecutableInput(this.params.getPdbOutFilename(), this.params.getSequence());
		this.env.createDirectory(SUBDIRECTORY_NAME);

		// run executable
		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing tmdock: " + e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException("Error executing tmdock: " + e.getMessage());
		}

		final File result = this.env.getFile(this.params.getPdbOutFilename());
		final boolean pdbOutFileExists = result.exists();
		final String pdbFileUrl = this.uploader.getFileDestinationPath(this.params.getPdbOutFilename());

		// upload files
		TMDOCKTemplateModel templateModel = new TMDOCKTemplateModel();

		// upload PDB file
		if (pdbOutFileExists) {
			templateModel.setPdbFileUrl(pdbFileUrl);
			uploader.uploadFile(result, this.params.getPdbOutFilename(), null);
		}

		// read output
		for (String line : env.readOutputLines()) {
			templateModel.addMessage(line);
		}

		// render and upload HTML results page
		String resultsPageHtml = this.templateRenderer.render(templateModel);
		uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

		// send email
		if (this.emailer != null) {
			String message = "Your TMDOCK computation has finished.\n";
			if (pdbOutFileExists) {
				message += "The generated PDB is accessible at: " + pdbFileUrl + "\n";
			}
			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			message += "The generated statistics are accessible at: " + resultsUrl + "\n";

			this.emailer.send(message);
		}

		return null;
	}
}
