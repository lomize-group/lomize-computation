package lomize.computation.services.tmdock;

import com.amazonaws.regions.Regions;

public class TMDOCKConfig {
	public static final String RESULTS_PAGE_FILENAME = "tmdock_results.html";
	public static final String TEMPLATE_FILENAME = "tmdock_results.ftl";

	public static final String BUCKET_NAME = "tmdock-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "TMDOCK Computation Results";

	public static final String EXECUTABLE_NAME = "tmdock";
	public static final String PROCESS_NAME = "tmdock";
	public static final String SUBDIRECTORY_NAME = "tmp";

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";
}