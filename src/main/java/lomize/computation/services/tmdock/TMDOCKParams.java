package lomize.computation.services.tmdock;

import lombok.Data;
import lomize.computation.controllers.request.TMDOCKRequest;

@Data
public class TMDOCKParams {
	private String sequence;
	private String pdbOutFilename;

	public TMDOCKParams(TMDOCKRequest req) {
		this.sequence = req.getSequence();
		this.pdbOutFilename = req.getFilename() + ".out.pdb";
	}
}