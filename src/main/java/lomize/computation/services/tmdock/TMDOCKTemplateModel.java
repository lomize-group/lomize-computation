package lomize.computation.services.tmdock;

import java.util.ArrayList;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TMDOCKTemplateModel {
	private final ArrayList<String> messageList = new ArrayList<String>();
	private String pdbFileUrl = null;

	public void addMessage(final String message) {
		this.messageList.add(message);
	}
}