package lomize.computation.services.cellpm;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MembraneType {
	@JsonProperty(Constants.DOPC)
	DOPC {
		@Override
		public String toString() {
			return Constants.DOPC;
		}
	};

	private static class Constants {
		public static final String DOPC = "DOPC";
	}
}