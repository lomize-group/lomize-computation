package lomize.computation.services.cellpm;

import static lomize.computation.services.cellpm.CellPMConfig.SUBDIRECTORY_NAME;
import static lomize.computation.services.cellpm.CellPMConfig.RESULTS_PAGE_FILENAME;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.http.entity.ContentType;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

public class CellPMService extends AbstractExecutionService<CellPMParams> {

	public CellPMService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	private static String getOutputFilename(final String inputFilename) {
		String[] splitFilename = inputFilename.split("\\.");
		splitFilename[0] += "out";
		return String.join(".", splitFilename);
	}

	public Void call() {
		// set executable input
		// sequence will be empty string so it's ok to write it if using pdb file
		this.env.setExecutableInput(this.params.getMembraneType().toString(), this.params.getTemperature().toString(),
				this.params.getPh().toString(), this.params.getPdbFilename().toString(),
				this.params.getSequence().toString());

		// run executable
		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (IOException e) {
			throw new RuntimeException("Error executing cellpm: " + e.getMessage());
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing cellpm: " + e.getMessage());
		}

		// determine names of files output by cellpm executable
		final String originalPathwayFilename = Paths.get(SUBDIRECTORY_NAME, getOutputFilename(this.params.getPdbFilename())).toString();
		final String originalBoundFilename = Paths.get(SUBDIRECTORY_NAME, this.params.getPdbFilename()).toString();

		// collect template parameters
		CellPMTemplateModel templateModel = CellPMTemplateModel.fromOutput(this.env.readOutputLines());

		// rename output files and set file url params if the files exist
		File pathwayFile = this.env.getFile(originalPathwayFilename);
		if (pathwayFile.exists()) {
			// rename pathway file
			final String newPathwayFilename = "pathway_" + this.params.getPdbFilename();
			try {
				pathwayFile = Util.moveFile(pathwayFile, this.env.getFile(newPathwayFilename),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				throw new RuntimeException("Error: could not rename cellpm output file from " + originalPathwayFilename
						+ " to " + newPathwayFilename + "; " + e.getMessage());
			}
			// upload pathway file
			this.uploader.uploadFile(pathwayFile, pathwayFile.getName(), null);

			// set pathway file param in template model
			templateModel.setPathwayPdbFileUrl(this.uploader.getFileDestinationPath(pathwayFile.getName()));
		}
		File boundFile = this.env.getFile(originalBoundFilename);
		if (boundFile.exists()) {
			// rename bound file
			final String newBoundFilename = "bound_state_" + this.params.getPdbFilename();
			try {
				boundFile = Util.moveFile(boundFile, this.env.getFile(newBoundFilename),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				throw new RuntimeException("Error: could not rename cellpm output file from " + originalBoundFilename
						+ " to " + newBoundFilename + "; " + e.getMessage());
			}
			// upload bound file
			this.uploader.uploadFile(boundFile, boundFile.getName(), null);

			// set bound file param in template model
			templateModel.setBoundPdbFileUrl(this.uploader.getFileDestinationPath(boundFile.getName()));
		}

		// upload results page
		final String resultsPageHtml = this.templateRenderer.render(templateModel);
		this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

		// send email
		if (this.emailer != null) {
			String message = "Your CellPM computation has finished.\n";

			if (pathwayFile.exists()) {
				message += "The generated Pathway PDB is accessible at: "
						+ this.uploader.getFileDestinationPath(pathwayFile.getName()) + "\n";
			}
			if (boundFile.exists()) {
				message += "The generated Bound State PDB is accessible at: "
						+ this.uploader.getFileDestinationPath(boundFile.getName()) + "\n";
			}

			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			message += "The generated statistics are accessible at: " + resultsUrl + "\n";

			this.emailer.send(message);
		}

		return null;
	}
}
