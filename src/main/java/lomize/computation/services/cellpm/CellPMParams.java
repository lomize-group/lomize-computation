package lomize.computation.services.cellpm;

import lombok.Data;
import lombok.NoArgsConstructor;
import lomize.computation.common.Util;
import lomize.computation.controllers.request.CellPMRequest;

@Data
@NoArgsConstructor
public class CellPMParams {
	private Double temperature;
	private Double ph;
	private MembraneType membraneType;

	private String pdbFilename;
	private String sequence;

	private static String PDB_EXT = ".pdb";

	public void setPdbFilename(final String pdbFilename) {
		if (pdbFilename.endsWith(PDB_EXT)) {
			this.pdbFilename = pdbFilename;
		} else {
			this.pdbFilename = pdbFilename + PDB_EXT;
		}
	}

	public CellPMParams(CellPMRequest req) {
		this.temperature = req.getTemperature();
		this.ph = req.getPh();
		this.membraneType = req.getMembraneType();

		// use sequence and provided filename if no pdb file
		if (req.getPdbFile() == null) {
			this.sequence = req.getSequence();
			this.setPdbFilename(req.getOutputPdbFilename());
		} else {
			this.sequence = "";
			this.setPdbFilename(Util.sanitizeFilename(req.getPdbFile().getOriginalFilename()));
		}
	}
}