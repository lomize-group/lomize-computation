package lomize.computation.services.cellpm;

import com.amazonaws.regions.Regions;

public class CellPMConfig {
	public static final String RESULTS_PAGE_FILENAME = "cellpm_results.html";
	public static final String TEMPLATE_FILENAME = "cellpm_results.ftl";

	public static final String BUCKET_NAME = "cellpm-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "CellPM Computation Results";

	public static final String EXECUTABLE_NAME = "cellpm";
	public static final String PROCESS_NAME = "cellpm";

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";

	public static final String SUBDIRECTORY_NAME = "pdb_output";

	public static final String getGlmolUrl(final String pdbFileUrl) {
		return GLMOL_BASE_URL + pdbFileUrl;
	}

	public static final String getJmolUrl(final String pdbFileUrl) {
		return JMOL_BASE_URL + pdbFileUrl;
	}

	private static final String GLMOL_BASE_URL = "https://cellpm.org/molecule_viewer?url=";
	private static final String JMOL_BASE_URL = "https://bioinformatics.org/firstglance/fgij/fg.htm?mol=";
}
