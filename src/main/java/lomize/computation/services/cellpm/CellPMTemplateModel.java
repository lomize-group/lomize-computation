package lomize.computation.services.cellpm;

import java.util.ArrayList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lomize.computation.common.Measurement;

@Data
public class CellPMTemplateModel {

	public static class OutputParamNames {
		public static final String LOG_PERM_COEFF = "Log of permeability coefficient";
	}

	private String boundPdbFileUrl;
	private String pathwayPdbFileUrl;

	@Setter(AccessLevel.NONE)
	private String boundGlmolUrl;

	@Setter(AccessLevel.NONE)
	private String boundJmolUrl;

	@Setter(AccessLevel.NONE)
	private String pathwayGlmolUrl;

	@Setter(AccessLevel.NONE)
	private String pathwayJmolUrl;

	@Setter(AccessLevel.NONE)
	private ArrayList<String> outputLines = new ArrayList<>();

	@Setter(AccessLevel.NONE)
	private ArrayList<Measurement> outputDataList = new ArrayList<>();

	public static CellPMTemplateModel fromOutput(final ArrayList<String> cellpmOutputLines) {
		CellPMTemplateModel templateModel = new CellPMTemplateModel();
		for (final String line : cellpmOutputLines) {
			templateModel.addOutputLine(line);
			if (line.startsWith(OutputParamNames.LOG_PERM_COEFF)) {
				Measurement data = new Measurement();
				final String[] splitLine = line.split("=");
				data.setName(OutputParamNames.LOG_PERM_COEFF);
				data.setValue(splitLine[splitLine.length - 1].trim());
				templateModel.addData(data);
			}
		}
		return templateModel;
	}

	public void addOutputLine(String outputLine) {
		this.outputLines.add(outputLine);
	}

	public void addData(Measurement outputData) {
		this.outputDataList.add(outputData);
	}

	public void setPathwayPdbFileUrl(String pathwayPdbFileUrl) {
		this.pathwayPdbFileUrl = pathwayPdbFileUrl;
		this.pathwayGlmolUrl = CellPMConfig.getGlmolUrl(pathwayPdbFileUrl);
		this.pathwayJmolUrl = CellPMConfig.getJmolUrl(pathwayPdbFileUrl);
	}

	public void setBoundPdbFileUrl(String boundPdbFileUrl) {
		this.boundPdbFileUrl = boundPdbFileUrl;
		this.boundGlmolUrl = CellPMConfig.getGlmolUrl(boundPdbFileUrl);
		this.boundJmolUrl = CellPMConfig.getJmolUrl(boundPdbFileUrl);
	}

	private String submitAgainUrl = "https://cellpm.org/cellpm_server";
}