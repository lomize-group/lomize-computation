package lomize.computation.services.ppm3;

import static lomize.computation.services.ppm3.PPM3Config.RESULTS_PAGE_FILENAME;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

public class PPM3Service extends AbstractExecutionService<PPM3Params> {
    private static final Logger logger = LoggerFactory.getLogger(PPM3Service.class);

    public PPM3Service(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
            final TemplateRenderer templateRenderer) {
        super(env, uploader, emailer, templateRenderer);
    }

    private static String getOutputFilename(final String inputFilename) {
        String[] splitFilename = inputFilename.split("\\.");
        splitFilename[0] += "out";
        return String.join(".", splitFilename);
    }

    public Void call() {
        // set input for executable
        ArrayList<String> lines = new ArrayList<String>();
        lines.add(this.params.getHeteroatoms().toString());
        lines.add(this.params.getInputPdbFilename());
        lines.add(String.valueOf(this.params.getMembranes().size()));

        for (Membrane membrane : this.params.getMembranes()) {
            MembraneType membraneType = membrane.getMembraneType();
            lines.add(membraneType != null ? membraneType.toString() : "");
            lines.add(membrane.getAllowCurvature() ? "curved" : "planar");
            lines.add(membrane.getTopologyIn() ? "in" : "out");
            lines.add(membrane.getSubunits());
        }

        this.env.setExecutableInputLines(lines.toArray(new String[lines.size()]));

        // run executable
        try {
            this.process = this.env.runExecutable();
            this.process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Error executing ppm3: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error executing ppm3: " + e.getMessage());
        }

        final String outputPdbFilename = getOutputFilename(this.params.getInputPdbFilename());
        final File outputPdbFile = this.env.getFile(outputPdbFilename);
        final boolean outputPdbFileExists = outputPdbFile.exists();

        // upload output pdb file
        if (outputPdbFileExists) {
            this.uploader.uploadFile(outputPdbFile, outputPdbFilename, null);
        }

        final File outputTables = this.env.getFile("output_tables.txt");
        lines = new ArrayList<>(); // sus: re-using this variable

        try (BufferedReader file = new BufferedReader(new FileReader(outputTables))) {
            String line;
            while ((line = file.readLine()) != null)
                lines.add(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

        PPM3TemplateModel templateModel = PPM3TemplateModel.fromOutput(lines);
        templateModel.setMessageList(this.env.readOutputLines());

        final String pdbFileUrl = this.uploader.getFileDestinationPath(outputPdbFilename);
        if (outputPdbFileExists)
            templateModel.setPdbFileUrl(pdbFileUrl);

        // render and upload HTML template
        final String resultsPageHtml = this.templateRenderer.render(templateModel);
        this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);
        final String resultsFname = this.env.getFile(RESULTS_PAGE_FILENAME).getAbsolutePath();
        logger.debug("resultsFname = " + resultsFname);

        try (FileWriter writer = new FileWriter(resultsFname)) {
            writer.write(resultsPageHtml);
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.debug("wrote template");

        // send email
        if (this.emailer != null) {
            String message = "Your PPM3 computation has finished.\n";
            if (outputPdbFileExists)
                message += "The generated PDB is accessible at: " + pdbFileUrl + "\n";
            final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
            message += "The generated statistics are accessible at: " + resultsUrl + "\n";
            this.emailer.send(message);
        }

        return null;
    }
}
