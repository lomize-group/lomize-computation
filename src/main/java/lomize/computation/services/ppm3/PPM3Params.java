package lomize.computation.services.ppm3;

import java.util.ArrayList;
import lombok.Data;
import lomize.computation.common.TopologyInOut;
import lomize.computation.common.Util;
import lomize.computation.common.YesNo;
import lomize.computation.controllers.request.PPM3Request;
import lomize.computation.services.ppm3.Membrane;

@Data
public class PPM3Params {
    private ArrayList<Membrane> membranes;
    private YesNo heteroatoms;
    private String inputPdbFilename;

    public PPM3Params(final PPM3Request req) {
        this.membranes = req.getMembranes();
        this.heteroatoms = YesNo.fromBoolean(req.getHeteroatoms());
        this.inputPdbFilename = Util.sanitizeFilename(req.getPdbFilename());
    }
}
