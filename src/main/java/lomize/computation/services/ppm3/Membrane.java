package lomize.computation.services.ppm3;

import lombok.Data;

@Data
public class Membrane {
    private MembraneType membraneType;
    private Boolean allowCurvature;
    private Boolean topologyIn;
    private String subunits;
}
