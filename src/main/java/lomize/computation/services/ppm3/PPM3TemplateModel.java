package lomize.computation.services.ppm3;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Data;
import lombok.AccessLevel;
import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lomize.computation.common.Measurement;

@Data
public class PPM3TemplateModel {
    private static final Logger logger = LoggerFactory.getLogger(PPM3TemplateModel.class);

    @Data
    public static class Table2Row {
        private String subunits;
        private String tilt;
        private String segments;
    }

    @Data
    public static class Membrane {
        private String name = null;
        private String message = null;
        private String type = null;

        // Table 1 data
        private Measurement thickness;
        private Measurement tilt;
        private String radius = null; // curved only
        private String EPlanar;  // both planar and curved
        private String ECurved;  // curved only

        // Table 2 data
        private ArrayList<Table2Row> embeddedResidues = new ArrayList<>();
        // full name: transmembraneSecondaryStructureSegments
        private ArrayList<Table2Row> transmembrane = new ArrayList<>();
    }

    @Setter(AccessLevel.NONE)
    private ArrayList<Membrane> membranes = new ArrayList<>();

    private ArrayList<String> messageList = null;

    @Setter(AccessLevel.NONE)
    private String pdbFileUrl = null;
    @Setter(AccessLevel.NONE)
    private String icn3dUrl = null;
    @Setter(AccessLevel.NONE)
    private String jmolUrl = null;

    public void setPdbFileUrl(final String pdbFileUrl) {
        this.pdbFileUrl = pdbFileUrl;
        this.icn3dUrl = PPM3Config.getIcn3dUrl(pdbFileUrl);
        this.jmolUrl = PPM3Config.getJmolUrl(pdbFileUrl);
    }

    public static PPM3TemplateModel fromOutput(final ArrayList<String> ppm3Output) {
        PPM3TemplateModel templateModel = new PPM3TemplateModel();
        templateModel.membranes = new ArrayList<>();
        Membrane membrane = null;

        final Pattern membranePattern = Pattern.compile("^Membrane (\\d+)$"),
            transmembranePattern = Pattern.compile("^[a-zA-Z0-9];\\s*\\d+;.*$");

        for (int i = 0; i < ppm3Output.size(); ++i) {
            String line = ppm3Output.get(i).trim();
            logger.debug(line);

            final Matcher membraneMatch = membranePattern.matcher(line),
                transmembraneMatch = transmembranePattern.matcher(line);

            // new membrane
            if (membraneMatch.matches()) {
                if (membrane != null)
                    templateModel.membranes.add(membrane);
                membrane = new Membrane();
                membrane.setName(line);

                line = ppm3Output.get(++i).trim();
                logger.debug(line);

                if (line.equals("Protein in planar membrane")) {
                    membrane.setType("planar");
                } else if (line.equals("Protein in a spherically deformed membrane")) {
                    membrane.setType("curved");
                } else {
                    logger.error("unknown membrane type");
                    break;
                }
            // transmembrane residues table 2
            } else if (transmembraneMatch.matches()) {
                String[] values = line.split(";");
                Table2Row row = new Table2Row();
                row.setSubunits(values[0].trim());
                row.setTilt(values[1].trim());
                row.setSegments(values[2].trim());
                membrane.getTransmembrane().add(row);
            // embedded residues table 2
            } else if (line.startsWith("#")) {
                String[] values = line.substring(1).split(";");
                Table2Row row = new Table2Row();
                row.setSubunits(values[0].trim());
                row.setTilt(values[1].trim());
                row.setSegments(values[2].trim());
                membrane.getEmbeddedResidues().add(row);
            // planar membrane table 1
            } else if (membrane != null && membrane.type.equals("planar") &&
                    line.equals("depth;d_err;tilt;t_err;Eplanar")) {
                line = ppm3Output.get(++i).trim();
                logger.debug(line);
                String[] values = line.split(";");

                Measurement thickness = new Measurement();
                thickness.setValue(values[0].trim());
                thickness.setError(values[1].trim());
                membrane.setThickness(thickness);

                Measurement tilt = new Measurement();
                tilt.setValue(values[2].trim());
                tilt.setError(values[3].trim());
                membrane.setTilt(tilt);

                membrane.setEPlanar(values[4].trim());
            // curved membrane table 1
            } else if (membrane != null && membrane.type.equals("curved") &&
                    line.equals("depth;radius;tilt;Ecurved;Eplanar")) {
                line = ppm3Output.get(++i).trim();
                logger.debug(line);
                String[] values = line.split(";");

                Measurement thickness = new Measurement();
                thickness.setValue(values[0].trim());
                membrane.setThickness(thickness);

                membrane.setRadius(values[1].trim());

                Measurement tilt = new Measurement();
                tilt.setValue(values[2].trim());
                membrane.setTilt(tilt);

                membrane.setECurved(values[3].trim());
                membrane.setEPlanar(values[4].trim());
            } else if (line.equals("Peptide in micelle")) {
                line = ppm3Output.get(++i).trim();
                logger.debug(line);
                String[] values = line.split("=");

                if (membrane != null) {
                    logger.error("unexpected micelle");
                    break;
                }

                membrane = new Membrane();
                membrane.setType("micelle");
                membrane.setECurved(values[1].trim());
            }
        }

        if (membrane != null)
            templateModel.membranes.add(membrane);
        return templateModel;
    }
}
