package lomize.computation.services.ppm3;
import com.fasterxml.jackson.annotation.JsonProperty;

public enum MembraneType {
    @JsonProperty(Constants.PMm)
    PMm {
        @Override
        public String toString() {
            return Constants.PMm;
        }
    },

    @JsonProperty(Constants.PMp)
    PMp {
        @Override
        public String toString() {
            return Constants.PMp;
        }
    },

    @JsonProperty(Constants.PMf)
    PMf {
        @Override
        public String toString() {
            return Constants.PMf;
        }
    },

    @JsonProperty(Constants.ERf)
    ERf {
        @Override
        public String toString() {
            return Constants.ERf;
        }
    },

    @JsonProperty(Constants.ERm)
    ERm {
        @Override
        public String toString() {
            return Constants.ERm;
        }
    },

    @JsonProperty(Constants.GOL)
    GOL {
        @Override
        public String toString() {
            return Constants.GOL;
        }
    },

    @JsonProperty(Constants.LYS)
    LYS {
        @Override
        public String toString() {
            return Constants.LYS;
        }
    },

    @JsonProperty(Constants.END)
    END {
        @Override
        public String toString() {
            return Constants.END;
        }
    },

    @JsonProperty(Constants.VAC)
    VAC {
        @Override
        public String toString() {
            return Constants.VAC;
        }
    },

    @JsonProperty(Constants.MOM)
    MOM {
        @Override
        public String toString() {
            return Constants.MOM;
        }
    },

    @JsonProperty(Constants.MIM)
    MIM {
        @Override
        public String toString() {
            return Constants.MIM;
        }
    },

    @JsonProperty(Constants.THp)
    THp {
        @Override
        public String toString() {
            return Constants.THp;
        }
    },

    @JsonProperty(Constants.THb)
    THb {
        @Override
        public String toString() {
            return Constants.THb;
        }
    },

    @JsonProperty(Constants.GnO)
    GnO {
        @Override
        public String toString() {
            return Constants.GnO;
        }
    },

    @JsonProperty(Constants.GnI)
    GnI {
        @Override
        public String toString() {
            return Constants.GnI;
        }
    },

    @JsonProperty(Constants.GpI)
    GpI {
        @Override
        public String toString() {
            return Constants.GpI;
        }
    },

    @JsonProperty(Constants.ARC)
    ARC {
        @Override
        public String toString() {
            return Constants.ARC;
        }
    },

    @JsonProperty(Constants.LPC)
    LPC {
        @Override
        public String toString() {
            return Constants.LPC;
        }
    },

    @JsonProperty(Constants.MPC)
    MPC {
        @Override
        public String toString() {
            return Constants.MPC;
        }
    },

    @JsonProperty(Constants.OPC)
    OPC {
        @Override
        public String toString() {
            return Constants.OPC;
        }
    },

    @JsonProperty(Constants.EPC)
    EPC {
        @Override
        public String toString() {
            return Constants.EPC;
        }
    },

    @JsonProperty(Constants.MIC)
    MIC {
        @Override
        public String toString() {
            return Constants.MIC;
        }
    };

    private static class Constants {
        public static final String PMm = "PMm";
        public static final String PMp = "PMp";
        public static final String PMf = "PMf";
        public static final String ERf = "ERf";
        public static final String ERm = "ERm";
        public static final String GOL = "GOL";
        public static final String LYS = "LYS";
        public static final String END = "END";
        public static final String VAC = "VAC";
        public static final String MOM = "MOM";
        public static final String MIM = "MIM";
        public static final String THp = "THp";
        public static final String THb = "THb";
        public static final String GnO = "GnO";
        public static final String GnI = "GnI";
        public static final String GpI = "GpI";
        public static final String ARC = "ARC";
        public static final String LPC = "LPC";
        public static final String MPC = "MPC";
        public static final String OPC = "OPC";
        public static final String EPC = "EPC";
        public static final String MIC = "MIC";
    }
}
