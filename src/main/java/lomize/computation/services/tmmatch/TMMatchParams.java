package lomize.computation.services.tmmatch;

import lombok.Data;
import lomize.computation.controllers.request.TMMatchRequest;

@Data
public class TMMatchParams {
	private String sequence1, sequence2;
	private String pdbOutFilename;

	public TMMatchParams(TMMatchRequest req) {
		this.sequence1 = req.getSequence1();
        this.sequence2 = req.getSequence2();
		this.pdbOutFilename = req.getFilename() + ".out.pdb";
	}
}
