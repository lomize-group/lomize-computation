package lomize.computation.services.tmmatch;

import com.amazonaws.regions.Regions;

public class TMMatchConfig {
	public static final String RESULTS_PAGE_FILENAME = "tmmatch_results.html";
	public static final String TEMPLATE_FILENAME = "tmmatch_results.ftl";

	public static final String BUCKET_NAME = "tmmatch-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "TMMatch Computation Results";

	public static final String EXECUTABLE_NAME = "tmmatch";
	public static final String PROCESS_NAME = "tmmatch";
	public static final String SUBDIRECTORY_NAME = "tmp";

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";
}
