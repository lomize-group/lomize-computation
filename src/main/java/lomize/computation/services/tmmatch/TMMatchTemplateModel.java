package lomize.computation.services.tmmatch;

import java.util.ArrayList;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TMMatchTemplateModel {
	private final ArrayList<String> messageList = new ArrayList<String>();
	private String pdbFileUrl = null;

	public void addMessage(final String message) {
		this.messageList.add(message);
	}
}
