package lomize.computation.services.permm;

import com.amazonaws.regions.Regions;

public class PerMMConfig {
	public static final String RESULTS_PAGE_FILENAME = "permm_results.html";
	public static final String TEMPLATE_FILENAME = "permm_results.ftl";

	public static final String BUCKET_NAME = "permm-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "PerMM Computation Results";

	public static final String EXECUTABLE_NAME = "permm";
	public static final String PROCESS_NAME = "permm";

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";

	public static final String getGlmolUrl(final String pdbFileUrl) {
		return GLMOL_BASE_URL + pdbFileUrl;
	}

	private static final String GLMOL_BASE_URL = "https://permm.phar.umich.edu/server/glmol?url=";
}
