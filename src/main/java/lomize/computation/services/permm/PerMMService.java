package lomize.computation.services.permm;

import static lomize.computation.services.permm.PerMMConfig.RESULTS_PAGE_FILENAME;

import java.io.File;
import java.io.IOException;
import java.nio.file.StandardCopyOption;

import org.apache.http.entity.ContentType;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;

public class PerMMService extends AbstractExecutionService<PerMMParams> {

	public PerMMService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	private static String getOutputFilename(final String inputFilename, final String separator) {
		String[] splitFilename = inputFilename.split("\\.");
		splitFilename[0] += separator;
		return String.join(".", splitFilename);
	}

	public Void call() {
		this.env.setExecutableInput(this.params.getTemperature().toString(), this.params.getPh().toString(),
				this.params.getOptimizationGlobal().toString(), this.params.getEstimatePermeabilityPo().toString(),
				this.params.getInputPdbFilename());

		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (IOException e) {
			throw new RuntimeException("Error executing ppm: " + e.getMessage());
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing ppm: " + e.getMessage());
		}

		final String originalPathwayFilename = getOutputFilename(this.params.getInputPdbFilename(), "out");
		File pathwayFile = this.env.getFile(originalPathwayFilename);
		final boolean pathwayFileExists = pathwayFile.exists();
		final String newPathwayFilename = "pathway_" + this.params.getInputPdbFilename();

		final String originalBoundFilename = getOutputFilename(this.params.getInputPdbFilename(), "1");
		File boundFile = this.env.getFile(originalBoundFilename);
		final boolean boundFileExists = boundFile.exists();
		final String newBoundFilename = "bound_state_" + this.params.getInputPdbFilename();
		
		final String pathwayPdbFileUrl = this.uploader.getFileDestinationPath(newPathwayFilename);
		final String boundPdbFileUrl = this.uploader.getFileDestinationPath(newBoundFilename);

		// render HTML template from output and upload
		PerMMTemplateModel templateModel = PerMMTemplateModel.fromOutput(this.env.readOutputLines());
		templateModel.setInputPdbFilename(this.params.getInputPdbFilename());

		// upload output files if they exist
		if (pathwayFileExists) {
			try {
				pathwayFile = Util.moveFile(pathwayFile, this.env.getFile(newPathwayFilename), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				throw new RuntimeException("Could not rename pathway file: " + e.getMessage());
			}
			pathwayFile.renameTo(this.env.getFile(newPathwayFilename));
			this.uploader.uploadFile(pathwayFile, newPathwayFilename, null);
			templateModel.setPathwayPdbFileUrl(pathwayPdbFileUrl);
		}
		if (boundFileExists) {
			try {
				boundFile = Util.moveFile(boundFile, this.env.getFile(newBoundFilename), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				throw new RuntimeException("Could not rename bound file: " + e.getMessage());
			}
			this.uploader.uploadFile(boundFile, newBoundFilename, null);
			templateModel.setBoundPdbFileUrl(boundPdbFileUrl);
		}

		final String resultsPageHtml = this.templateRenderer.render(templateModel);
		this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

		// send email
		if (this.emailer != null) {
			String message = "Your PerMM computation has finished.\n";

			if (pathwayFileExists) {
				message += "The generated Pathway PDB is accessible at: " + pathwayPdbFileUrl + "\n";
			}
			if (boundFileExists) {
				message += "The generated Bound State PDB is accessible at: " + boundPdbFileUrl + "\n";
			}

			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			message += "The generated statistics are accessible at: " + resultsUrl + "\n";

			this.emailer.send(message);
		}

		return null;
	}
}
