package lomize.computation.services.permm;

import lombok.Data;
import lombok.NoArgsConstructor;
import lomize.computation.common.Util;
import lomize.computation.common.YesNo;
import lomize.computation.controllers.request.PerMMRequest;

@Data
@NoArgsConstructor
public class PerMMParams {
	private Double temperature;
	private Double ph;
	private YesNo estimatePermeabilityPo;
	private YesNo optimizationGlobal;
	private String inputPdbFilename;

	public PerMMParams(final PerMMRequest req) {
		this.temperature = req.getTemperature();
		this.ph = req.getPh();
		this.estimatePermeabilityPo = req.getEstimatePermeabilityPo() ? YesNo.YES : YesNo.NO;
		this.optimizationGlobal = req.getOptimizationMethod() == OptimizationMethod.GLOBAL ? YesNo.YES : YesNo.NO;
		this.inputPdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
	}
}