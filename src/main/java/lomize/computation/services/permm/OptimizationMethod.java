package lomize.computation.services.permm;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OptimizationMethod {
	@JsonProperty(Constants.DRAG)
	DRAG {
		@Override
		public String toString() {
			return Constants.DRAG;
		}
	},

	@JsonProperty(Constants.GLOBAL)
	GLOBAL {
		@Override
		public String toString() {
			return Constants.GLOBAL;
		}
	};

	private static class Constants {
		public static final String DRAG = "drag";
		public static final String GLOBAL = "global";
	}
}