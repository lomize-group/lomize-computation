package lomize.computation.services.permm;

import java.util.ArrayList;

import lombok.Data;
import lombok.AccessLevel;
import lombok.Setter;
import lomize.computation.common.Measurement;

@Data
public class PerMMTemplateModel {
	public static class OutputParamNames {
		public static final String FREE_ENERGY_DOPC = "Free energy of binding (DOPC)";
		public static final String LOG_PERM_BLM = "Log of perm. coeff. - BLM";
		public static final String LOG_PERM_BBB_PO = "Log of perm. coeff - BBB (Po)";
		public static final String LOG_PERM_CACO2_PO = "Log of perm. coeff - CACO2 (Po)";
		public static final String LOG_PERM_BBB_PS = "Log of perm. coeff - BBB (Ps)";
		public static final String LOG_PERM_PAMPA_PO = "Log of perm. coeff - PAMPA-DS (Po)";
		public static final String LOG_PERM_PAMPA = "Log of perm. coeff - PAMPA-DS";
	}
	

	@Setter(AccessLevel.NONE)
	private ArrayList<String> outputLines = new ArrayList<>();

	public void addOutputLine(String outputData) {
		this.outputLines.add(outputData);
	}

	@Setter(AccessLevel.NONE)
	private ArrayList<Measurement> outputDataList = new ArrayList<>();

	public void addData(Measurement outputData) {
		this.outputDataList.add(outputData);
	}

	private String inputPdbFilename;
	@Setter(AccessLevel.NONE)
	private String pathwayPdbFileUrl;
	@Setter(AccessLevel.NONE)
	private String pathwayViewerUrl;

	public void setPathwayPdbFileUrl(String pathwayPdbFileUrl) {
		this.pathwayPdbFileUrl = pathwayPdbFileUrl;
		this.pathwayViewerUrl = PerMMConfig.getGlmolUrl(pathwayPdbFileUrl);
	}

	@Setter(AccessLevel.NONE)
	private String boundPdbFileUrl;
	@Setter(AccessLevel.NONE)
	private String boundViewerUrl;

	public void setBoundPdbFileUrl(String boundPdbFileUrl) {
		this.boundPdbFileUrl = boundPdbFileUrl;
		this.boundViewerUrl = PerMMConfig.getGlmolUrl(boundPdbFileUrl);
	}

	public static PerMMTemplateModel fromOutput(ArrayList<String> permmOutputLines) {
		PerMMTemplateModel templateModel = new PerMMTemplateModel();
		for (final String line : permmOutputLines) {
			templateModel.addOutputLine(line);

			String paramName = null;
			String unit = "";
			String value = "";
			final String[] lineSplit = line.split("\\s");
			if (line.startsWith(PerMMTemplateModel.OutputParamNames.FREE_ENERGY_DOPC)) {
				paramName = PerMMTemplateModel.OutputParamNames.FREE_ENERGY_DOPC;
				value = lineSplit[lineSplit.length - 2];
				unit = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_BLM)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_BLM;
				value = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_BBB_PO)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_BBB_PO;
				value = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_CACO2_PO)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_CACO2_PO;
				value = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_BBB_PS)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_BBB_PS;
				value = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_PAMPA_PO)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_PAMPA_PO;
				value = lineSplit[lineSplit.length - 1];
			} else if (line.startsWith(PerMMTemplateModel.OutputParamNames.LOG_PERM_PAMPA)) {
				paramName = PerMMTemplateModel.OutputParamNames.LOG_PERM_PAMPA;
				value = lineSplit[lineSplit.length - 1];
			}

			if (paramName != null) {
				Measurement outputData = new Measurement();
				outputData.setName(paramName);
				outputData.setValue(value);
				outputData.setUnit(unit);
				templateModel.addData(outputData);
			}
		}
		return templateModel;
	}

	private String submitAgainUrl = "https://permm.phar.umich.edu/server";
}