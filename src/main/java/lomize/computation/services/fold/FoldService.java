package lomize.computation.services.fold;

import static lomize.computation.services.fold.FoldConfig.RESULTS_PAGE_FILENAME;

import java.nio.file.Path;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;
import lomize.computation.controllers.request.FoldRequest;

public class FoldService extends AbstractExecutionService<FoldRequest> {
    private static final Logger logger = LoggerFactory.getLogger(FoldService.class);

    public FoldService(
            final Environment env,
            final IFileStorageClient uploader,
            final IEmailClient emailer,
            final TemplateRenderer templateRenderer) {
        super(env, uploader, emailer, templateRenderer);
    }

    @Override
    public Integer getTimeLimit() {
        // long time limit to effectively remove time limit
        return 10000; // days
    }

    public Void call() {
        // write request object to program input
        final File inputIn = this.env.getFile("input.in");
        final ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writeValue(inputIn, this.params);
        } catch (IOException error) {
            logger.error(error.toString());
            return null;
        }

        // run executable
        try {
            this.process = this.env.runExecutable();
            this.process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Error executing fold: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error executing fold: " + e.getMessage());
        }

        logger.debug("finished fold");

        // upload results, image files, and pdbs while filling in the model
        FoldTemplateModel templateModel = new FoldTemplateModel();
        templateModel.setPdbs(new ArrayList<>());

        File resultsArchive = this.env.getFile("test.results.tar.gz");
        logger.debug("uploading test.results.tar.gz");
        this.uploader.uploadFile(resultsArchive, "test.results.tar.gz", ContentType.DEFAULT_BINARY);
        templateModel.setResultsUrl(this.uploader.getFileDestinationPath("test.results.tar.gz"));

        File coverageFile = this.env.getFile("results/test_coverage.png");
        if (coverageFile.exists()) {
            logger.debug("uploading results/test_coverage.png");
            this.uploader.uploadFile(coverageFile, "test_coverage.png", ContentType.IMAGE_PNG);
            templateModel.setCoverageUrl(this.uploader.getFileDestinationPath("test_coverage.png"));
        }

        File paeFile = this.env.getFile("results/test_pae.png");
        if (paeFile.exists()) {
            logger.debug("uploading results/test_pae.png");
            this.uploader.uploadFile(paeFile, "test_pae.png", ContentType.IMAGE_PNG);
            templateModel.setPaeUrl(this.uploader.getFileDestinationPath("test_pae.png"));
        }

        File plddtFile = this.env.getFile("results/test_plddt.png");
        if (plddtFile.exists()) {
            logger.debug("uploading results/test_plddt.png");
            this.uploader.uploadFile(plddtFile, "test_plddt.png", ContentType.IMAGE_PNG);
            templateModel.setPlddtUrl(this.uploader.getFileDestinationPath("test_plddt.png"));
        }

        final File resultsDir = this.env.getFile("results");
        for (String path : resultsDir.list()) {
            if (path.endsWith(".pdb")) {
                File file = this.env.getFile("results/" + path);
                logger.debug("uploading " + file.getName());
                this.uploader.uploadFile(file, file.getName(), null);
                templateModel.getPdbs().add(new FoldTemplateModel.Pdb(file.getName(), this.uploader.getFileDestinationPath(file.getName())));
            }
        }
        templateModel.getPdbs().sort((pdb1, pdb2) -> pdb1.getName().compareTo(pdb2.getName()));

        // create html results page, upload, and write to disk
        // (results html url does not need to go in template model)
        final String resultsHtml = this.templateRenderer.render(templateModel);
        final File resultsFile = this.env.getFile(RESULTS_PAGE_FILENAME);
        logger.debug("results file: " + resultsFile.getAbsolutePath());

        try (FileWriter writer = new FileWriter(resultsFile)) {
            writer.write(resultsHtml);
        } catch (IOException e) {
            throw new RuntimeException("Error writing results html: " + e.getMessage());
        }

        logger.debug("uploading results file");
        this.uploader.uploadFile(resultsFile, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

        // send email with link to results page
        if (this.emailer != null) {
            String message = "Your fold computation has finished.\n"
                + "\n"
                + "The results are available at: " + this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME) + "\n";
            this.emailer.send(message);
            logger.debug("sending email:\n" + message);
        }

        logger.debug("finished");
        return null;
    }
}
