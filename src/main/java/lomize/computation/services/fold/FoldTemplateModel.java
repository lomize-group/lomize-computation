package lomize.computation.services.fold;

import java.util.ArrayList;

import lombok.Data;

@Data
public class FoldTemplateModel {
    // generated images of plots
    private String coverageUrl, paeUrl, plddtUrl;

    // generated pdb files
    private ArrayList<Pdb> pdbs;

    // tar.gz of all results
    private String resultsUrl;

    @Data
    public static class Pdb {
        private String name, pdbUrl, icn3dUrl, jmolUrl;

        public Pdb(String nameIn, String pdbUrlIn) {
            name = nameIn;
            pdbUrl = pdbUrlIn;
            icn3dUrl = FoldConfig.getIcn3dUrl(pdbUrl);
            jmolUrl = FoldConfig.getJmolUrl(pdbUrl);
        }
    }
}
