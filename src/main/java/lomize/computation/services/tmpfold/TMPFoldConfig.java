package lomize.computation.services.tmpfold;

import com.amazonaws.regions.Regions;

public class TMPFoldConfig {
	public static final String RESULTS_PAGE_FILENAME = "tmpfold_results.html";
	public static final String TEMPLATE_FILENAME = "tmpfold_results.ftl";

	public static final String BUCKET_NAME = "tmpfold-results";
	public static final Regions BUCKET_REGION = Regions.US_EAST_2;

	public static final String EMAIL_SUBJECT = "TMPFold Computation Results";

	public static final String EXECUTABLE_NAME = "bind1";
	public static final String PROCESS_NAME = "tmpfold";

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";

	public static String getGlmolUrl(final String pdbFileUrl) {
		return GLMOL_URL + pdbFileUrl;
	}

	public static String getSubunitFilename(final String inputPdbCode, final String subunitName) {
		return inputPdbCode + subunitName + "path.pdb";
	}

	private static final String GLMOL_URL = "https://opm.phar.umich.edu/viewer?type=subunit&url=";
}