package lomize.computation.services.tmpfold;

import static lomize.computation.services.tmpfold.TMPFoldConfig.RESULTS_PAGE_FILENAME;
import static lomize.computation.services.tmpfold.TMPFoldConfig.getSubunitFilename;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.StandardCopyOption;

import org.apache.http.entity.ContentType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.Util;
import lomize.computation.common.cloud.IEmailClient;
import lomize.computation.common.cloud.IFileStorageClient;
import lomize.computation.common.execution.Environment;
import lomize.computation.services.AbstractExecutionService;
import lomize.computation.services.tmpfold.TMPFoldTemplateModel.Subunit;;

public class TMPFoldService extends AbstractExecutionService<TMPFoldParams> {
	private static final Logger logger = LoggerFactory.getLogger(TMPFoldService.class);

	public TMPFoldService(final Environment env, final IFileStorageClient uploader, final IEmailClient emailer,
			final TemplateRenderer templateRenderer) {
		super(env, uploader, emailer, templateRenderer);
	}

	public Void call() {
		// set executable input
		this.env.setExecutableInput(this.params.getInputPdbFilename(), this.params.getSegments());
        logger.debug("running executable");

		// run executable
		try {
			this.process = this.env.runExecutable();
            this.process.waitFor();
		} catch (IOException e) {
			throw new RuntimeException("Error executing cellpm: " + e.getMessage());
		} catch (InterruptedException e) {
			throw new RuntimeException("Error executing cellpm: " + e.getMessage());
		}

        logger.debug("ran executable");

		// parse data from output
		TMPFoldTemplateModel templateModel = TMPFoldTemplateModel.fromOutput(this.env.readOutputLines());
		boolean hasGeneratedFiles = false;

		// set pdb file urls for each subunit and upload pdb files that exist
		for (Subunit subunit : templateModel.getSubunits()) {
			// get pointer to subunit file
			final String origPdbFilename = getSubunitFilename(templateModel.getPdbCode(), subunit.getName());
			File pdbFile = this.env.getFile(origPdbFilename);

			// if it exists, rename and upload it.
			// it technically doesn't need to be renamed locally in order to rename while
			// uploading,
			// but it's helpful for debugging
			if (pdbFile.isFile()) {
				hasGeneratedFiles = true;

				final String newFilename = templateModel.getPdbCode() + "_" + subunit.getName() + ".pdb";
				try {
					pdbFile = Util.moveFile(pdbFile, this.env.getFile(newFilename),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					throw new RuntimeException("Error: could not rename cellpm output file from " + origPdbFilename
							+ " to " + newFilename + "; " + e.getMessage());
				}
				this.uploader.uploadFile(pdbFile, pdbFile.getName(), null);
				subunit.setPdbFileUrl(this.uploader.getFileDestinationPath(pdbFile.getName()));
			}
		}

		// render HTML results page and upload it
		final String resultsPageHtml = this.templateRenderer.render(templateModel);
		this.uploader.uploadFile(resultsPageHtml, RESULTS_PAGE_FILENAME, ContentType.TEXT_HTML);

        String fname = this.env.getFile(RESULTS_PAGE_FILENAME).getAbsolutePath();
        logger.debug("results fname = " + fname);

        try (FileWriter writer = new FileWriter(fname)) {
            writer.write(resultsPageHtml);
        } catch (IOException e) {
            e.printStackTrace();
        }

		// send email
		if (this.emailer != null) {
			StringBuilder messageBuilder = new StringBuilder("Your TMPFold computation has finished.\n");

			if (hasGeneratedFiles) {
				messageBuilder.append("The generated PDBs are accessible at:\n");

				for (Subunit subunit : templateModel.getSubunits()) {
					if (subunit.getPdbFileUrl() != null) {
						messageBuilder.append("Subunit " + subunit.getName() + ": " + subunit.getPdbFileUrl() + "\n");
					}
				}
			}

			final String resultsUrl = this.uploader.getFileDestinationPath(RESULTS_PAGE_FILENAME);
			messageBuilder.append("The generated statistics are accessible at: " + resultsUrl + "\n");

			this.emailer.send(messageBuilder.toString());
		}

		return null;
	}
}
