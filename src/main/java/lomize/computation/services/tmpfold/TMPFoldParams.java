package lomize.computation.services.tmpfold;

import lombok.Data;
import lombok.NoArgsConstructor;
import lomize.computation.common.Util;
import lomize.computation.controllers.request.TMPFoldRequest;

@Data
@NoArgsConstructor
public class TMPFoldParams {
	private String segments = "";
	private String inputPdbFilename = "";

	public TMPFoldParams(final TMPFoldRequest req) {
		this.segments = req.getSegments();
		this.inputPdbFilename = Util.sanitizeFilename(req.getPdbFile().getOriginalFilename());
	}
}