package lomize.computation.services.tmpfold;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.AccessLevel;

@Data
public class TMPFoldTemplateModel {
	private static final Logger logger = LoggerFactory.getLogger(TMPFoldTemplateModel.class);

	public static class OutputSections {
		public static final String PDB_FILE = "PDB file:";
		public static final String ASSEMBLY_PATHWAY = "TM helix assembly pathway:";
		public static final String SUBUNIT = "Subunit:";
		public static final String SUBUNIT_TM_SEGMENTS = "TM segments:";
		public static final String SUBUNIT_ENERGIES = "Energy of association of all TM helices in this subunit:";
		public static final String HELICES = "Helix-helix association free energies:";
		public static final String FREE_ENERGY = "Free energy of association of TM helices from different subunits:";
		public static final String PAIRWISE_ENERGIES = "Energies of association of subunits:";
	}

	@Data
	@NoArgsConstructor
	public static class Segment {
		public String start;
		public String end;
	}

    @Data
    @NoArgsConstructor
    public static class AssemblyPathwayStep {
        private String num;
        private String gibbs;
        private String assembly;
    };

	@Data
	@NoArgsConstructor
	public static class Subunit {
		// e.g., "A", "B", "C", "M", "L", or "H"
		private String name;
		private String energyOfAssociationOutput;
		private String pdbFileUrl;

		@Setter(AccessLevel.NONE)
		private String glmolViewerUrl;

		private final ArrayList<Segment> tmSegments = new ArrayList<>();
		private final ArrayList<String[]> helixMatrix = new ArrayList<>();
        private final ArrayList<AssemblyPathwayStep> assemblyPathway = new ArrayList<>();
		private final ArrayList<String> assemblySteps = new ArrayList<>();

		public void setPdbFileUrl(final String pdbFileUrl) {
			this.pdbFileUrl = pdbFileUrl;
			this.glmolViewerUrl = TMPFoldConfig.getGlmolUrl(pdbFileUrl);
		}

		public void addTmSegment(final Segment tmSegment) {
			this.tmSegments.add(tmSegment);
		}

		public void addHelixData(final String[] helixDatum) {
			this.helixMatrix.add(helixDatum);
		}

        public void addAssemblyPathwayStep(final AssemblyPathwayStep step) {
            this.assemblyPathway.add(step);
        }

		public void addAssemblyStep(final String assemblyStep) {
			this.assemblySteps.add(assemblyStep);
		}
	}

	private String pdbCode;

	private ArrayList<String> outputLines = new ArrayList<>();
	private ArrayList<Subunit> subunits = new ArrayList<>();
	private ArrayList<String> freeEnergies = new ArrayList<>();
	private ArrayList<String> pairwiseEnergies = new ArrayList<>();
	private ArrayList<String> pdbFileUrls = new ArrayList<>();

	public void addOutputLine(final String outputLine) {
		this.outputLines.add(outputLine);
	}

	public void addSubunit(final Subunit subunit) {
		this.subunits.add(subunit);
	}

	public void addFreeEnergy(final String freeEnergy) {
		this.freeEnergies.add(freeEnergy);
	}

	public void addPairwiseEnergy(final String pairwiseEnergyLine) {
		this.pairwiseEnergies.add(pairwiseEnergyLine);
	}

	public static TMPFoldTemplateModel fromOutput(final ArrayList<String> tmpfoldOutputLines) {
		TMPFoldTemplateModel templateModel = new TMPFoldTemplateModel();
		String currentSection = null;
		Subunit curSubunit = null;

        final Pattern stepPattern = Pattern.compile("^Step\\s*(\\d+)\\s*(-?[\\d.]*)\\s*(.*)$"),
            pathwayPattern = Pattern.compile("^Pathway:\\s*(.*)$");

		for (String line : tmpfoldOutputLines) {
			line = line.trim();
			templateModel.addOutputLine(line);

			if (line.isEmpty()) {
				continue;
			}

			if (line.startsWith(OutputSections.PDB_FILE)) {
				// read pdb filename into model
				final String pdbFilename = line.substring(line.lastIndexOf(':') + 1).trim();
				templateModel.setPdbCode(pdbFilename.split("\\.")[0]);
			} else if (line.startsWith(OutputSections.ASSEMBLY_PATHWAY)) {
				currentSection = OutputSections.ASSEMBLY_PATHWAY;
			} else if (line.startsWith(OutputSections.SUBUNIT)) {
				currentSection = OutputSections.SUBUNIT;

				// start reading a new subunit
				curSubunit = new Subunit();
				curSubunit.setName(line.substring(line.lastIndexOf(':') + 1).trim());

				// add to list of subunits;
				templateModel.addSubunit(curSubunit);
			} else if (line.startsWith(OutputSections.SUBUNIT_TM_SEGMENTS)) {
				currentSection = OutputSections.SUBUNIT_TM_SEGMENTS;

				// get array of whitespace-separated numbers from the right side of the colon
				String[] nums = line.substring(line.lastIndexOf(':') + 1).trim().split("\\s+");
				String start = null;

				// get them in pairs of format "num1-num2", "num3-num4", etc.
				for (String num : nums) {
					if (start == null) {
						start = num;
					} else {
						// can create a new segment
						Segment seg = new Segment();
						seg.setStart(start);
						seg.setEnd(num);

						// add segment to subunit
						curSubunit.addTmSegment(seg);

						// reset start to read another segment
						start = null;
					}
				}
			} else if (line.startsWith(OutputSections.SUBUNIT_ENERGIES)) {
				currentSection = OutputSections.SUBUNIT_ENERGIES;
				curSubunit.setEnergyOfAssociationOutput(line);	
			} else if (line.startsWith(OutputSections.HELICES)) {
				currentSection = OutputSections.HELICES;
			} else if (line.startsWith(OutputSections.FREE_ENERGY)) {
				currentSection = OutputSections.FREE_ENERGY;
			} else if (line.startsWith(OutputSections.PAIRWISE_ENERGIES)) {
				currentSection = OutputSections.PAIRWISE_ENERGIES;
			} else if (currentSection != null) {
				if (currentSection == OutputSections.HELICES) {
					// add each row of the helix energies matrix
					curSubunit.addHelixData(line.split("\\s+"));
				} else if (currentSection == OutputSections.FREE_ENERGY) {
					templateModel.addFreeEnergy(line);
				} else if (currentSection == OutputSections.PAIRWISE_ENERGIES) {
					templateModel.addPairwiseEnergy(line);
				} else if (currentSection == OutputSections.ASSEMBLY_PATHWAY) {
                    final Matcher stepMatch = stepPattern.matcher(line),
                        pathwayMatch = pathwayPattern.matcher(line);

                    if (stepMatch.matches()) {
                        AssemblyPathwayStep step = new AssemblyPathwayStep();
                        step.num = stepMatch.group(1);
                        step.gibbs = stepMatch.group(2);
                        step.assembly = stepMatch.group(3);
                        curSubunit.addAssemblyPathwayStep(step);
                    } else if (pathwayMatch.matches()) {
                        AssemblyPathwayStep step = new AssemblyPathwayStep();
                        step.num = "Pathway";
                        step.gibbs = null;
                        step.assembly = pathwayMatch.group(1);
                        curSubunit.addAssemblyPathwayStep(step);
                    } else {
                        logger.error("unexpected line: " + line);
                        break;
                    }
				}
			}
		}
		return templateModel;
	}
}
