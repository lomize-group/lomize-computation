package lomize.computation.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TopologyInOut {
	@JsonProperty(Constants.IN)
	IN {
		@Override
		public String toString() {
			return Constants.IN;
		}
	},

	@JsonProperty(Constants.OUT)
	OUT {
		@Override
		public String toString() {
			return Constants.OUT;
		}
	};

	private static class Constants {
		public static final String IN = "in";
		public static final String OUT = "out";
	}
}