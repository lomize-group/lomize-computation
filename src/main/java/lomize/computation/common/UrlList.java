package lomize.computation.common;

import java.util.ArrayList;
import java.util.Collection;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class UrlList {

	@Data
	@NoArgsConstructor
	public static class Link {
		private String label;
		private String url;

		public Link(final String label, final String url) {
			this.label = label;
			this.url = url;
		}
	}

	private final ArrayList<Link> links;

	public UrlList() {
		this.links = new ArrayList<Link>();
	}

	public UrlList(final Collection<Link> links) {
		this.links = new ArrayList<Link>();
		this.links.addAll(links);
	}

	public void add(final Link link) {
		this.links.add(link);
	}

	public void add(final String label, final String url) {
		this.links.add(new Link(label, url));
	}
}