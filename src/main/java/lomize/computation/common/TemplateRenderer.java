package lomize.computation.common;

import java.io.IOException;
import java.io.StringWriter;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.Getter;

public class TemplateRenderer {

	private final Configuration templateCfg;

	@Getter
	private final String templateFilename;

	public TemplateRenderer(final Configuration templateCfg, final String templateFilename) {
		this.templateCfg = templateCfg;
		this.templateFilename = templateFilename;
	}

	public TemplateRenderer(final String templateFilename) {
		this.templateCfg = Util.getStandardTemplateConfig();
		this.templateFilename = templateFilename;
	}

	public String render(final Object templateData) {
		final StringWriter templateWriter = new StringWriter();
		try {
			final Template template = this.templateCfg.getTemplate(this.templateFilename);
			template.process(templateData, templateWriter);
		} catch (IOException e) {
			throw new RuntimeException("Error loading template ''" + this.templateFilename +"'': " + e.getMessage());
		} catch (TemplateException e) {
			throw new RuntimeException("Error rendering template ''" + this.templateFilename + "'': " + e.getMessage());
		}
		
		return templateWriter.toString();
	}

}