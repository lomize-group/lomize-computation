package lomize.computation.common;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONArray;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

public class Util {

	public static String getUtcTimeStampString() {
		final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		final Date now = new Date();
		return format.format(now);
	}

	public static Configuration getStandardTemplateConfig() {
		Configuration templateCfg = new Configuration(Configuration.VERSION_2_3_30);
		templateCfg.setDefaultEncoding(StandardCharsets.UTF_8.toString());
		templateCfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		try {
			templateCfg.setDirectoryForTemplateLoading(TEMPLATE_DIRECTORY);
		} catch (IOException e) {
			throw new RuntimeException("Error loading templates directory: " + TEMPLATE_DIRECTORY.getAbsolutePath());
		}
		return templateCfg;
	}

	public static String getTimestampedRandomFolderName() {
		final int randomNum = ThreadLocalRandom.current().nextInt(0, 10000000 + 1);
		return getUtcTimeStampString() + "_" + randomNum;
	}

	public static File getProcessDirectory(final String processName) {
		return Paths.get(PROCESSES, processName).toFile();
	}

	public static File getExecutableDirectory(final String processName) {
		return Paths.get(RESOURCES, EXECUTABLES, processName).toFile();
	}

	public static String sanitizeFilename(final String filename) {
		return filename.replaceAll("[^a-zA-Z0-9\\._]+", "_");
	}

	public static File moveFile(File src, File dest, CopyOption... options) throws IOException {
		return Files.move(src.toPath(), dest.toPath(), options).toFile();
	}

	public static String[] firstWords(final String str, final int numWords) {
		return firstWords(str, "\\s+", numWords);
	}

	public static String[] firstWords(final String str, final String regex, final int numWords) {
		String[] sep = str.split(regex);

		// avoid indexing out of bounds
		final int length = Math.min(sep.length, numWords);

		String[] result = new String[length];
		for (int i = 0; i < length; ++i) {
			result[i] = sep[i];
		}
		return result;
	}

	public static String[] toStringArray(final JSONArray input) {
		if (input == null) {
			return null;
		}
		final String[] result = new String[input.length()];
		for (int i = 0; i < result.length; i++) {
			result[i] = input.optString(i);
		}
		return result;
	}

	public static String[] toUniqueArray(final String[] input) {
		Set<String> set = new HashSet<String>(Arrays.asList(input));
		return set.toArray(new String[set.size()]);
	}

	private static final String RESOURCES = "resources";
	private static final String EXECUTABLES = "executables";
	public static final String PROCESSES = "processes";

	private static final File TEMPLATE_DIRECTORY = Paths.get(RESOURCES, "templates").toFile();
}
