package lomize.computation.common.client;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.xml.ws.http.HTTPException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.util.UriUtils;

public class OpmClient {

	public static final String OPM_BASE_URL = "https://lomize-group-opm.herokuapp.com/";
	public static final String OPM_UNIPROT_ENDPOINT = OPM_BASE_URL + "primary_structures/uniprotcode/";
	public static final String OPM_PFAM_ENDPOINT = OPM_BASE_URL + "primary_structures/pfamcode/";
	public static final String OPM_PDB_ENDPOINT = OPM_BASE_URL + "primary_structures/pdbid/";
	public static final String OPM_SPECIES_NAME_ENDPOINT = OPM_BASE_URL + "species/name/";
	public static final String OPM_FAMILY_PFAM_ENDPOINT = OPM_BASE_URL + "families/pfamcode/";

	public static class OpmNames {
		public static final String NAME = "name";
		public static final String PDBCODE = "pdbid";
		public static final String RESOLUTION = "resolution";
		public static final String TOPOLOGY_SUBUNIT = "topology_subunit";
		public static final String TOPOLOGY_SHOW_IN = "topology_show_in";
		public static final String THICKNESS = "thickness";
		public static final String THICKNESSERROR = "thicknesserror";
		public static final String TILT = "tilt";
		public static final String TILTERROR = "tilterror";
		public static final String GIBBS = "gibbs";
		public static final String MEMBRANE_ID = "membrane_id";
		public static final String SPECIES_ID = "species_id";
		public static final String FAMILY_ID = "family_id";
		public static final String SUPERFAMILY_ID = "superfamily_id";
		public static final String CLASSTYPE_ID = "classtype_id";
		public static final String TYPE_ID = "type_id";
		public static final String UNIPROT_CODES = "uniprotcodes";
		public static final String UNIPROT_CODE = "uniprotcode";

	}

	private final HttpClientWrapper client;

	public OpmClient(final HttpClient client) {
		this.client = new HttpClientWrapper(client);
	};

	public JSONObject getPrimaryStructure(final String pdbCode)
			throws IOException, HTTPException, ClientProtocolException {
		return this.client.getJson(OPM_PDB_ENDPOINT + pdbCode);
	}

	public JSONObject getSpeciesByName(final String speciesName)
			throws IOException, HTTPException, ClientProtocolException {
		final String encoded = UriUtils.encode(speciesName, StandardCharsets.UTF_8.toString());
		return this.client.getJson(OPM_SPECIES_NAME_ENDPOINT + encoded);
	}

	private JSONArray fetchMany(final String url)
			throws IOException, JSONException, HTTPException, ClientProtocolException {
		// OpmEntry result = new OpmEntry();
		JSONObject resp = this.client.getJson(url);

		try {
			return resp.getJSONArray("objects");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new JSONException("error fetching from OPM");
		}
	}

	public JSONArray getFamiliesByPfam(final String pfamCode)
			throws IOException, HTTPException, ClientProtocolException {
		return this.fetchMany(OPM_FAMILY_PFAM_ENDPOINT + pfamCode);
	}

	public JSONArray getPrimaryStructuresByUniprot(final String uniprotCode)
			throws IOException, HTTPException, ClientProtocolException {
		return this.fetchMany(OPM_UNIPROT_ENDPOINT + uniprotCode);
	}

	public JSONArray getPrimaryStructuresByPfam(final String pfamCode)
			throws IOException, HTTPException, ClientProtocolException {
		return this.fetchMany(OPM_PFAM_ENDPOINT + pfamCode);
	}

}