package lomize.computation.common.client;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.http.HTTPException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.w3c.dom.Document;

public class HttpClientWrapper {
	protected final HttpClient client;

	public HttpClientWrapper(final HttpClient client) {
		this.client = client;
	}

	public JSONObject getJson(final String url) throws IOException, HTTPException, ClientProtocolException {
		HttpUriRequest request = new HttpGet(url);
		HttpResponse response = this.client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != 200) {
			throw new HTTPException(statusCode);
		}

		String jsonString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

		return new JSONObject(jsonString);
	}

	public Document getXml(final String url)
			throws IOException, HTTPException, ClientProtocolException, ParserConfigurationException {
		HttpUriRequest request = new HttpGet(url);
		HttpResponse response = this.client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != 200) {
			throw new HTTPException(statusCode);
		}

		HttpEntity entity = response.getEntity();

		Document doc = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		try {
			doc = builder.parse(entity.getContent());
		} catch (Exception e) {
			throw new IOException("Could not parse HTTP response to XML document");
		}

		return doc;
	}
}