package lomize.computation.common.client;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.Data;

public class UniprotClient {

	@Data
	public static class UniprotResponse {
		private String proteinName;
		private String fullSpeciesName;
		private String shortSpeciesName;
		private String[] pfamCodes;
		private String accession;
		private String id;
	}

	public static final String UNIPROT_BASE_URL = "https://www.ebi.ac.uk/proteins/api/proteins/";

	private final HttpClientWrapper client;

	public UniprotClient(final HttpClient client) {
		this.client = new HttpClientWrapper(client);
	};

	private static String findProteinName(JSONObject json) {
		// get protein name
		String proteinName = null;
		try {
			proteinName = json.getJSONObject("protein").getJSONObject("recommendedName").getJSONObject("fullName")
					.getString("value");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (proteinName == null) {
			try {
				proteinName = json.getJSONObject("protein").getJSONArray("submittedName").getJSONObject(0)
						.getJSONObject("fullName").getString("value");
			} catch (JSONException e) {
				e.printStackTrace();
				proteinName = "";
			}
		}
		return proteinName;
	}

	private static String findFullSpeciesName(JSONObject json) {
		try {
			JSONObject species = json.getJSONObject("organism");
			JSONArray names = species.getJSONArray("names");

			// get the scientific name
			for (int i = 0; i < names.length(); ++i) {
				try {
					JSONObject nameObj = names.getJSONObject(i);
					if (nameObj.getString("type").equalsIgnoreCase("scientific")) {
						return nameObj.getString("value");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String shortenSpeciesName(final String fullSpeciesName) {
		if (fullSpeciesName == null) {
			return null;
		}

		String[] nameParts = fullSpeciesName.split("\\s+");
		if (nameParts.length == 0) {
			return null;
		}

		StringBuilder shortSpeciesName = new StringBuilder();
		shortSpeciesName.append(nameParts[0]);
		if (nameParts.length > 1) {
			shortSpeciesName.append(" ");
			shortSpeciesName.append(nameParts[1]);
		}

		return shortSpeciesName.toString();
	}

	private static String[] getPfamCodes(JSONObject json) {
		JSONArray dbRefs = null;
		try {
			dbRefs = json.getJSONArray("dbReferences");
		} catch (JSONException e) {
			return new String[0];
		}

		Set<String> pfamSet = new HashSet<>();
		for (int i = 0; i < dbRefs.length(); ++i) {
			try {
				JSONObject entry = dbRefs.getJSONObject(i);
				if (entry.getString("type").equalsIgnoreCase("pfam")) {
					pfamSet.add(entry.getString("id"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		String[] pfamCodes = pfamSet.toArray(new String[pfamSet.size()]);
		Arrays.sort(pfamCodes);
		return pfamCodes;
	}

	private static String getUniprotId(JSONObject json) {
		try {
			return json.getString("id").toUpperCase();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public UniprotResponse getEntry(final String accession) throws Exception {
		UniprotResponse result = new UniprotResponse();
		JSONObject json = this.client.getJson(UNIPROT_BASE_URL + accession);

		result.setAccession(accession);
		result.setId(getUniprotId(json));
		result.setProteinName(findProteinName(json));

		final String speciesName = findFullSpeciesName(json);

		result.setFullSpeciesName(speciesName);
		result.setShortSpeciesName(shortenSpeciesName(speciesName));
		result.setPfamCodes(getPfamCodes(json));

		return result;
	}

	public String[] getUniprotCodesFromIds(final String[] uniprotCodes) {
		// uniprot "code" (i.e., uniprot "accession") example: P08575, Q07837
		// uniprot "ID" (i.e., uniprot "name") examples: PTPRC_HUMAN, HRD1_YEAST
		// IDs are more useful than codes
		Set<String> uniprotIds = new HashSet<>();
		for (final String uniprotCode : uniprotCodes) {
			try {
				final UniprotResponse uniprot = this.getEntry(uniprotCode);
				uniprotIds.add(uniprot.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String[] uniprotIdArray = uniprotIds.toArray(new String[uniprotIds.size()]);
		Arrays.sort(uniprotIdArray);
		return uniprotIdArray;
	}

}