package lomize.computation.common.client;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.http.client.HttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import lombok.Data;

public class RcsbClient {

	@Data
	public static class RcsbPdbResponse {
		private String pdbCode;
		private String resolution;
	}

	@Data
	public static class RcsbMolResponse {
		private String pdbCode;
		private String subunit;
		private String[] uniprotCodes;
		private String[] speciesNames;
	}

	public static final String RCSB_MOL_URL = "https://www.rcsb.org/pdb/rest/describeMol?structureId=";
	public static final String RCSB_PDB_URL = "https://www.rcsb.org/pdb/rest/describePDB?structureId=";

	private final HttpClientWrapper client;

	public RcsbClient(final HttpClient client) {
		this.client = new HttpClientWrapper(client);
	};

	private static NodeList getNodeList(final Document doc, final String... xPathSequence) {
		XPathFactory xpf = XPathFactory.newInstance();
		XPath xpath = xpf.newXPath();
		try {
			XPathExpression expr = xpath.compile("/" + String.join("/", xPathSequence));
			return (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathException e) {
			e.printStackTrace();
			return null;
		}
	}

	public RcsbPdbResponse fetchPdb(final String pdbCode) throws Exception {
		RcsbPdbResponse result = new RcsbPdbResponse();

		Document doc = this.client.getXml(RCSB_PDB_URL + pdbCode);
		Element pdb = (Element) getNodeList(doc, "PDBdescription", "PDB").item(0);

		if (pdb == null) {
			return result;
		}

		result.setPdbCode(pdb.getAttribute("structureId").toLowerCase());

		String resMethod = pdb.getAttribute("expMethod");
		if (resMethod.equals("X-RAY DIFFRACTION")) {
			resMethod = null;
		} else if (resMethod.equals("ELECTRON MICROSCOPY")) {
			resMethod = "EM";
		} else if (resMethod.equals("SOLUTION NMR")) {
			resMethod = "NMR";
		} else {
			resMethod = "N/A";
		}

		if (resMethod == null || resMethod.equals("EM")) {
			final String resVal = pdb.getAttribute("resolution");
			if (resMethod == null) {
				result.setResolution(resVal);
			} else {
				result.setResolution(resVal + " " + resMethod);
			}
		} else {
			result.setResolution(resMethod);
		}

		return result;
	}

	private static String[] uniqueNodeList(final NodeList nodes, final String attr) {
		// get unique values
		Set<String> valSet = new HashSet<>();
		for (int i = 0; i < nodes.getLength(); ++i) {
			Element node = (Element) nodes.item(i);
			if (node != null) {
				String val = node.getAttribute(attr);
				if (val != null && !val.equals("")) {
					valSet.add(val);
				}
			}
		}
		String[] valArray = valSet.toArray(new String[valSet.size()]);
		return valArray;
	}

	public RcsbMolResponse fetchMol(final String pdbCode) throws Exception {
		RcsbMolResponse result = new RcsbMolResponse();
		result.setPdbCode(pdbCode);

		Document doc = this.client.getXml(RCSB_MOL_URL + pdbCode);

		// find subunit
		try {
			final String structureId = "structureId[@id='" + pdbCode.toUpperCase() + "']";
			Element chain = (Element) getNodeList(doc, "molDescription", structureId, "polymer", "chain").item(0);
			// set subunit
			result.setSubunit(chain.getAttribute("id"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		NodeList speciesNodes = getNodeList(doc, "molDescription", "structureId", "polymer", "Taxonomy");
		if (speciesNodes == null) {
			result.setSpeciesNames(new String[0]);
		} else {
			String[] speciesNames = uniqueNodeList(speciesNodes, "name");
			Arrays.sort(speciesNames);
			result.setSpeciesNames(speciesNames);
		}

		// get accession elements which contain uniprotcodes
		NodeList accessions = getNodeList(doc, "molDescription", "structureId", "polymer", "macroMolecule",
				"accession");

		// iterate over accessions to get uniprotcodes
		if (accessions == null) {
			result.setUniprotCodes(new String[0]);
		} else {
			String[] uniprotCodes = uniqueNodeList(accessions, "id");
			Arrays.sort(uniprotCodes);

			// set uniprotcodes
			result.setUniprotCodes(uniprotCodes);
		}

		return result;
	}
}