package lomize.computation.common.cloud;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Paths;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import org.apache.commons.codec.Charsets;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Setter;

public class AmazonS3StorageClient implements IFileStorageClient {

	private static final Logger logger = LoggerFactory.getLogger(AmazonS3StorageClient.class);
    // XXX: never set to false while deploying
    private static final boolean DO_UPLOAD = true;

	private final Regions region;
	private final AmazonS3 s3Client;
	private final String bucketName;

	private @Setter String destPath = "";

	public AmazonS3StorageClient(String bucketName) {
		this.region = Regions.DEFAULT_REGION;
		this.s3Client = AmazonS3ClientBuilder.standard().withRegion(this.region).build();
		this.bucketName = bucketName;
	}

	public AmazonS3StorageClient(String bucketName, Regions region) {
		this.region = region;
		this.s3Client = AmazonS3ClientBuilder.standard().withRegion(region).build();
		this.bucketName = bucketName;
	}

	private String getDestFilePath(String fileName) {
		return Paths.get(this.destPath, fileName).toString();
	}

	public void uploadFile(String fileContents, String destFileName, ContentType contentType) {
        logger.debug("uploading to " + this.bucketName + ": " + destFileName);
        if (!DO_UPLOAD) {
            return;
        }

		try {
			String filePath = this.getDestFilePath(destFileName);

			// convert file contents string to byte stream
			byte[] contentBytes = fileContents.getBytes(Charsets.UTF_8);
			ByteArrayInputStream dataStream = new ByteArrayInputStream(contentBytes);

			// specify byte length in metadata
			ObjectMetadata metadata = new ObjectMetadata();
			if (contentType != null) {
				metadata.setContentType(contentType.toString());
			}
			metadata.setContentLength(contentBytes.length);

			// create request with metadata and public ACL specified, and upload object
			PutObjectRequest request = new PutObjectRequest(this.bucketName, filePath, dataStream, metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead);
			this.s3Client.putObject(request);
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			logger.error("AmazonServiceException: ", e.getMessage());
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			logger.error("SdkClientException: ", e.getMessage());
			e.printStackTrace();
		}
	}

	public void uploadFile(final File fileToUpload, final String destFileName, final ContentType contentType) {
        logger.debug("uploading to " + this.bucketName + ": " + destFileName);
        if (!DO_UPLOAD) {
            return;
        }

		try {
			String filePath = this.getDestFilePath(destFileName);

			// create metadata
			ObjectMetadata metadata = new ObjectMetadata();
			if (contentType != null) {
				metadata.setContentType(contentType.toString());
			}

			// create request and upload object
			PutObjectRequest request = new PutObjectRequest(this.bucketName, filePath, fileToUpload)
					.withMetadata(metadata).withCannedAcl(CannedAccessControlList.PublicRead);
			this.s3Client.putObject(request);
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}
	}

	public String getFileDestinationPath(String fileName) {
		final String baseUrl = String.join(".", this.bucketName, "s3", this.region.getName(), "amazonaws.com");
		return "https://" + Paths.get(baseUrl, this.getDestFilePath(fileName)).toString();
	}

}
