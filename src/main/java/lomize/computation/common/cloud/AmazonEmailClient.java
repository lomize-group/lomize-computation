package lomize.computation.common.cloud;

import java.nio.charset.StandardCharsets;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

import org.apache.commons.validator.routines.EmailValidator;

public class AmazonEmailClient implements IEmailClient {
	private static final String FROM = "lomize.group.x@gmail.com";
	private static final Regions DEFAULT_REGION = Regions.US_EAST_1;

	private String to;
	private final String subject;
	private final AmazonSimpleEmailService client;

	public AmazonEmailClient(String to, String subject) {
		this.to = to;
		this.subject = subject;
		this.client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(AmazonEmailClient.DEFAULT_REGION)
				.build();
	}

	public AmazonEmailClient(String to, String subject, Regions region) {
		this.to = to;
		this.subject = subject;
		this.client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(region).build();
	}

	@Override
	public void send(final String message) {
		// don't send if invalid email address
		if (this.to == null || !EmailValidator.getInstance().isValid(this.to)) {
			return;
		}

		try {
			// build email request
			SendEmailRequest request = new SendEmailRequest()
					.withDestination(new Destination().withToAddresses(this.to))
					.withMessage(new Message()
							.withBody(new Body().withText(
									new Content().withCharset(StandardCharsets.UTF_8.toString()).withData(message)))
							.withSubject(new Content().withCharset(StandardCharsets.UTF_8.toString())
									.withData(this.subject)))
					.withSource(FROM);

			// send email
			this.client.sendEmail(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}