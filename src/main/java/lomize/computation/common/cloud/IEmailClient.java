package lomize.computation.common.cloud;

public interface IEmailClient {
	public void send(final String message);
}