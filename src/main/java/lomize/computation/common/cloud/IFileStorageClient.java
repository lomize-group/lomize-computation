package lomize.computation.common.cloud;

import java.io.File;

import org.apache.http.entity.ContentType;

public interface IFileStorageClient {

	/**
	 * Upload file from string
	 */
	public void uploadFile(final String fileData, final String destFileName, final ContentType contentType);

	/**
	 * Upload file from File, with Content-Type header specified.
	 */
	public void uploadFile(final File fileToUpload, final String destFileName, final ContentType contentType);


	/**
	 * Get the expected path-like location of a file with the given fileName.
	 */
	public String getFileDestinationPath(String fileName);

}