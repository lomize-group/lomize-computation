package lomize.computation.common.execution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import lombok.Getter;
import lomize.computation.common.Util;

public class Environment {

	@Getter
	private File envDirectory = null;

	@Getter
	private final String envName;

	private Executable executable;

	public static final String INPUT_FILENAME = "input.in";
	public static final String OUTPUT_FILENAME = "output.out";

	public Environment(final String envName) {
		this.envName = envName;
		this.executable = null;
		this.envDirectory = null;
	}

	public Environment() {
		this.envName = Util.getTimestampedRandomFolderName();
		this.executable = null;
		this.envDirectory = null;
	}

	public void copyChildren(final String srcFilePath) {
		File srcFile = new File(srcFilePath);
		this.copyChildren(srcFile);
	}

	public void copyChildren(final File srcFile) {
		try {
			if (srcFile.isDirectory()) {
				FileUtils.copyDirectory(srcFile, this.envDirectory);
			} else {
				FileUtils.copyFile(srcFile, this.envDirectory, false);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	public void createIn(final File parentDirectory) {
		this.envDirectory = Paths.get(parentDirectory.getAbsolutePath(), this.envName).toAbsolutePath().toFile();
		this.envDirectory.mkdirs();
	}

	public void copyFile(final File srcFile) throws IOException {
		FileUtils.copyFile(srcFile, this.envDirectory, false);
	}

	public void copyFile(final File srcFile, final String destPath) throws IOException {
		FileUtils.copyFile(srcFile, this.getFile(destPath), false);
	}

	public void createFile(final String dirPath) throws IOException {
		this.getFile(dirPath).createNewFile();
	}

	public void createDirectory(final String dirPath) {
		this.getFile(dirPath).mkdirs();
	}

	public Process runExecutable() throws IOException {
		if (this.executable == null) {
			throw new IOException("Executable not found");
		}
		this.executable.setInputStream(this.getFile(INPUT_FILENAME));
		this.executable.setOutputStream(this.getFile(OUTPUT_FILENAME));
		return this.executable.run();
	}

	public void setExecutableName(final String executableName) {
		this.executable = new Executable(this.getFile(executableName));
	}

    public void setExecutableInputLines(final String[] lines) {
		try (FileWriter writer = new FileWriter(this.getFile(INPUT_FILENAME).getAbsolutePath())) {
			for (String line : lines) {
				writer.write(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setExecutableInput(final String... lines) {
        setExecutableInputLines(lines);
    }

	public final ArrayList<String> readOutputLines() {
		ArrayList<String> result = new ArrayList<>();
		try (FileReader fReader = new FileReader(this.getFile(OUTPUT_FILENAME).getAbsolutePath());
				BufferedReader bufRead = new BufferedReader(fReader)) {
			String line;
			while ((line = bufRead.readLine()) != null) {
				result.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public File getFile(final String... filePath) {
		return Paths.get(this.envDirectory.getAbsolutePath(), filePath).toAbsolutePath().toFile();
	}

	public void cleanUp() throws IOException {
		if (this.envDirectory != null) {
			FileUtils.deleteDirectory(this.envDirectory);
		}
	}

}
