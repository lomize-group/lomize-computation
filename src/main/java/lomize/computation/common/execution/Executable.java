package lomize.computation.common.execution;

import java.io.File;
import java.io.IOException;

import lombok.Setter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Executable {
    private static final Logger logger = LoggerFactory.getLogger(Executable.class);

	private final File executable;

	@Setter
	private File inputStream = null;

	@Setter
	private File outputStream = null;

	public Executable(final String executablePath) {
		this.executable = new File(executablePath);
	}

	public Executable(final File executableFile) {
		this.executable = executableFile;
	}

	public Process run() throws IOException {
        logger.debug("running: " + this.executable.getName());
        boolean ok = this.executable.setExecutable(true);
        if (!ok) {
            logger.debug("setExecutable failed on " + this.executable.getName());
        }

		// create process with command to execute exeFile
		ProcessBuilder pb = new ProcessBuilder("./" + this.executable.getName())
				.directory(this.executable.getParentFile()); // set directory to run executable in

		if (this.inputStream != null) {
			pb = pb.redirectInput(this.inputStream); // STDIN
		}
		if (this.outputStream != null) {
			pb = pb.redirectOutput(this.outputStream); // STDOUT
		}

        Process process = pb.start();
        logger.debug("started pid " + process.pid());

		// once started, should no longer have execute bit
		this.executable.setExecutable(false);

        // the caller shall use waitFor
        return process;
	}
}
