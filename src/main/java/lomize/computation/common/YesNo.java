package lomize.computation.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum YesNo {
	@JsonProperty(Constants.YES)
	YES {
		@Override
		public String toString() {
			return Constants.YES;
		}
	},

	@JsonProperty(Constants.NO)
	NO {
		@Override
		public String toString() {
			return Constants.NO;
		}
	};

	public static YesNo fromBoolean(Boolean b) {
		return b ? YES : NO;
	}

	private static class Constants {
		public static final String YES = "yes";
		public static final String NO = "no";
	}
}