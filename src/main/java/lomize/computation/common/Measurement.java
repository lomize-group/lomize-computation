package lomize.computation.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Measurement {
	private String value = null;
	private String error = null;
	private String name = null;
	private String unit = null;
}