package lomize.computation.mock;

import java.util.ArrayList;

import lombok.Getter;
import lomize.computation.common.cloud.IEmailClient;

public class MockEmailClient implements IEmailClient {
	@Getter
	private ArrayList<String> messages = new ArrayList<>();

	public void send(final String message) {
		this.messages.add(message);
	}
}