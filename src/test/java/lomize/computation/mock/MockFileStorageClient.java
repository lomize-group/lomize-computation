package lomize.computation.mock;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.entity.ContentType;

import lombok.Getter;
import lomize.computation.common.cloud.IFileStorageClient;

public class MockFileStorageClient implements IFileStorageClient {

	@Getter
	private ArrayList<File> sentFiles = new ArrayList<>();
	
	@Getter
	private ArrayList<String> sentFilesDestNames = new ArrayList<>();

	@Getter
	private ArrayList<ContentType> sentFileContentTypes = new ArrayList<>();

	public void uploadFile(final File file, final String destName, final ContentType contentType) {
		sentFiles.add(file);
		sentFilesDestNames.add(destName);
		sentFileContentTypes.add(contentType);
	}

	@Getter
	private ArrayList<String> sentStrFiles = new ArrayList<>();
	
	@Getter
	private ArrayList<String> sentStrFilesDestNames = new ArrayList<>();

	@Getter
	private ArrayList<ContentType> sentStrFileContentTypes = new ArrayList<>();

	public void uploadFile(final String file, final String destName, final ContentType contentType) {
		sentStrFiles.add(file);
		sentStrFilesDestNames.add(destName);
		sentStrFileContentTypes.add(contentType);
	}

	public String getFileDestinationPath(final String fileName) {
		return fileName;
	}
}