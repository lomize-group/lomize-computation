package lomize.computation.controllers;

// @SpringBootTest
// @AutoConfigureMockMvc
public class FMAPControllerTest {

	// @Autowired
	// private MockMvc mockMvc;

	// @Autowired
	// private FMAPController controller;

	// private static JSONObject getGoodRequestBody() {
	// 	JSONObject obj = new JSONObject();
	// 	try {
	// 		obj.put("temperature", 200);
	// 		obj.put("ph", 7.5);
	// 		obj.put("gen3dModels", true);
	// 		obj.put("model", "Peptide in water");
	// 		obj.put("sequence", "abcdefg");
	// 	} catch (JSONException e) {
	// 		throw new RuntimeException(e);
	// 	}
	// 	return obj;
	// }

	// private static String ENDPOINT = "/fmap";

	// private ResultActions sendPostBody(Object body, MediaType contentType) throws Exception {
	// 	return this.mockMvc.perform(post(ENDPOINT).contentType(contentType).content(body.toString()));
	// }

	// @Test
	// public void testAutowiredExists() {
	// 	assertNotNull(this.controller);
	// 	assertNotNull(this.mockMvc);
	// }

	// @Test
	// public void testMissingParams() throws Exception {
	// 	JSONObject req = new JSONObject();
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andDo(print()).andExpect(status().isBadRequest());
	// }

	// @Test
	// public void testVaryingModels() throws Exception {
	// 	JSONObject req = getGoodRequestBody();

	// 	// try invalid model
	// 	req.put("model", "invalid_model type!!!!4352345!!!!!!");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	// pep_membrane needs membrane type specified
	// 	req.put("model", "pep_membrane");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	// membrane type is invalid; should still fail
	// 	req.put("membraneType", "invalid_membrane type!!!!32423512");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	// put in a valid membrane type; should be fine now
	// 	req.put("membraneType", "DOPC");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// 	req.put("membraneType", "dopc");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());


	// 	// pep_micelles needs micelle type specified
	// 	req.put("model", "pep_micelles");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	// micelle type is invalid; should still fail
	// 	req.put("micelleType", "invalid_micelle type!!!!324dfdf23512");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	// put in a valid micelle type; should be fine now
	// 	req.put("micelleType", "sds");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// 	req.put("micelleType", "DPC");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// 	req.put("micelleType", "lps");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// }

	// @Test
	// public void testVaryingTemperature() throws Exception {
	// 	JSONObject req = getGoodRequestBody();
	// 	req.put("temperature", "f");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("temperature", "zzzf");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("temperature", "22g");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("temperature", null);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("temperature", "22");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// 	req.put("temperature", "78.777");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("temperature", 453);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("temperature", 443.5);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// }

	// @Test
	// public void testVaryingPh() throws Exception {
	// 	JSONObject req = getGoodRequestBody();
	// 	req.put("ph", null);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("ph", "hehe");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("ph", "0");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("ph", "5");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("ph", "5.56");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("ph", 0);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("ph", 0.03);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("ph", 7);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// }

	// @Test
	// public void testVaryingGen3dModels() throws Exception {
	// 	JSONObject req = getGoodRequestBody();
	// 	req.put("gen3dModels", null);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", 1);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", "hello");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", "");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", 0);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", 0.5);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", "1.5");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", "1");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", "0");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("gen3dModels", false);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("gen3dModels", true);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// }

	// @Test
	// public void testVaryingOutputPdbFilename() throws Exception {
	// 	JSONObject req = getGoodRequestBody();
	// 	req.put("outputPdbFilename", ")(*(&^735975497");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("outputPdbFilename", "hello.fgh");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("outputPdbFilename", "hello.pdb");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isBadRequest());

	// 	req.put("outputPdbFilename", null);
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());

	// 	req.put("outputPdbFilename", "hello");
	// 	this.sendPostBody(req, MediaType.APPLICATION_JSON).andExpect(status().isOk());
	// }

	// @Test
	// public void testUserEmail() {

	// }

	// @Test
	// public void testPredefinedSegments() {

	// }
}