package lomize.computation.controllers.request;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TMDOCKRequestTest {

	@Autowired
	private Validator validator;

	@Test
	public void filenameFormatValidation() {
		TMDOCKRequest req = new TMDOCKRequest();
		req.setFilename("1uaz("); // bad filename
		req.setSequence("1234");
		req.setUserEmail("user@gmail.com");

		Set<ConstraintViolation<TMDOCKRequest>> violations = validator.validate(req);
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);

		req.setFilename("1uaz.pdb"); // bad filename
		violations = validator.validate(req);
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);

		req.setFilename("1uaz"); // good filename
		violations = validator.validate(req);
		assertThat(violations).isEmpty();

		req.setFilename("");
		violations = validator.validate(req);
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);

		req.setFilename("a");
		violations = validator.validate(req);
		assertThat(violations).isEmpty();
		assertThat(violations).hasSize(0);

		req.setFilename("abc");
		violations = validator.validate(req);
		assertThat(violations).isEmpty();
		assertThat(violations).hasSize(0);

		req.setFilename("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP");
		violations = validator.validate(req);
		assertThat(req.getFilename()).hasSizeGreaterThan(30);
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);

		req.setFilename("abcdefghijklmnopqrstuvwxyzABCD");
		violations = validator.validate(req);
		assertThat(req.getFilename()).hasSize(30);
		assertThat(violations).isEmpty();
		assertThat(violations).hasSize(0);
	}

	@Test
	public void filenameNotSetValidation() {
		// filename by default gets set to "result" (it does not need to be explicitly
		// sent)
		TMDOCKRequest req = new TMDOCKRequest();
		req.setSequence("1234");
		req.setUserEmail("user@gmail.com");

		Set<ConstraintViolation<TMDOCKRequest>> violations = validator.validate(req);
		assertThat(violations).isEmpty();
	}

	@Test
	public void nullSequenceValidation() {
		TMDOCKRequest req = new TMDOCKRequest();
		req.setFilename("1uaz");
		req.setUserEmail("user@gmail.com");
		// missing sequence

		Set<ConstraintViolation<TMDOCKRequest>> violations = validator.validate(req);
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);

		req.setSequence("abcd"); // good sequence
		violations = validator.validate(req);
		assertThat(violations).isEmpty();
	}

	@Test
	public void nullEmailIsValid() {
		TMDOCKRequest req = new TMDOCKRequest();
		req.setFilename("1uaz");
		req.setSequence("123");

		Set<ConstraintViolation<TMDOCKRequest>> violations = validator.validate(req);
		assertThat(violations).isEmpty();

		req.setUserEmail("abcd@def.com"); // good sequence
		violations = validator.validate(req);
		assertThat(violations).isEmpty();
	}

	public void allOkay() {
		TMDOCKRequest req = new TMDOCKRequest();
		req.setFilename("1uaz"); // bad filename
		req.setSequence("1234");
		req.setUserEmail("user@gmail.com");

		Set<ConstraintViolation<TMDOCKRequest>> violations = validator.validate(req);
		assertThat(violations).isEmpty();
	}
}