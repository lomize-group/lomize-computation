package lomize.computation.services.ppm;


import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import lomize.computation.common.Measurement;
import lomize.computation.common.TemplateRenderer;

public class PPMTemplateModelTest {
	private final TemplateRenderer tr = new TemplateRenderer(PPMConfig.TEMPLATE_FILENAME);

	private static PPMTemplateModel getModelWithMessageList(int numLines) {
		final PPMTemplateModel model = new PPMTemplateModel();
		for (int i = 0; i < numLines; ++i) {
			model.addMessage("Output line " + i);
		}
		return model;
	}

	private static PPMTemplateModel getModelWitDataList(int numLines) {
		final PPMTemplateModel model = getModelWithMessageList(25);
		for (int i = 0; i < numLines; ++i) {
			model.addMessage("Output line " + i);

            PPMTemplateModel.Section section = new PPMTemplateModel.Section();
            section.setName("meas" + i);
            model.addSection(section);
			PPMTemplateModel.OutputData data = new PPMTemplateModel.OutputData();
			data.setSegments(i + ": " + "1-20, 28 -50");
			data.setSubunits("abcdefg hijklmnop");
			data.setTilt("." + (i * 2 + 3) + "" + i);

			model.addData(data);
		}
		return model;
	}

	@Test
	public void testBasic() {
		PPMTemplateModel model = getModelWithMessageList(25);
		model.setPdbFileUrl("https://somewebsite.com/testPdbFile.pdb");
		model.setGibbs("42.3");

		Measurement thickness = new Measurement();
		thickness.setValue("0.6");
		thickness.setError(".5");
		model.setThickness(thickness);

		Measurement tilt = new Measurement();
		tilt.setValue("0.43");
		tilt.setError("7");
		model.setTilt(tilt);

		String rendered = tr.render(model);

		// output lines should be there
		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 24"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 25")));

		assertThat(rendered, containsStringIgnoringCase("Download output file"));
		assertThat(rendered, containsStringIgnoringCase(">PDB file</a>"));

		// jmol
		assertThat(rendered, containsStringIgnoringCase("Image of the protein in membrane"));
		assertThat(rendered, containsStringIgnoringCase("jmol"));
		assertThat(rendered, containsStringIgnoringCase("?mol=" + model.getPdbFileUrl()));
		
		// measurements
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
	}

	@Test
	public void testNullMeasurements() {
		PPMTemplateModel model = getModelWithMessageList(25);
		model.setPdbFileUrl("https://somewebsite.com/testPdbFile.pdb");
		
		String gibbs = "42.3";
		model.setGibbs(gibbs);

		Measurement thickness = new Measurement();
		thickness.setValue("0.6");
		thickness.setError(".5");
		model.setThickness(thickness);

		Measurement tilt = new Measurement();
		tilt.setValue("0.43");
		tilt.setError("7");
		model.setTilt(tilt);

		String rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, not(containsStringIgnoringCase("No value calculated")));

		// null gibs missing gibbs value, shows "no value calculated"
		model.setGibbs(null);
		rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, not(containsStringIgnoringCase("42.3 kcal/mol")));
		assertThat(rendered, containsStringIgnoringCase("No value calculated"));
		model.setGibbs(gibbs);
		rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, not(containsStringIgnoringCase("No value calculated")));

		// null thickness missing thickness value, shows "no value calculated"
		model.setThickness(null);
		rendered = tr.render(model);
		assertThat(rendered, not(containsStringIgnoringCase("0.6 &#177; .5")));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, containsStringIgnoringCase("No value calculated"));
		model.setThickness(thickness);
		rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, not(containsStringIgnoringCase("No value calculated")));

		// null tilt missing tilt value, shows "no value calculated"
		model.setTilt(null);
		rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, not(containsStringIgnoringCase("0.43&#177; 7")));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, containsStringIgnoringCase("No value calculated"));
		model.setTilt(tilt);
		rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("0.6 &#177; .5"));
		assertThat(rendered, containsStringIgnoringCase("0.43&#177; 7"));
		assertThat(rendered, containsStringIgnoringCase("42.3 kcal/mol"));
		assertThat(rendered, not(containsStringIgnoringCase("No value calculated")));
	}

	@Test
	public void testNullPdbLink() {
		PPMTemplateModel model = getModelWitDataList(25);
		String rendered = tr.render(model);

		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 24"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 25")));

		assertThat(rendered, not(containsStringIgnoringCase("Download output file")));
		assertThat(rendered, not(containsStringIgnoringCase(">PDB file</a>")));
	}

	@Test
	public void testDataList() {
		PPMTemplateModel model = getModelWitDataList(25);
		model.setPdbFileUrl("https://somewebsite.com/testPdbFile.pdb");
		String rendered = tr.render(model);

		assertThat(rendered, containsStringIgnoringCase("meas0"));
		assertThat(rendered, containsStringIgnoringCase("meas1"));
		assertThat(rendered, containsStringIgnoringCase("meas24"));
		assertThat(rendered, not(containsStringIgnoringCase("meas25")));
		assertThat(rendered, containsStringIgnoringCase("abcdefg hijklmnop"));
		assertThat(rendered, containsStringIgnoringCase("1-20, 28 -50"));
	}
}
