package lomize.computation.services.permm;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import lomize.computation.services.permm.PerMMTemplateModel.OutputParamNames;
import lomize.computation.common.TemplateRenderer;

public class PerMMTemplateModelTest {
	private final TemplateRenderer tr = new TemplateRenderer(PerMMConfig.TEMPLATE_FILENAME);

	private static String SAMPLE_DIRPATH = "src/test/java/lomize/computation/services/permm/samples/";

	private static void saveStrToFile(final String text, final String filename) throws Exception {
		PrintWriter out = new PrintWriter(SAMPLE_DIRPATH + filename);
		out.print(text);
		out.close();
	}
	
	private static ArrayList<String> getOutputSample() {
		return new ArrayList<String>(Arrays.asList(
			new String[]{
				"hello heres a message 0",
				"hello heres a message 1",
				"hello heres a message 2",
				OutputParamNames.FREE_ENERGY_DOPC + " 1.5a kcal/mol ",
				"hello heres a message 3",
				OutputParamNames.LOG_PERM_BLM + " 7b",
				"hello heres a message 4",
				OutputParamNames.LOG_PERM_BBB_PO + " 5.4c",
				"hello heres a message 5",
				OutputParamNames.LOG_PERM_CACO2_PO + " none d",
				"hello heres a message 6",
				OutputParamNames.LOG_PERM_BBB_PS + " 0.003e",
				"hello heres a message 7",
				"hello heres a message 8",
				"hello heres a message 9",
				"hello heres a message 10",
				OutputParamNames.LOG_PERM_PAMPA + " 0.0 f",
				OutputParamNames.LOG_PERM_PAMPA_PO + " 0.3g",
			}
		));
	}

	private static PerMMTemplateModel getBasicModel() {
		PerMMTemplateModel templateModel = PerMMTemplateModel.fromOutput(getOutputSample());
		templateModel.setInputPdbFilename("inputFile.pdb");
		templateModel.setPathwayPdbFileUrl("pathways_inputFile.pdb");
		templateModel.setBoundPdbFileUrl("bound_state_inputFile.pdb");
		return templateModel;
	}

	@Test
	public void testBasic() throws Exception {
		PerMMTemplateModel templateModel = getBasicModel();
		String rendered = tr.render(templateModel);
		saveStrToFile(rendered, "test.html");

		// check output list
		assertThat(rendered, containsStringIgnoringCase(OutputParamNames.FREE_ENERGY_DOPC + " 1.5a"));
		assertThat(rendered, containsStringIgnoringCase("hello heres a message 1"));
		assertThat(rendered, containsStringIgnoringCase("hello heres a message 2"));
		assertThat(rendered, not(containsStringIgnoringCase("hello heres a message 155")));

		// check data list
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.FREE_ENERGY_DOPC + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("1.5a kcal/mol"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_BLM + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_BLM + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_BBB_PO + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_CACO2_PO + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_BBB_PS + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_PAMPA + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_PAMPA_PO + "</td>"));

		// downloads table
		assertThat(rendered, containsStringIgnoringCase(templateModel.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));

		// viewers
		assertThat(rendered, containsStringIgnoringCase(templateModel.getBoundViewerUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getPathwayViewerUrl()));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no output files were generated.")));

		// no collapse output list
		assert templateModel.getOutputLines().size() <= 20;
		assertThat(rendered, not(containsStringIgnoringCase("class=\"collapser bottomless\"")));	
		assertThat(rendered, not(containsStringIgnoringCase("Show more")));

		// change so collapse output list is there
		templateModel.addOutputLine("hello heres a message 11");
		templateModel.addOutputLine("hello heres a message 12");
		templateModel.addOutputLine("hello heres a message 13");
		templateModel.addOutputLine("hello heres a message 14");
		templateModel.addOutputLine("hello heres a message 15");
		templateModel.addOutputLine("hello heres a message 16");
		assert templateModel.getOutputLines().size() > 20;
		rendered = tr.render(templateModel);
		assertThat(rendered, containsStringIgnoringCase("class=\"collapser bottomless\""));	
		assertThat(rendered, containsStringIgnoringCase("Show more"));
	}

	@Test
	public void testNullUrls() {
		PerMMTemplateModel model = PerMMTemplateModel.fromOutput(getOutputSample());
		model.setInputPdbFilename("hello.pdb");
		String rendered = tr.render(model);

		assertThat(rendered, containsStringIgnoringCase("No output files generated."));
		assertThat(rendered, containsStringIgnoringCase("No images available; no output files were generated."));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State")));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State PDB")));
		assertThat(rendered, not(containsStringIgnoringCase("Translocation Pathway")));
		assertThat(rendered, not(containsStringIgnoringCase("Pathway PDB")));

		model.setPathwayPdbFileUrl("https://website.com/pathway_hello.pdb");
		rendered = tr.render(model);
		
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no output files were generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State")));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State PDB")));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayViewerUrl()));

		model = PerMMTemplateModel.fromOutput(getOutputSample());
		model.setBoundPdbFileUrl("https://website.com/bound_state_hello.pdb");
		model.setInputPdbFilename("hello.pdb");
		rendered = tr.render(model);
		
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no output files were generated.")));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, not(containsStringIgnoringCase("Translocation Pathway")));
		assertThat(rendered, not(containsStringIgnoringCase("Pathway PDB")));

		model.setPathwayPdbFileUrl("https://website.com/pathway_hello.pdb");
		model.setBoundPdbFileUrl("https://website.com/bound_state_hello.pdb");
		rendered = tr.render(model);
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no output files were generated.")));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundViewerUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayViewerUrl()));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
	}
}