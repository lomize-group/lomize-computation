package lomize.computation.services.permm;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.YesNo;
import lomize.computation.common.execution.Environment;
import lomize.computation.mock.MockEmailClient;
import lomize.computation.mock.MockFileStorageClient;

public class PerMMServiceTest {

	private static final File PROCESSES = new File("src/test/java/lomize/computation/services/permm/processes");
	private Environment env;
	private MockEmailClient emailer;
	private MockFileStorageClient uploader;
	private TemplateRenderer tr;

	@Before
	public void setUp() throws Exception {
		this.env = new Environment("test_process");
		this.env.createIn(PROCESSES);
		this.env.createFile(PerMMConfig.EXECUTABLE_NAME);
		this.env.setExecutableName(PerMMConfig.EXECUTABLE_NAME);
		this.emailer = new MockEmailClient();
		this.uploader = new MockFileStorageClient();
		this.tr = new TemplateRenderer(PerMMConfig.TEMPLATE_FILENAME);
	}

	@After
	public void tearDown() throws Exception {
		this.env.cleanUp();
	}

	@Test
	public void testInputFile() throws Exception {
		PerMMService mockedService = new PerMMService(this.env, this.uploader, this.emailer, this.tr);
		PerMMParams params = new PerMMParams();
		params.setPh(7.5);
		params.setEstimatePermeabilityPo(YesNo.YES);
		params.setInputPdbFilename("testingMol.pdb");
		params.setOptimizationGlobal(YesNo.NO);
		params.setTemperature(290.2);
		mockedService.setParams(params);
		mockedService.run();

		FileReader fReader = new FileReader(this.env.getFile(Environment.INPUT_FILENAME).getAbsolutePath());
		BufferedReader bufRead = new BufferedReader(fReader);
		String line = bufRead.readLine();
		assertEquals("290.2", line, "line 1 temperature was incorrect");
		line = bufRead.readLine();
		assertEquals("7.5", line, "line 2 ph was incorrect");
		line = bufRead.readLine();
		assertEquals("no", line, "line 3 optimization global was incorrect");
		line = bufRead.readLine();
		assertEquals("yes", line, "line 4 estimate permeability po was incorrect");
		line = bufRead.readLine();
		assertEquals("testingMol.pdb", line, "line 5 input filename was incorrect");
	}

}