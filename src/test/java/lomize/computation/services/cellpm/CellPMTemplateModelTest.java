package lomize.computation.services.cellpm;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import lomize.computation.services.cellpm.CellPMTemplateModel.OutputParamNames;
import lomize.computation.common.TemplateRenderer;

public class CellPMTemplateModelTest {
	private final TemplateRenderer tr = new TemplateRenderer(CellPMConfig.TEMPLATE_FILENAME);
	
	private static ArrayList<String> getOutputSample() {
		return new ArrayList<String>(Arrays.asList(
			new String[]{
				"hello heres a message 0",
				"hello heres a message 1",
				"hello heres a message 2",
				OutputParamNames.LOG_PERM_COEFF + " = 1.5a",
				"hello heres a message 3",
				"hello heres a message 3",
				"hello heres a message 3",
				"hello heres a message 4",
				"hello heres a message 5",
				"hello heres a message 7",
				"hello heres a message 7",
				"hello heres a message 8",
				"hello heres a message 10"
			}
		));
	}

	private static CellPMTemplateModel getBasicModel() {
		CellPMTemplateModel templateModel = CellPMTemplateModel.fromOutput(getOutputSample());
		templateModel.setPathwayPdbFileUrl("pathways_inputFile.pdb");
		templateModel.setBoundPdbFileUrl("bound_state_inputFile.pdb");
		return templateModel;
	}

	@Test
	public void testBasic() {
		CellPMTemplateModel templateModel = getBasicModel();
		String rendered = tr.render(templateModel);

		// check output list
		assertThat(rendered, containsStringIgnoringCase(OutputParamNames.LOG_PERM_COEFF + " = 1.5a"));
		assertThat(rendered, containsStringIgnoringCase("hello heres a message 1"));
		assertThat(rendered, containsStringIgnoringCase("hello heres a message 2"));
		assertThat(rendered, containsStringIgnoringCase("hello heres a message 10"));
		assertThat(rendered, not(containsStringIgnoringCase("hello heres a message 155")));

		// check data list
		assertThat(rendered, containsStringIgnoringCase("<td>" + OutputParamNames.LOG_PERM_COEFF + "</td>"));
		assertThat(rendered, containsStringIgnoringCase("<td>1.5a</td>"));

		// downloads table
		assertThat(rendered, containsStringIgnoringCase(templateModel.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));

		// viewers
		assertThat(rendered, containsStringIgnoringCase(templateModel.getBoundGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getBoundJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getPathwayGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(templateModel.getPathwayJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no output files were generated.")));

		// no collapse output list
		assert templateModel.getOutputLines().size() <= 20;
		assertThat(rendered, not(containsStringIgnoringCase("class=\"collapser bottomless\"")));	
		assertThat(rendered, not(containsStringIgnoringCase("Show more")));

		// change so collapse output list is there
		templateModel.addOutputLine("hello heres a message 11");
		templateModel.addOutputLine("hello heres a message 12");
		templateModel.addOutputLine("hello heres a message 13");
		templateModel.addOutputLine("hello heres a message 14");
		templateModel.addOutputLine("hello heres a message 15");
		templateModel.addOutputLine("hello heres a message 16");
		templateModel.addOutputLine("hello heres a message 17");
		templateModel.addOutputLine("hello heres a message 18");
		templateModel.addOutputLine("hello heres a message 19");
		templateModel.addOutputLine("hello heres a message 20");
		templateModel.addOutputLine("hello heres a message 21");
		assert templateModel.getOutputLines().size() > 20;
		rendered = tr.render(templateModel);
		assertThat(rendered, containsStringIgnoringCase("class=\"collapser bottomless\""));	
		assertThat(rendered, containsStringIgnoringCase("Show more"));
	}

	@Test
	public void testNoData() {
		CellPMTemplateModel model = new CellPMTemplateModel();
		String rendered = tr.render(model);
		assertThat(rendered, containsStringIgnoringCase("No data."));
	}

	@Test
	public void testNullUrls() {
		CellPMTemplateModel model = CellPMTemplateModel.fromOutput(getOutputSample());
		String rendered = tr.render(model);

		assertThat(rendered, containsStringIgnoringCase("No output files generated."));
		assertThat(rendered, containsStringIgnoringCase("No images available; no files were generated"));
		assertThat(rendered, not(containsStringIgnoringCase("No data.")));
		assertThat(rendered, not(containsStringIgnoringCase("No link provided")));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State")));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State PDB")));
		assertThat(rendered, not(containsStringIgnoringCase("Translocation Pathway")));
		assertThat(rendered, not(containsStringIgnoringCase("Pathway PDB")));

		model.setPathwayPdbFileUrl("https://website.com/pathway_hello.pdb");
		rendered = tr.render(model);
		
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("No images available; no files were generated")));
		assertThat(rendered, containsStringIgnoringCase("No link provided"));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, not(containsStringIgnoringCase("Bound State PDB")));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayJmolUrl()));

		model = CellPMTemplateModel.fromOutput(getOutputSample());
		model.setBoundPdbFileUrl("https://website.com/bound_state_hello.pdb");
		rendered = tr.render(model);
		
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, containsStringIgnoringCase("No link provided"));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, not(containsStringIgnoringCase("Pathway PDB")));

		model.setPathwayPdbFileUrl("https://website.com/pathway_hello.pdb");
		model.setBoundPdbFileUrl("https://website.com/bound_state_hello.pdb");
		rendered = tr.render(model);
		assertThat(rendered, not(containsStringIgnoringCase("No output files generated.")));
		assertThat(rendered, not(containsStringIgnoringCase("No link provided")));
		assertThat(rendered, containsStringIgnoringCase("Bound State"));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getBoundJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("Bound State PDB"));
		assertThat(rendered, containsStringIgnoringCase("Translocation Pathway"));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getPathwayJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("Pathway PDB"));
	}
}