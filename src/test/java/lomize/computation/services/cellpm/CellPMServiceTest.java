package lomize.computation.services.cellpm;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.execution.Environment;
import lomize.computation.mock.MockEmailClient;
import lomize.computation.mock.MockFileStorageClient;

public class CellPMServiceTest {

	private static final File PROCESSES = new File("src/test/java/lomize/computation/services/cellpm/processes");
	private Environment env;
	private MockEmailClient emailer;
	private MockFileStorageClient uploader;
	private TemplateRenderer tr;

	@Before
	public void setUp() throws Exception {
		this.env = new Environment("test_process");
		this.env.createIn(PROCESSES);
		this.env.createFile(CellPMConfig.EXECUTABLE_NAME);
		this.env.setExecutableName(CellPMConfig.EXECUTABLE_NAME);
		this.emailer = new MockEmailClient();
		this.uploader = new MockFileStorageClient();
		this.tr = new TemplateRenderer(CellPMConfig.TEMPLATE_FILENAME);
	}

	@After
	public void tearDown() throws Exception {
		this.env.cleanUp();
	}

	@Test
	public void testInputFile() throws Exception {
		CellPMService mockedService = new CellPMService(this.env, this.uploader, this.emailer, this.tr);
		CellPMParams params = new CellPMParams();
		params.setPh(7.5);
		params.setPdbFilename("hello");
		params.setTemperature(290.2);
		params.setSequence("abcdefg");
		params.setMembraneType(MembraneType.DOPC);
		mockedService.setParams(params);
		mockedService.run();

		FileReader fReader = new FileReader(this.env.getFile(Environment.INPUT_FILENAME).getAbsolutePath());
		BufferedReader bufRead = new BufferedReader(fReader);
		String line = bufRead.readLine();
		assertEquals("DOPC", line, "line 1 membraneType was incorrect");
		line = bufRead.readLine();
		assertEquals("290.2", line, "line 2 temperature was incorrect");
		line = bufRead.readLine();
		assertEquals("7.5", line, "line 3 ph was incorrect");
		line = bufRead.readLine();
		assertEquals("hello.pdb", line, "line 4 pdb filename was incorrect");
		line = bufRead.readLine();
		assertEquals("abcdefg", line, "line 5 sequence po was incorrect");
	}

}