package lomize.computation.services.opm.update.unformatted;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;

import lomize.computation.mock.MockFileStorageClient;

public class OpmPdbRetrievalServiceTest {

	private static String SAMPLE_DIRPATH = "src/test/java/lomize/computation/services/opm/samples/";

	private static void saveStrToFile(final String text, final String filename) throws Exception {
		PrintWriter out = new PrintWriter(SAMPLE_DIRPATH + filename);
		out.print(text);
		out.close();
	}

	private MockFileStorageClient uploader;

	@Before
	public void setUp() throws Exception {
		this.uploader = new MockFileStorageClient();
	}

	@Test
	public void testBasic() throws Exception {
		OpmPdbRetrievalService mockedService = new OpmPdbRetrievalService(this.uploader);
		mockedService.setPdbCodes(new String[] { "1uaz", "1fft", "1ygr" });
		mockedService.run();
		mockedService.getRunningFuture().join();
		assertThat(this.uploader.getSentStrFiles(), hasSize(1));

		saveStrToFile(this.uploader.getSentStrFiles().get(0), "1uaz_1fft_1ygr.out");
	}
}