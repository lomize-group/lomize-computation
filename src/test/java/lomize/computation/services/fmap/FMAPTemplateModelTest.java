package lomize.computation.services.fmap;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;

import org.junit.Test;

import lomize.computation.common.TemplateRenderer;

public class FMAPTemplateModelTest {

	private final TemplateRenderer tr = new TemplateRenderer(FMAPConfig.TEMPLATE_FILENAME);

	private static FMAPTemplateModel getModelWithMessageList(int numLines) {
		final FMAPTemplateModel model = new FMAPTemplateModel();
		ArrayList<String> messageList = new ArrayList<>();
		for (int i = 0; i < numLines; ++i) {
			messageList.add("Output line " + i);
		}
		model.setMessageList(messageList);
		return model;
	}

	@Test
	public void testBasic() {
		FMAPTemplateModel model = getModelWithMessageList(50);
		model.setPdbFileUrl("testPdbFile.pdb");
		final String rendered = this.tr.render(model);

		// table of links to pdb viewers should be there
		// pdb file
		assertThat(rendered, containsStringIgnoringCase(">pdb file</a>"));
		assertThat(rendered, containsStringIgnoringCase("<th>Image of pdb</th>"));
		// glmol
		assertThat(rendered, containsStringIgnoringCase(">GLmol</a>"));
		assertThat(rendered, containsStringIgnoringCase("?url=" + model.getPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(model.getGlmolUrl()));
		// jmol
		assertThat(rendered, containsStringIgnoringCase(">Jmol</a>"));
		assertThat(rendered, containsStringIgnoringCase(model.getJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("mol=" + model.getPdbFileUrl()));

		// output lines
		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 49"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 50")));		
	}

	@Test
	public void testNullPdbUrl() {
		FMAPTemplateModel model = getModelWithMessageList(50);
		String rendered = this.tr.render(model);

		// output lines should be there
		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 49"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 50")));

		// table of links to pdb viewers should not be there
		// pdb file
		assertThat(rendered, not(containsStringIgnoringCase(">pdb file</a>")));
		assertThat(rendered, not(containsStringIgnoringCase("<th>Image of pdb</th>")));
		//glmol
		assertThat(rendered, not(containsStringIgnoringCase(">GLmol</a>")));
		assertNull(model.getGlmolUrl());
		// jmol
		assertThat(rendered, not(containsStringIgnoringCase(">Jmol</a>")));
		assertNull(model.getJmolUrl());

		model.setPdbFileUrl("testPdbFile.pdb");
		rendered = this.tr.render(model);

		// now they should be there
		assertThat(rendered, containsStringIgnoringCase(">pdb file</a>"));
		assertThat(rendered, containsStringIgnoringCase("<th>Image of pdb</th>"));
		assertThat(rendered, containsStringIgnoringCase(">GLmol</a>"));
		assertThat(rendered, containsStringIgnoringCase(model.getGlmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("?url=" + model.getPdbFileUrl()));
		assertThat(rendered, containsStringIgnoringCase(">Jmol</a>"));
		assertThat(rendered, containsStringIgnoringCase(model.getJmolUrl()));
		assertThat(rendered, containsStringIgnoringCase("?mol=" + model.getPdbFileUrl()));
	}
	

}