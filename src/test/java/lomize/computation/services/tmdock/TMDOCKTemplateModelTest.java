package lomize.computation.services.tmdock;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import lomize.computation.common.TemplateRenderer;

public class TMDOCKTemplateModelTest {
	private final TemplateRenderer tr = new TemplateRenderer(TMDOCKConfig.TEMPLATE_FILENAME);

	private static TMDOCKTemplateModel getModelWithMessageList(int numLines) {
		final TMDOCKTemplateModel model = new TMDOCKTemplateModel();
		for (int i = 0; i < numLines; ++i) {
			model.addMessage("Output line " + i);
		}
		return model;
	}

	@Test
	public void testBasic() {
		TMDOCKTemplateModel model = getModelWithMessageList(50);
		model.setPdbFileUrl("https://somewebsite.com/testPdbFile.pdb");

		String rendered = tr.render(model);

		// output lines should be there
		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 49"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 50")));

		// glmol
		assertThat(rendered, containsStringIgnoringCase("GLMOL Visualization"));
		assertThat(rendered, containsStringIgnoringCase(">pdb file</a>"));
		assertThat(rendered, containsStringIgnoringCase("$(document).ready(function() {"));
	}

	@Test
	public void testNullPdbUrl() {
		TMDOCKTemplateModel model = getModelWithMessageList(50);

		String rendered = tr.render(model);

		// output lines should be there
		assertThat(rendered, containsStringIgnoringCase("Output line 0"));
		assertThat(rendered, containsStringIgnoringCase("Output line 1"));
		assertThat(rendered, containsStringIgnoringCase("Output line 49"));
		assertThat(rendered, not(containsStringIgnoringCase("Output line 50")));

		// glmol
		assertThat(rendered, not(containsStringIgnoringCase("GLMOL Visualization")));
		assertThat(rendered, not(containsStringIgnoringCase(">pdb file</a>")));
		assertThat(rendered, not(containsStringIgnoringCase("$(document).ready(function() {")));
	}

}