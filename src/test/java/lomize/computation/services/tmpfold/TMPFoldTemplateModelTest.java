package lomize.computation.services.tmpfold;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.junit.Test;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.services.tmpfold.TMPFoldTemplateModel.Subunit;

public class TMPFoldTemplateModelTest {

	private final TemplateRenderer tr = new TemplateRenderer(TMPFoldConfig.TEMPLATE_FILENAME);

	private static String SAMPLE_DIRPATH = "src/test/java/lomize/computation/services/tmpfold/samples/";

	private static ArrayList<String> readOutputLines(final String sampleFilename) throws Exception{
		FileReader fReader = new FileReader(SAMPLE_DIRPATH + sampleFilename);
		BufferedReader bufRead = new BufferedReader(fReader);

		ArrayList<String> result = new ArrayList<>();
		String line;
		while ((line = bufRead.readLine()) != null) {
			result.add(line);
		}

		fReader.close();
		bufRead.close();
		return result;
	}

	private static void saveStrToFile(final String text, final String filename) throws Exception {
		PrintWriter out = new PrintWriter(SAMPLE_DIRPATH + filename);
		out.print(text);
		out.close();
	}

	@Test
	public void testLargeSample() throws Exception {
		TMPFoldTemplateModel model = TMPFoldTemplateModel.fromOutput(readOutputLines("sample1.out"));
		for (Subunit s : model.getSubunits()) {
			s.setPdbFileUrl("fakePdbFileUrl.pdb");
		}
		final String rendered = tr.render(model);
		saveStrToFile(rendered, "sample1.html");
	}

}