package lomize.computation.services.tmpfold;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lomize.computation.common.TemplateRenderer;
import lomize.computation.common.execution.Environment;
import lomize.computation.mock.MockEmailClient;
import lomize.computation.mock.MockFileStorageClient;

public class TMPFoldServiceTest {
	private static final File PROCESSES = new File("src/test/java/lomize/computation/services/permm/processes");
	private Environment env;
	private MockEmailClient emailer;
	private MockFileStorageClient uploader;
	private TemplateRenderer tr;

	@Before
	public void setUp() throws Exception {
		this.env = new Environment("test_process");
		this.env.createIn(PROCESSES);
		this.env.createFile(TMPFoldConfig.EXECUTABLE_NAME);
		this.env.setExecutableName(TMPFoldConfig.EXECUTABLE_NAME);
		this.env.createFile(Environment.OUTPUT_FILENAME);

		this.emailer = new MockEmailClient();
		this.uploader = new MockFileStorageClient();
		this.tr = new TemplateRenderer(TMPFoldConfig.TEMPLATE_FILENAME);
	}

	@After
	public void tearDown() throws Exception {
		this.env.cleanUp();
	}

	@Test
	public void testInputFile() throws Exception {
		TMPFoldService mockedService = new TMPFoldService(this.env, this.uploader, this.emailer, this.tr);
		TMPFoldParams params = new TMPFoldParams();
		params.setSegments("akjbdfhsabfljashbljhsdbgjahs");
		mockedService.setParams(params);
		mockedService.run();

		FileReader fReader = new FileReader(this.env.getFile(Environment.INPUT_FILENAME).getAbsolutePath());
		BufferedReader bufRead = new BufferedReader(fReader);
		String line = bufRead.readLine();
		assertEquals("", line, "line 1 pdb filename was incorrect");
		line = bufRead.readLine();
		assertEquals("akjbdfhsabfljashbljhsdbgjahs", line, "line 2 segments was incorrect");
	}
}