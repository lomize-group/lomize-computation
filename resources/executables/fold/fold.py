#!/home/opm/localcolabfold/colabfold-conda/bin/python3
# adapted from https://colab.research.google.com/github/sokrypton/ColabFold/blob/main/AlphaFold2.ipynb
import csv
import json
import os
import re
import shutil
import sys
from pathlib import Path
from colabfold.utils import setup_logging
from colabfold.batch import get_queries, run


results = Path('results')
results.mkdir()
log = Path('log.txt')

def main():
    config = json.load(sys.stdin)
    sequences = [
        re.sub(r'\s', '', sequence)
        for sequence in config['sequences']
    ]

    with (results / 'test.csv').open('w') as f:
        writer = csv.writer(f)
        writer.writerow(['id', 'sequence'])
        writer.writerow(['test', ':'.join(sequences)])

    queries, is_complex = get_queries(results / 'test.csv')

    if config['templateMode'] == 'PDB70':
        use_templates = True
        custom_template_path = None
    elif config['templateMode'] == 'CUSTOM':
        use_templates = True
        custom_template_path = Path('template')
        custom_template_path.mkdir()
        for file in config['templateFiles']:
            filename, extension = os.path.splitext(file['name'])
            # alphafold requires 4 letter lowercase filename
            filename = f'{filename[:4].lower()}{extension}'
            with (custom_template_path / filename).open('w') as f:
                f.write(file['contents'])
    else:
        use_templates = False
        custom_template_path = None

    pair_mode = {
        'UNPAIRED_PAIRED': 'unpaired+paired',
        'PAIRED': 'paired',
        'UNPAIRED': 'unpaired',
    }[config['pairMode']]

    if config['msaMode'] == 'CUSTOM':
        assert config['msaFile'] is not None
        with (results / 'test.custom.a3m').open('w') as f:
            f.write(config['msaFile'])

    # remove leading underscore
    num_recycles = int(config['numRecycles'][1:])
    num_relax = config['numRelax']
    recycle_early_stop_tolerance = {
        'AUTO': None,
        '_0_0': 0.0,
        '_0_2': 0.2,
        '_0_5': 0.5,
        '_0_7': 0.7,
        '_1_0': 1.0,
    }[config['recycleEarlyStopTolerance']]
    max_seq, max_extra_seq = {
        'AUTO': (None, None),
        '_512_1024': (512, 1024),
        '_256_512': (256, 512),
        '_64_128': (64, 128),
        '_32_64': (32, 64),
        '_16_32': (16, 32),
    }[config['maxMsa']]

    setup_logging(log)
    run(
        model_type=config['modelType'].lower(),
        queries=queries,
        result_dir=results,
        num_models=5,
        model_order=[1, 2, 3, 4, 5],
        num_recycles=num_recycles,
        msa_mode=config['msaMode'].lower(),
        is_complex=is_complex,
        use_templates=use_templates,
        custom_template_path=custom_template_path,
        num_relax=num_relax,
        pair_mode=pair_mode,
        recycle_early_stop_tolerance=recycle_early_stop_tolerance,
        max_seq=max_seq,
        max_extra_seq=max_extra_seq,
        random_seed=config['randomSeed'],
        num_seeds=config['numSeeds'],
        use_dropout=config['useDropout'],
        use_cluster_profile=not config['disableClusterProfile'],
        save_recycles=config['saveRecycles'],
    )

try:
    main()
except Exception as exc:
    print(f'{exc.__class__.__name__}: {exc}')

sys.stdout.flush()
# include log in the output
# but tar can't archive the open file, so copy it
try:
    shutil.copy(log, results)
except:
    pass
shutil.copy('output.out', results)
print('fold run finished')
os.system('tar --create --gzip --file test.results.tar.gz results/*')
print('archive finished')
