<#import "./lib/helper.ftl" as Helper>

<#assign expandable=(outputLines?size > 20)>
<#assign collapserClass=(expandable?then('collapser bottomless', ''))>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>PerMM Results</title>
    <meta http-equiv="cache-control" content="no-cache" />
    <script language="JavaScript" src="https://cdn.plot.ly/plotly-latest.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src ="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src ="https://storage.googleapis.com/permm-assets/js/custom_chart.js"></script>
    <#if expandable>
        <script type="text/javascript">
            var expanded = false;
            $(document).ready(function(){
                var expander = $("#expand");
                if (expander.length == 0) {
                    return;
                }
                var msgs = $("#messages");
                expander.click(function(){
                    if (expanded) {
                        $(this).text("Show more");
                        msgs.addClass("collapser");
                    }
                    else {
                        $(this).text("Show less");
                        msgs.removeClass("collapser");
                    }
                    expanded = !expanded;
                });
            });
        </script>
    </#if>
    <style type="text/css">
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }
        table {
            width: 100%;
            max-width: 600px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            margin-bottom: 10px;
        }
        caption {
            color: #0066cc;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }
        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }
        .page-title {
          color: #0066cc;
          text-align: center;
        }
        .bottomless {
            border-bottom: none;
        }
        .collapser {
            max-height: 250px;
            position: relative;
            overflow: hidden;
            display: block;
            padding: 0;
        }
        .collapser:after {
            content:"";
            width: 100%;
            height: 100px;    
            position: absolute;
            left: 0;
            top: 150px;
            padding: 0;
            background:linear-gradient(rgba(255,255,255,0), #FFF);
        }
        .expand {
          width: 100%;
          cursor: pointer;
          color: #06c;
          font-size: 12px;
          padding-top: 0;
          padding-bottom: 5px;
        }
        .expand:hover {
          color: #23527c;
          text-decoration: underline;
        }
        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }
        tr {
            padding-bottom: 2px;
        }
        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }
        #chart_div {
            padding: 15px;
        }

        .centered {
            text-align: center;
        }

        div.buttons {
            margin-top: 15px;
            margin-bottom: 25px;
        }

    </style>
</head>
<body>

    <h1 class="page-title">${inputPdbFilename}</h1>

    <div id="fileData_area" style ="display:none;">
        <@Helper.ListAllLines outputLines />
    </div>

    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th>Energy Plot</th>
            </tr>
        </thead>
        <tbody>
            <tr class="row">
                <td><div id="chart_div" class="centered"></div></td>
            </tr>
        </tbody>
    </table>
    
    <br/>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption>Calculated Parameters</caption>
        <thead>
            <tr>
                <th colspan="2">Calculated Parameters</th>
            </tr>
        </thead>
        <tbody>
			<#list outputDataList as d>
                <tr class="row">
                    <td>${d.name}</td>
                    <td>${d.value}<#if d.unit??> ${d.unit}</#if></td>
                </tr>
            </#list>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Output Messages</th>
            </tr>
        </thead>
        <tbody>
            <tr class="row">
                <td class="${collapserClass}" id="messages">
                    <@Helper.ListAllLines outputLines />
                </td>
            </tr>
			<#if expandable>
                <tr>
                    <td class="expand"><span id="expand">Show more</span></td>
                </tr>
			</#if>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Download Output Files</th>
            </tr>
        </thead>
        <tbody>
            <#if Helper.notBlank(boundPdbFileUrl)>
                <tr class="row">
                    <td>
                        <a href="${boundPdbFileUrl}" download>Bound State PDB</a>
                    </td>
               </tr>
			</#if>
            <#if Helper.notBlank(pathwayPdbFileUrl)>
				<tr class="row">
                    <td>
                        <a href="${pathwayPdbFileUrl}" download>Pathway PDB</a>
                    </td>
                </tr>
			</#if>
            <#if !Helper.notBlank(boundPdbFileUrl) && !Helper.notBlank(pathwayPdbFileUrl)>
                <tr class="row">
                    <td>
                        No output files generated.
                    </td>
                </tr>
            </#if>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th>Images of the molecule in membrane</th>
            </tr>
        </thead>
        <tbody>
            <#if Helper.notBlank(boundViewerUrl)>
                <tr class="row">
                    <td>
                        <a href="${boundViewerUrl}" target="_blank">
                            Bound State
                        </a>
                    </td>
                </tr>
            </#if>
            <#if Helper.notBlank(pathwayViewerUrl)>
                <tr class="row">
                    <td>
                        <a href="${pathwayViewerUrl}" target="_blank">
                            Translocation Pathway
                        </a>
                    </td>
                </tr>
            </#if>
            <#if !Helper.notBlank(boundViewerUrl) && !Helper.notBlank(pathwayViewerUrl)>
                <tr class="row">
                    <td>
                        No images available; no output files were generated.
                    </td>
                </tr>
            </#if>
        </tbody>
    </table>
    <div class="centered buttons">
        <a href="${submitAgainUrl}" target="_blank"><button type="button">Submit new molecule</button></a>
        <br/>
        <button onClick="window.print()">Print this page</button>
    </div>
</body>
</html>
