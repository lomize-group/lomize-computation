<#import "./lib/helper.ftl" as Helper>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>FMAP Server - Membranome database</title>
	<style type="text/css">
		table {
			width: 100%;
			margin-top: 20px;
			margin-bottom: 20px;
			border-left: 1px solid #ddd;
			border-top: 1px solid #ddd;
		}

		table.small {
			max-width: 400px;
		}

		table.medium {
			max-width: 600px;
		}

		table.large {
			max-width: 800px;
		}

		th {
			color: #159;
			background: #eef3f5;
			border-bottom: 3px solid #999;
			border-right: 1px solid #ddd;
			padding: 5px;
			text-align: center;
		}

		tr {
			text-align: center;
			padding-bottom: 2px;
		}

		td {
			border-bottom: 1px solid #ddd;
			border-right: 1px solid #ddd;
			padding: 5px;
		}

		.button-link {
			display: inline-block;
		}

		.footer {
			width: 100%;
			text-align: center;
		}
	</style>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="robots" content="all" />
	<meta name="keywords"
		content="OPM, orientation, protein, membrane, localization, delta, Gibbs, transfer, topology, thickness, tilt, angle, subunit" />
	<script language="JavaScript" src="tooltip.js" type="text/javascript"></script>
	<script language="JavaScript" src="chime_tooltip.js" type="text/javascript"></script>
</head>

<body style="background-color: #fff;">
	<br><br>
    <#if (peptide.energy)?? || (peptide.aminoAcidSequence)??>
        <table class="medium" cellspacing="0" cellpadding="0" align="center">
            <thead>
                <tr>
                    <th colspan="2">Peptide or protein</th>
                </tr>
            </thead>
            <tbody>
                <tr style="text-align: left;">
                    <td>Amino acid sequence</td>
                    <td>Binding energy, kcal/mol</td>
                </tr>
                <tr style="text-align: left;">
                    <td>${(peptide.aminoAcidSequence)!'N/A'}</td>
                    <td>${(peptide.energy)!'N/A'}</td>
                </tr>
            </tbody>
        </table>
    </#if>
    <table class="medium" cellspacing="0" cellpadding="0" align="center">
        <thead>
            <tr>
                <th colspan="4">Environmental Conditions</th>
            </tr>
        </thead>
        <tbody>
            <tr style="text-align: left;">
                <td>Model</td>
                <td>Environment</td>
                <td>Temperature, K</td>
                <td>pH</td>
            </tr>
            <tr style="text-align: left;">
                <td>${(environment.model)!'N/A'}</td>
                <td>${(environment.environment)!'N/A'}</td>
                <td>${(environment.temperature)!'N/A'}</td>
                <td>${(environment.ph)!'N/A'}</td>
            </tr>
        </tbody>
    </table>
    <#if alphaHelices?size != 0>
        <table class="large" cellspacing="0" cellpadding="0" align="center">
            <thead>
                <tr>
                    <th colspan="8">&alpha;-helices</th>
                </tr>
            </thead>
            <tbody>
                <tr style="text-align: left;">
                    <td>N</td>
                    <td>Segment</td>
                    <td>Sequence</td>
                    <td>Stability relative to coil in water, kcal/mol</td>
                    <td>Stability relative to bound coil, kcal/mol</td>
                    <td>&Delta;G<sub>transfer</sub>, kcal/mol</td>
                    <td>Tilt angle, &deg;</td>
                    <td>Depth/thickness, &#x212B;</td>
                </tr>
                <#list alphaHelices as helix>
                    <tr style="text-align: left;">
                        <td>${(helix?index + 1)}</td>
                        <td>${helix.segment}</td>
                        <td>${helix.sequence}</td>
                        <td>${helix.stability1}</td>
                        <td>${(helix.stability2)!'N/A'}</td>
                        <td>${(helix.gibbs)!'N/A'}</td>
                        <td>${(helix.tiltAngle)!'N/A'}</td>
                        <td>${(helix.depthThickness)!'N/A'}</td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </#if>
	<table class="large" cellspacing="0" cellpadding="0" align="center">
		<thead>
			<tr>
				<th>Output Messages</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<@Helper.ListAllLines messageList />
				</td>
			</tr>
		</tbody>
	</table>
	<#if Helper.notBlank(pdbMultipleModelsUrl)>
		<table class="small" cellspacing="0" cellpadding="0" align="center">
			<thead>
				<tr>
					<th>Download output coordinate file as multiple models</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href="${pdbMultipleModelsUrl}" download>pdb file</a>
					</td>
				</tr>
			</tbody>
		</table>
	</#if>
	<#if Helper.notBlank(glmolUrl)>
		<table class="small" cellspacing="0" cellpadding="0" align="center">
			<thead>
				<tr>
					<th>Visualization of &#x03b1;-helices</th>
				</tr>
			</thead>
			<tbody>
				<#if Helper.notBlank(glmolUrl)>
					<tr>
						<td>
							<a href="${glmolUrl}" target="_blank">GLmol</a>
						</td>
					</tr>
				</#if>
			</tbody>
		</table>
	</#if>
    <table class="small" cellspacing="0" cellpadding="0" align="center">
        <thead>
            <tr>
                <th colspan="3">Downloading and visualization of &#x03b1;-helices</th>
            </tr>
        </thead>
        <tbody>
            <tr style="text-align: left;">
                <td>&#x03b1;-helix</td>
                <td>Download</td>
                <td>Image</td>
            </tr>
            <#list pdbModels as model>
                <tr style="text-align: left;">
                    <td>${model.num}</td>
                    <td><a href="${model.url}">pdb_file</a></td>
                    <td><a href="${model.icn3dUrl}" target="_blank">iCn3D</a></td>
                </tr>
            </#list>
        </tbody>
    </table>
	<div class="footer">
		<span>
			<a href="${submitAgainUrl}" target="_blank" class="button-link">
				<button type="button">Run another peptide</button>
			</a>
		</span>
		<button onClick="window.print()">Print this page</button>
	</div>
</body>

</html>
