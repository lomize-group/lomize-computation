<#import "./lib/helper.ftl" as Helper>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title>PPM Results</title>
   <meta http-equiv="cache-control" content="no-cache" />
   <style type="text/css">
        .dark {
            background: #eef3f5;
        }
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }
        table {
            width: 100%;
            max-width: 600px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            padding-bottom: 10px;
        }
        caption {
            color: #0066cc;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }

        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        tr {
            padding-bottom: 2px;
        }

        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }

        div {
            padding-top: 15px;
            text-align: center;
        }
    </style>
</head>
<body>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption>Orientation of Proteins in Membranes</caption>
        <thead>
            <tr>
                <th>Depth/Hydrophobic Thickness</th>
                <th>&Delta;G<sub>transfer</sub></th>
                <th>Tilt Angle</th>
            </tr>
        </thead>
        <tbody>
            <tr class="row light">
                <td>
                    <#if thickness??>
                        ${thickness.value} &#177; ${thickness.error} &#197;
                    <#else>
                        No value calculated
                    </#if>
                </td>
                <td>
                    <#if gibbs??>
                        ${gibbs} kcal/mol
                    <#else>
                        No value calculated
                    </#if>
                </td>
                <td>
                    <#if tilt??>
                        ${tilt.value}&#177; ${tilt.error}&#176;
                    <#else>
                        No value calculated
                    </#if>
                </td>
            </tr>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption>Membrane Embedded Residues<br />(in Hydrocarbon Core)</caption>
        <thead>
            <tr>
                <th>Subunits</th>
                <th>Tilt</th>
                <th>Segments</th>
            </tr>
        </thead>
        <tbody>
            <#list sections as s>
                <#if s.data?has_content>
                    <tr class="row dark">
                        <td></td>
                        <td></td>
                        <td>${s.name}</td>
                    </tr>
                    <#list s.data as d>
                        <tr class="row light">
                            <td>${d.subunits}</td>
                            <td>${d.tilt}</td>
                            <td>${d.segments}</td>
                        </tr>
                    </#list>
                </#if>
            </#list>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Output Messages</th>
            </tr>
        </thead>
        <tbody>
           <tr class="row1">
                <td>
                    <@Helper.ListAllLines messageList />
                </td>
           </tr>
        </tbody>
    </table>
    <#if Helper.notBlank(pdbFileUrl)>
        <table cellspacing="0" cellpadding="0" class="data" align="center">
            <caption></caption>
            <thead>
                <tr>
                    <th>Download Output File</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row1">
                    <td>
                        <a href="${pdbFileUrl}" download>PDB file</a>
                    </td>
            </tr>
            </tbody>
        </table>
    </#if>
    <#if Helper.notBlank(jmolUrl) || Helper.notBlank(icn3dUrl) >
        <table cellspacing="0" cellpadding="0" class="data" align="center">
            <thead>
                <tr>
                    <th>Image of the protein in membrane</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row light">
                    <td>
                        <#if Helper.notBlank(jmolUrl)>
                            <a href="${jmolUrl}" target="_blank">
                                jmol
                            </a>
                        </#if>
                        <#if Helper.notBlank(icn3dUrl)>
                            &nbsp;|&nbsp;
                            <a href="${icn3dUrl}" target="_blank">
                                iCn3D
                            </a>
                        </#if>
                    </td>
                    
                </tr>
            </tbody>
        </table>
    </#if>
    <div> 
        <input type="button" onClick="window.print() " value="Print this page">
    </div>
</body>
</html>
