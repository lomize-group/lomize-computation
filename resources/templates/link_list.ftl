<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Results</title>
	<style type="text/css">
		.centered {
			text-align: center;
		}
		.list-item {
			padding-bottom: 5px;
		}
	</style>
</head>
<body>
	<div class="centered">
		<h1>Results</h1>
		<#list links as link>
			<div class="list-item">
				<a href="${link.url}">${link.label}</a>
			</div>
		</#list>
	</div>
</body>
</html>