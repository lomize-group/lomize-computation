<#import "./lib/helper.ftl" as Helper>

<#assign expandable=(outputLines?size > 20)>
<#assign collapserClass=(expandable?then('collapser bottomless', ''))>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>CellPM Results</title>
    <meta http-equiv="cache-control" content="no-cache" />
    <script language="JavaScript" src="https://cdn.plot.ly/plotly-latest.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src ='https://www.gstatic.com/charts/loader.js'></script>
    <script type="text/javascript" src ="https://storage.googleapis.com/cellpm-assets/js/chart.js"></script>
    <#if expandable>
        <script type="text/javascript">
            var expanded = false;
            $(document).ready(function(){
                var expander = $("#expand");
                if (expander.length == 0) {
                    return;
                }
                var msgs = $('#messages');
                expander.click(function(){
                    if (expanded) {
                        $(this).text('Show more');
                        msgs.addClass('collapser');
                    }
                    else {
                        $(this).text('Show less');
                        msgs.removeClass('collapser');
                    }
                    expanded = !expanded;
                });
            });
        </script>
    </#if>
    <style type="text/css">
        .dark {
            background: #eef3f5;
        }
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }
        table {
            width: 100%;
            max-width: 600px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            margin-bottom: 10px;
        }
        caption {
            color: #0066cc;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }

        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }
        .bottomless {
            border-bottom: none;
        }
        .collapser {
            max-height: 250px;
            position: relative;
            overflow: hidden;
            display: block;
            padding: 0;
        }
        .collapser:after {
            content:'';
            width: 100%;
            height: 100px;    
            position: absolute;
            left: 0;
            top: 150px;
            padding: 0;
            background:linear-gradient(rgba(255,255,255,0), #FFF);
        }

        .expand {
          width: 100%;
          cursor: pointer;
          color: #06c;
          font-size: 12px;
          padding-top: 0;
          padding-bottom: 5px;
        }

        .expand:hover {
          color: #23527c;
          text-decoration: underline;
        }

        .button-link {
            display: inline-block;
        }

        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        tr {
            padding-bottom: 2px;
        }

        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }

        div {
            padding: 15px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div id='fileData_area' style ='display:none;'>
		<@Helper.ListAllLines outputLines />
    </div>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
    <thead>
        <tr>
            <th>Energy Plot</th>
        </tr>
    </thead>
    <tbody>
        <tr class="row1">
            <td><div id = "chart_div"></div></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption>Interaction Properties</caption>
        <thead>
            <tr>
                <th colspan="2">Interaction Properties</th>
            </tr>
        </thead>
        <tbody>
            <#if outputDataList?size == 0>
                <tr class="row light">
                    <td colspan="2">No data.</td>
                </tr>
            </#if>
			<#list outputDataList as d>
                <tr class="row light">
                    <td>${d.name}</td>
                    <td>${d.value}</td>
                </tr>
            </#list>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th colspan="2">3D Images of the molecule in membranes</th>
            </tr>
        </thead>
        <tbody>
            <#if !Helper.notBlank(pathwayPdbFileUrl) && !Helper.notBlank(boundPdbFileUrl)>
                <tr class="row light">
                    <td colspan="2">No images available; no files were generated.</td>
                </tr>
            <#else>
                <tr class="row light">
                    <td style="width: 50%">Bound State</td>
                    <td style="width: 50%">Translocation Pathway</td>                        
                </tr>
                <tr class="row light">
                    <#if Helper.notBlank(boundGlmolUrl)>
                        <td>
                            <a href="${boundGlmolUrl}" target="_blank">
                                GLmol
                            </a>
                        </td>
                    <#else>
                        <td>No link provided</td>
                    </#if>
                    <#if Helper.notBlank(pathwayGlmolUrl)>
                        <td>
                            <a href="${pathwayGlmolUrl}" target="_blank">
                                GLmol
                            </a>
                        </td>
                    <#else>
                        <td>No link provided</td>
                    </#if>
                </tr>
                <tr class="row light">
                    <#if Helper.notBlank(boundJmolUrl)>
                        <td>
                            <a href="${boundJmolUrl}" target="_blank">
                                JMol
                            </a>
                        </td>
                    <#else>
                        <td>No link provided</td>
                    </#if>
                    <#if Helper.notBlank(pathwayJmolUrl)>
                        <td>
                            <a href="${pathwayJmolUrl}" target="_blank">
                                JMol
                            </a>
                        </td>
                    <#else>
                        <td>No link provided</td>
                    </#if>
                </tr>
            </#if>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Download Output Files</th>
            </tr>
        </thead>
        <tbody>
            <#if Helper.notBlank(boundPdbFileUrl)>
                <tr class="row1">
                    <td>
                        <a href="${boundPdbFileUrl}" download>Bound State PDB</a>
                    </td>
               </tr>
            </#if>
            <#if Helper.notBlank(pathwayPdbFileUrl)>
                <tr class="row1">
                    <td>
                        <a href="${pathwayPdbFileUrl}" download>Pathway PDB</a>
                    </td>
                </tr>
            </#if>
            <#if !Helper.notBlank(boundPdbFileUrl) && !Helper.notBlank(pathwayPdbFileUrl)>
                <tr class="row1">
                    <td>
                        No output files generated.
                    </td>
                </tr>
            </#if>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Output Messages</th>
            </tr>
        </thead>
        <tbody>
            <tr class="row1">
                <td class="${collapserClass}" id="messages">
					<@Helper.ListAllLines outputLines />
                </td>
            </tr>
			<#if expandable>
				<tr>
                    <td class="expand"><span id="expand">Show more</span></td>
                </tr>
			</#if>
        </tbody>
    </table>
    <div>
        <span>
            <a href="${submitAgainUrl}" target="_blank" class="button-link">
              <button type="button">Submit again</button>
            </a>
        </span>
        <input type="button" onClick="window.print()" value="Print this page">
    </div>
</body>
</html>
