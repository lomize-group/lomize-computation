<#--  NotBlank  -->
<#--    -->
#<#function notBlank str="">
	<#return (str?length > 0)>
</#function>

<#--  ListAllLines  -->
<#--  Show each entry in a list on a separate line  -->
<#macro ListAllLines items>
	<#list items as i>
		${i}
		<br/>
	</#list>
</#macro>

