<#import "./lib/helper.ftl" as Helper>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title>PPM3 Results</title>
   <meta http-equiv="cache-control" content="no-cache" />
   <style type="text/css">
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }

        table {
            width: 100%;
            max-width: 600px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            padding-bottom: 10px;
        }

        h1, caption {
            color: #0066cc;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }

        h1 {
            font-size: 20px;
        }

        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        tr {
            padding-bottom: 2px;
        }

        tr.dark {
            background: #eef3f5;
        }

        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }

        div {
            padding-top: 15px;
            text-align: center;
        }
    </style>
</head>
<body>
    <#list membranes as m>
        <#if m.name??>
            <h1>${m.name}</h1>
        </#if>
        <table cellspacing="0" cellpadding="0" class="data" align="center">
            <caption>
                Parameters of Protein in Membrane
            </caption>
            <thead>
                <tr>
                    <#if m.type == "planar">
                        <th>Depth/Hydrophobic Thickness</th>
                        <th>&Delta;G<sub>transfer</sub></th>
                        <th>Tilt Angle</th>
                    <#elseif m.type == "curved">
                        <th>Depth/hydrophobic thickness</th>
                        <th>Intrinsic radius of curvature</th>
                        <th>Tilt angle</th>
                        <th>&Delta;G<sub>transfer</sub> (curved membrane)</th>
                        <th>&Delta;G<sub>transfer</sub> (planar membrane)</th>
                    <#elseif m.type == "micelle">
                        <th>&Delta;G<sub>transfer</sub></th>
                    </#if>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <#if m.type == "planar">
                        <td>
                            <#if m.thickness??>
                                ${m.thickness.value} &#177; ${m.thickness.error} &#197;
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.EPlanar??>
                                ${m.EPlanar} kcal/mol
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.tilt??>
                                ${m.tilt.value} &#177; ${m.tilt.error}&#176;
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                    <#elseif m.type == "curved">
                        <td>
                            <#if m.thickness??>
                                ${m.thickness.value} &#197;
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.radius??>
                                ${m.radius} &#197;
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.tilt??>
                                ${m.tilt.value}&#176;
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.ECurved??>
                                ${m.ECurved} kcal/mol
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                        <td>
                            <#if m.EPlanar??>
                                ${m.EPlanar} kcal/mol
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                    <#elseif m.type == "micelle">
                        <td>
                            <#if m.ECurved??>
                                ${m.ECurved} kcal/mol
                            <#else>
                                No value calculated
                            </#if>
                        </td>
                    </#if>
                </tr>
            </tbody>
        </table>
        <#if (m.type == "planar" || m.type == "curved")
                && (m.embeddedResidues?size > 0 || m.transmembrane?size > 0)>
            <table cellspacing="0" cellpadding="0" class="data" align="center">
                <caption>
                    Membrane Embedded Residues<br />
                    (in Hydrocarbon Core)
                </caption>
                <thead>
                    <tr>
                        <th>Subunits</th>
                        <th>Tilt</th>
                        <th>Segments</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="dark">
                        <td colSpan="2"></td>
                        <td>Embedded residues:</td>
                    </tr>
                    <#list m.embeddedResidues as r>
                        <tr>
                            <td>${r.subunits}</td>
                            <td>${r.tilt}</td>
                            <td>${r.segments}</td>
                        </tr>
                    </#list>
                    <tr class="dark">
                        <td colSpan="2"></td>
                        <td>Transmembrane secondary structure segments:</td>
                    </tr>
                    <#list m.transmembrane as r>
                        <tr>
                            <td>${r.subunits}</td>
                            <td>${r.tilt}</td>
                            <td>${r.segments}</td>
                        </tr>
                    </#list>
                </tbody>
            </table>
        </#if>
    </#list>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <caption></caption>
        <thead>
            <tr>
                <th>Output Messages</th>
            </tr>
        </thead>
        <tbody>
           <tr>
                <td>
                    <@Helper.ListAllLines messageList />
                </td>
           </tr>
        </tbody>
    </table>
    <#if Helper.notBlank(pdbFileUrl)>
        <table cellspacing="0" cellpadding="0" class="data" align="center">
            <caption></caption>
            <thead>
                <tr>
                    <th>Download Output File</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="${pdbFileUrl}" download>PDB file</a>
                    </td>
            </tr>
            </tbody>
        </table>
    </#if>
    <#if Helper.notBlank(jmolUrl) || Helper.notBlank(icn3dUrl) >
        <table cellspacing="0" cellpadding="0" class="data" align="center">
            <thead>
                <tr>
                    <th>Image of the protein in membrane</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <#if Helper.notBlank(jmolUrl)>
                            <a href="${jmolUrl}" target="_blank">
                                jmol
                            </a>
                        </#if>
                        <#if Helper.notBlank(icn3dUrl)>
                            &nbsp;|&nbsp;
                            <a href="${icn3dUrl}" target="_blank">
                                iCn3D
                            </a>
                        </#if>
                    </td>
                </tr>
            </tbody>
        </table>
    </#if>
    <div>
        <input type="button" onClick="window.print() " value="Print this page">
    </div>
</body>
</html>
