<#import "./lib/helper.ftl" as Helper>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title>Fold Results</title>
   <meta http-equiv="cache-control" content="no-cache" />
   <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.3/dist/jquery.min.js"></script>
   <script src="https://3Dmol.org/build/3Dmol-min.js"></script>
   <script src="https://3Dmol.org/build/3Dmol.ui-min.js"></script>
   <style type="text/css">
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }

        table {
            width: 100%;
            max-width: 600px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            padding-bottom: 10px;
        }

        h1, caption {
            color: #0066cc;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }

        h1 {
            font-size: 20px;
        }

        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        tr {
            padding-bottom: 2px;
        }

        tr.dark {
            background: #eef3f5;
        }

        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }

        div.printer {
            padding-top: 15px;
            text-align: center;
        }

        img {
            max-width: 100%;
        }
    </style>
</head>
<body>
    <#if coverageUrl?? || paeUrl?? || plddtUrl??>
        <h1>Plots</h1>
        <#if coverageUrl??>
            <table cellspacing="0" cellpadding="0" class="data" align="center">
                <thead>
                    <tr>
                        <th>Sequence Coverage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="${coverageUrl}" target="_blank">
                                <img src="${coverageUrl}" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </#if>
        <#if paeUrl??>
            <table cellspacing="0" cellpadding="0" class="data" align="center">
                <thead>
                    <tr>
                        <th>PAE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="${paeUrl}" target="_blank">
                                <img src="${paeUrl}" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </#if>
        <#if plddtUrl??>
            <table cellspacing="0" cellpadding="0" class="data" align="center">
                <thead>
                    <tr>
                        <th>Predicted IDDT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="${plddtUrl}" target="_blank">
                                <img src="${plddtUrl}" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </#if>
    </#if>
    <#if (pdbs?size > 0)>
        <h1>Visualizations</h1>
        <#list pdbs as p>
            <table cellspacing="0" cellpadding="0" class="data" align="center">
                <thead>
                    <tr>
                        <th>
                            <a href="${p.pdbUrl}">${p.name}</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="${p.jmolUrl}" target="_blank">jmol</a>
                            &nbsp;|&nbsp;
                            <a href="${p.icn3dUrl}" target="_blank">iCn3D</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div
                                style="height: 400px; position: relative;"
                                class="viewer_3Dmoljs"
                                data-href="${p.pdbUrl}"
                                data-backgroundcolor="0xffffff"
                                data-style="stick"
                                data-ui="true"
                            >
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </#list>
    </#if>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th>Download Results Folder</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <a href="${resultsUrl}">test.results.tar.gz</a>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="printer">
        <input type="button" onClick="window.print() " value="Print this page">
    </div>
</body>
</html>
