<#import "./lib/helper.ftl" as Helper>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>TMMatch Server - Membranome database</title>
  <style type="text/css">

	table.large {
	  width: 100%;
	  max-width: 800px;
	  margin-top: 20px;
	  margin-bottom: 20px;
	  border-left: 1px solid #ddd;
	  border-top: 1px solid #ddd;
	}

	table.small {
	  width: 80%;
	  max-width: 400px;
	  margin-top: 20px;
	  margin-bottom: 20px;
	  border-left: 1px solid #ddd;
	  border-top: 1px solid #ddd;
	}

	th {
	  color: #159;
	  background: #eef3f5;
	  border-bottom: 3px solid #999;
	  border-right: 1px solid #ddd;
	  padding: 5px;
	  text-align: center;
	}
	tr {
	  text-align: center;
	  padding-bottom: 2px;
	}
	td {
	  border-bottom: 1px solid #ddd;
	  border-right: 1px solid #ddd;
	  padding: 5px;
	}

	.active {
	  background-color: #E1E1E1;
	}

	.footer {
	  width: 100%;
	  text-align: center;
	}
  </style>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
  <meta name="robots" content="all" />
  <meta name="keywords" content="OPM, orientation, protein, membrane, localization, delta, Gibbs, transfer, topology, thickness, tilt, angle, subunit" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src = "https://storage.googleapis.com/membranome-assets/server_assets/Three49custom.js"></script>
  <script type="text/javascript" src="https://storage.googleapis.com/membranome-assets/server_assets/GLmol.js"></script>
  <link rel="stylesheet" href="https://storage.googleapis.com/membranome-assets/server_assets/kmfranz_style.css" type="text/css" media="screen"/>
</head>
<body style="background-color: #fff;">
  <table class="large" cellspacing="0" cellpadding="0" align="center">
	<thead>
	  <tr>
		<th>Output Messages</th>
	  </tr>
	</thead>
	<tbody>
	 <tr>
		<td>
			<@Helper.ListAllLines messageList />
		</td>
	 </tr>
	</tbody>
  </table>
  <#if Helper.notBlank(pdbFileUrl)>
	<table width="400px" cellspacing="0" cellpadding="0" class="data" align="center">
		<caption></caption>
		<thead>
		<tr>
			<th>GLMOL Visualization</th>
		</tr>
		</thead>
		<tbody>
		<tr class="row1" style="padding:0px;">
			<td style="padding:0px;">
			<div id='glmol' style="width:500px; height:400px; background-color: lightgray;"></div>
			<div id='glmol_footer'>
				<p id ="model_number" style="font-family: monospace;"></p>
				<p id ="sequence_1" style = "font-family: monospace; letter-spacing:4px;"></p>
				<p id ="sequence_2" style = "font-family: monospace; letter-spacing:4px;"></p>
			</div>
			</td>
		</tr>
		</tbody>
	</table>
	<table id="results-table" width="500px" cellspacing="0" cellpadding="0" class="results-view" align="center">
		<thead>
		<tr>
			<td><b>Model</b></td>
			<td><b>&Delta;G<sub>asc</sub></b></td>
			<td><b>&Delta;G<sub>stb</sub></b></td>
			<td><b>E<sub>asc</sub></b></td>
			<td><b>R<sub>asym</sub></b></td>
			<td><b>Distance</b></td>
			<td><b>Angle</b></td>
		</tr>
		<tr>
			<td></td>
			<td>kcal/mol</td>
			<td>kcal/mol</td>
			<td>kcal/mol</td>
			<td>&#8491;</td>
			<td>&#8491;</td>
			<td>Degrees</td>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<table class="small" cellspacing="0" cellpadding="0" align="center">
	  <thead>
		<tr>
		  <th>Download Output File</th>
		</tr>
	  </thead>
	  <tbody>
		<tr>
		  <td>
			<a href="${pdbFileUrl}" download>pdb file</a>
		  </td>
		</tr>
	  </tbody>
	</table>
  </#if>
  <div class="footer">
	  <input type="button" onClick="window.print() " value="Print this page">
  </div>
  <div class = 'f-hidden' style='display:none;'>
	<textarea style='none;' id = 'glmol_src'></textarea>
  </div>
  <div id = 'glmol_src_container' style='display:none;'></div>
</body>
<#if Helper.notBlank(pdbFileUrl)>
	<script type="text/javascript">
		//Global GLMol Object3D^M
		var glmol_object = new GLmol('glmol', true);
		var global_model_attributes = [];
		var active = undefined;
		//All this can be moved to a separate file once finished.^M
		$(document).ready(function() {
			var url = "${pdbFileUrl?js_string}";
			//this calls the AJAX request to load the file from server
			// and will parse out the data.
			load_mol_data(url); 
			//load the GLMol Applet ^M
			//because we are affecting the DOM elements with the source of the PDB file.^M
			//we have to wait for the browser to update the DOM with the text.^M
			//Jquery Append function acts synchroniously, while the browser does not^M
			//this is why we have to wait 10ms to ensure that the DOM is updated before calling the^M
			//initialize function^M
			setTimeout(function(){
			initialize_mol_view();
			}, 2000);
		});
		function load_mol_data(url){
			$.get(url, function(ret){
			//console.log(ret);
			$('#glmol_src').val(ret);
			var lines = ret.split("\n");
			var models = [];
			var modelSource = [];
			var modelAttributes = [];
			var DGasc = 0;
			var DGstd = 0;
			var Easc = 0;
			var Rasym = 0;
			var currentModelNumber = 0;
			var InterhelixDistance = 0;
			var Angle = 0;
			var SequenceRemark = "";
			var SequenceRemark_2 = "";
			var lineConcatenate = "";
			for (var i = 0; i < lines.length; i++){                   
				var lineData = lines[i].split(" ");
				if (lineData[0] == "MODEL") {
				var modelNumber = lineData.slice(-1)[0];
				append_glmol_src(modelNumber);
				models.push(modelNumber);
				lineConcatenate = lines[i] + "\n";
				}
				else if( lineData[0] == "REMARK"){
				lines[i] = lines[i].substring(6);
				lines[i] = lines[i].replace(/ /g,'');
				var attributes = lines[i].split(",");
				var quants = [];
				if(attributes.length > 1){
					//First two lines in the remark section of the file^M
					for (var n = 0; n < attributes.length; n++){
					//segments = attributes[n].split("=")^M
					quants.push(attributes[n].replace(/[^0-9\.\-]+/g,""));
					}
					if(attributes[0].substring(0, 4) == "DGas"){
					DGasc = quants[0];
					DGstd = quants[1];
					Easc = quants[2];
					Rasym = quants[3];
					}
					else{
					InterhelixDistance = quants[0];
					Angle = quants[1] + "0";
					}
				}
				else{
					//shorter section in the remarks^M
					if(lines[i].indexOf('.') !== -1){
					SequenceRemark_2 = lines[i];
					}
					else{
					//no period string^M
					SequenceRemark = lines[i];
					}
				}                        
				}
				else if(lineData[0] == "ENDMDL"){
				lineConcatenate = lineConcatenate + lines[i] + '\n';
				modelSource.push(lineConcatenate);
				modelAttributes.push({"modelNumber":modelNumber, "DGasc":DGasc, "DGstd":DGstd, "Easc":Easc, "Rasym":Rasym,"InterhelixDistance": InterhelixDistance, "Angle":Angle, "SequenceRemark":SequenceRemark, "SequenceRemark_2":SequenceRemark_2});
				lineConcatenate = "";
				}
				else {
				lineConcatenate = lineConcatenate + lines[i] + '\n';
				}
			}
			global_model_attributes = modelAttributes;
			//iterate through each model^M
			insert_source_data(modelSource);
			load_table(modelAttributes);
			});
		};
		function initialize_mol_view(){
			glmol_object.defineRepresentation = function() {
			var all = this.getAllAtoms();
			var allHet = this.getHetatms(all);
			//var hetatm = this.removeSolvents(allHet);^M
			this.colorByAtom(all, {});
			this.colorByChain(all);
			var asu = new THREE.Object3D();
			console.log(this.sphereRadius);
			this.drawAtomsAsSphere(asu, allHet, this.sphereRadius);
			this.drawBondsAsStick(asu, this.getSidechains(all), this.cylinderRadius, this.cylinderRadius);
			this.drawCartoon(asu, all, this.curveWidth, this.thickness);
			this.drawSymmetryMates2(this.modelGroup, asu, this.protein.biomtMatrices);
			this.modelGroup.add(asu);
			};
			glmol_object.loadMolecule();
		}
		function insert_source_data(modelSource){
			for (var i = 0; i < modelSource.length; i++){
			$("#molsrc_"+i).val(modelSource[i]);
			}
		}
		function load_table(modelAttributes){
			for(var i = 0; i < modelAttributes.length; i++){
			$('#results-table tbody').append("<tr id='" + i + "' onclick='changeMolecule(this)'> <td> "+ modelAttributes[i].modelNumber +" </td> <td> "+ modelAttributes[i].DGasc +" </td> <td> "+ modelAttributes[i].DGstd +" </td> <td> "+ modelAttributes[i].Easc +" </td> <td> "+ modelAttributes[i].Rasym +" </td> <td> " + modelAttributes[i].InterhelixDistance +" </td><td> "+ modelAttributes[i].Angle +" </td> </tr>");
			}


			$('#sequence_1').text(global_model_attributes[0].SequenceRemark);
			$('#sequence_2').text(global_model_attributes[0].SequenceRemark_2);
			$('#model_number').text("Model " + global_model_attributes[0].modelNumber);
		}
			
		function append_glmol_src(index){
			//This creates the hidden table of glmol sources for each model^M
			//to switch between them we will load whatever data is in the corresponding ^M
			// text box.^M
			$('#glmol_src_container').append('<textarea id = \'molsrc_'+ (index - 1) +'\'> </textarea>');
		};
		
		function changeMolecule(caller){
			if (active === caller.id) {
			return;
			}
			if (active) {
			$('#' + active).removeClass('active');
			}
			$(caller).addClass('active');
			active = caller.id;
			var new_source = $('#molsrc_'+active).val()
			$('#glmol_src').val(new_source);
			glmol_object.loadMolecule();
			$('#sequence_1').text(global_model_attributes[active].SequenceRemark);
			$('#sequence_2').text(global_model_attributes[active].SequenceRemark_2);
			$('#model_number').text("Model " + global_model_attributes[active].modelNumber);
		}
	</script>
</#if>
</html>
