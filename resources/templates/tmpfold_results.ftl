<#import "./lib/helper.ftl" as Helper>

<#assign expandable=(outputLines?size > 20)>
<#assign collapserClass=(expandable?then('collapser bottomless', ''))>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>TMPfold Results</title>
    <meta http-equiv="cache-control" content="no-cache" />
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <#if expandable>
        <script type="text/javascript">
            var expanded = false;
            $(document).ready(function(){
                var expander = $("#expand");
                if (expander.length == 0) {
                return;
                }
                var msgs = $('#all_output');
                expander.click(function(){
                    if (expanded) {
                    $(this).text('Show more');
                    msgs.addClass('collapser');
                    }
                    else {
                    $(this).text('Show less');
                    msgs.removeClass('collapser');
                    }
                    expanded = !expanded;
                });
            });
        </script>
    </#if>
    <style type="text/css">
        .dark {
            background: #eef3f5;
        }
        html, body {
            width: 100%;
            overflow: auto;
            margin: 25px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            line-height: 1.7em;
        }
        table {
            min-width: 600px;
            max-width: 90%;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            margin: 0 auto;
            font-size: 10px;
            vertical-align: top;
            margin-bottom: 20px;
        }

        caption {
            color: #06c;
            text-align: center;
            font-size: 14px;
            font-weight: bold;
            margin-bottom: 0px;
            padding-bottom: 5px;
        }

        h2{
            color: #0066cc;
            text-align: center;
            font-weight: bold;
            margin-bottom: 10px;
        }
        h3{
            color: #0066cc;
            text-align: center;
            font-weight: bold;
            margin-bottom: 0;
            padding-bottom: 0;
        }

        hr{
            width: 92%;
            margin: 0 auto;
        }

        th {
            color: #159;
            background: #eef3f5;
            border-bottom: 3px solid #999;
            border-right: 1px solid #ddd;
            padding: 5px;
            text-align: center;
        }

        .bottomless {
            border-bottom: none;
        }
        .collapser {
            max-height: 250px;
            position: relative;
            overflow: hidden;
            display: block;
            /*padding-bottom: 100px 500px 0 5px;*/
            padding: 5px;
        }
        .collapser:after {
            content:'';
            width: 100%;
            height: 100px;    
            position: absolute;
            left: 0;
            top: 150px;
            padding: 10px;
            background:linear-gradient(rgba(255,255,255,0), #FFF);
        }

        .expand {
          width: 100%;
          cursor: pointer;
          color: #06c;
          font-size: 12px;
          padding-top: 0;
          padding-bottom: 5px;
          text-align: center;
        }

        .expand:hover {
          color: #23527c;
          text-decoration: underline;
        }

        .bold {
            font-weight: bold;
        }

        td {
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 5px;
        }
        td.center-cell {
            text-align: center;
        }

        td.output-cell {
            font-family: monospace;
            font-size: 13px;
            text-align: left;
        }

        tr {
            padding-bottom: 2px;
        }

        table, th, td {
            line-height: 1.6em;
            vertical-align: top
        }

        div {
            padding-top: 15px;
            text-align: center;
        }
    </style>
</head>
<body>
    <h2>TMPfold Results<#if pdbCode??> - ${pdbCode}.pdb</#if></h2>
    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th>Output</th>
            </tr>
        </thead>
        <tbody>
           <tr>
                <td class="${collapserClass} output-cell" id="all_output">
                    <@Helper.ListAllLines outputLines />
                </td>
           </tr>
           <#if expandable>
               <tr>
                   <td class="expand"><span id="expand">Show more</span></td>
               </tr>
           </#if>
        </tbody>
    </table>
    <br/>

    <#list subunits as s>
        <#if (s.helixMatrix?size > 0) >
            <hr/>
            <h3>Subunit ${s.name}</h3>
            <div>
                <table cellspacing="0" cellpadding="0" class="data">
                    <thead>
                        <tr>
                            <th colspan="2">TM Segments - Subunit ${s.name}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                TM segments:
                                <#list s.tmSegments as seg>
                                    ${seg.start}-${seg.end}<#if seg?hasNext>, </#if>
                                </#list>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${s.energyOfAssociationOutput}
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" class="data">
                    <thead>
                        <tr>
                            <th colspan="${s.helixMatrix?size}">
                                Helix-helix association free energies - Subunit ${s.name}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <#list s.helixMatrix as row>
                            <#if row?counter == 1>
                            <tr class="dark bold">
                            <#else>
                            <tr class="${row?itemCycle('dark', '')}">
                            </#if>
                                <#if row?counter == 1>
                                    <td class="center-cell">TMH</td>
                                </#if>
                                <#list row as val>
                                    <#if val?counter == 1>
                                    <td class="center-cell bold">${val}</td>
                                    <#else>
                                    <td class="center-cell">${val}</td>
                                    </#if>
                                </#list>
                            </tr>
                        </#list>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" class="data">
                    <caption>
                        TM Helix Assembly Pathway
                    </caption>
                    <thead>
                        <tr>
                            <th>Step</th>
                            <th>&Delta;<i>G<sub>assoc</sub></i></th>
                            <th>Assembly</th>
                        </tr>
                    </thead>
                    <tbody>
                        <#list s.assemblyPathway as step>
                            <tr>
                                <#if step.gibbs??>
                                    <td class="center-cell">${step.num}</td>
                                    <td class="center-cell">${step.gibbs}</td>
                                <#else>
                                    <td class="center-cell" colspan="2">${step.num}</td>
                                </#if>
                                    <td class="center-cell">${step.assembly}</td>
                            </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
        </#if>
    </#list>
    <br/>
    <hr/>
    <br/>

    <#if (freeEnergies?size > 0)>
        <table cellspacing="0" cellpadding="0" class="data">
            <thead>
                <tr>
                    <th colspan="2">
                        Free energy of association of TM helices from different subunits
                    </th>
                </tr>
            </thead>
            <tbody>
                <#list freeEnergies as e>
                    <tr>
                        <td>${e}</td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </#if>

    <#if (pairwiseEnergies?size > 0)>
        <table cellspacing="0" cellpadding="0" class="data">
            <thead>
                <tr>
                    <th>
                        Energies of association of subunits
                    </th>
                </tr>
            </thead>
            <tbody>
                <#list pairwiseEnergies as energy>
                    <#if energy?hasNext>
                    <tr>
                    <#else>
                    <tr class="center-cell dark">
                    </#if>
                        <td>${energy}</td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </#if>

    <table cellspacing="0" cellpadding="0" class="data" align="center">
        <thead>
            <tr>
                <th colspan="3">Download & View Output Files</th>
            </tr>
        </thead>
        <tbody>
            <#assign hasFiles = false>
            <#list subunits as s>
            <#if s.pdbFileUrl?? && s.glmolViewerUrl??>
                <#assign hasFiles = true>
                <tr>
                    <td class="center-cell">Subunit ${s.name}</td>
                    <td class="center-cell">
                        <a href="${s.pdbFileUrl}" download>Download ${s.name}</a>
                    </td>
                    <td class="center-cell">
                        <a target="_blank" href="${s.glmolViewerUrl}">
                            View ${s.name} in GLmol
                        </a>
                    </td>
                </tr>
            </#if>
            </#list>
            <#if !hasFiles>
                <tr>
                    <td colspan="3">No output files</td>
                </tr>
            </#if>
        </tbody>
    </table>
    <div> 
        <input type="button" onClick="window.print()" value="Print this page">
    </div>
    <br/>
</body>
</html>
