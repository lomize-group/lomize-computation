#!/usr/bin/env bash

# htop process monitor
tmux send "htop" "C-j"

# bootRun debug server
tmux new-window
tmux send "gradle bootRun" "C-j"

# application builder
tmux new-window
tmux send "gradle build -x test --continuous" "C-j"
