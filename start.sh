## Name of build output (results from running "./gradlew build")
JARFILE=build/libs/lomize-computation-0.1.0.jar

## Name of the init.d system service that will be started
SERVICE_NAME=lomize-computation-server.service


## Build;
## Then make the jar read-only for security.
## Then restart systemctl.

echo "REMOVING OLD JAR";
rm $JARFILE;
### Remove old JAR file if it's there


### Build
echo "BUILDING";
./gradlew bootJar &&

echo "CHANGING JAR PERMISSION TO READONLY";
chmod 500 $JARFILE;
### Change jar bundle to readonly

echo "RESTARTING SERVICE";
sudo systemctl restart $SERVICE_NAME;
### Restart systemctl service to deploy changes
